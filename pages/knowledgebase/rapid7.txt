===== Rapid 7 =====
/**
 * This guide is dynamically updated. the following tags are used in the following sections:
 * - A general description of Rapid 7 and references to general CVE answers and support
 * - A list of non-CVE responses
 */

Rapid 7 is a third-party compliance validation company that may perform audits and other such penetration tests against ClearOS. This section of support is dedicated for responding to audits and to qualify your running version of ClearOS with them for PCI, HIPAA and other such standards.

If you do NOT see a response listed here for an item in your audit, please consult with the following for support methods:

  * [[:content:en_us:kb_bestpractices_vulnerabilities_overview|Vulnerabilities Overview]]

===== Rapid 7 non-CVE Responses =====
  * [[:content:en_us:kb_3rdparty_rapid_7_missing_httponly_flag_from_cookie|Missing HttpOnly Flag From Cookie]]
  * [[:content:en_us:kb_3rdparty_rapid_7_missing_secure_flag_ssl_cookie|Missing Secure Flag From SSL Cookie]]
  * [[:content:en_us:kb_3rdparty_rapid_7_click_jacking|Click Jacking]]
  * [[:content:en_us:kb_3rdparty_rapid_7_tcp_timestamp_response|TCP timestamp response]]
  * [[:content:en_us:kb_3rdparty_rapid_7_tlsssl_3des_cipher_suite|TLS/SSL Server Supports 3DES Cipher Suite]]
  * [[:content:en_us:kb_3rdparty_rapid_7_tlsssl_supports_static_key_ciphers|TLS/SSL Supports The Use of Static Key Ciphers]]
===== Notices =====
This section of support documentation that deals with vulnerabilities, CVEs, and named security audit providers is not to be construed as a complete and accurate representation of all known vulnerabilities to ClearOS, ClearCenter repository contents, community applications, and/or other closed and open source vulnerabilities. Rather the purpose is to provide information, help, and technical responses to active support and vulnerability challenges as they arise. If the information is incorrect or incomplete, please use the ClearCenter Support method that is associated with your license to request updates to particular issues and they will be modified here in response.


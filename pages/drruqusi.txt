===== ClearOS Documentation Library =====
Welcome to the documentation library for ClearOS, ClearCenter, and ClearFoundation. This page is the comprehensive index for all documentation related to ClearOS, ClearFoundation, and ClearCenter. Several broad categories exist where you will find the information you are looking for. Bookmark this page so if you get lost, you can start here again. The types of information here is broken into these broad categories:

  * **User Guides**
    * (These are product specific manuals. These contain the answer to 'what'.)
  * **Knowledgebase, Howtos, and Best Practices**
    * (These generally contain information not in the manual and answer the questions 'why' and 'how'.)
  * **Announcements**
    * (These document what is new, old and in between. Topics include released items and answers to security questions as they arise.)
  * **Help**
    * (This all is too much. I need more of an overview to understand what this is all about, what to do, how to get help, or something that isn't listed here)
===== User Guides =====
User Guides are authoritative guides written by the application developers to highlight the features and functionality of an app or for the install of ClearOS. If you are installing ClearOS or you are trying to find out all your options on an app in the marketplace, this is a good place to start.

=== User Guides for ClearOS ===
User Guide indexes by version of ClearOS provide a great place to start with your version-specific installation. Popular guides include:

/* {{ :clearos7_interface_dashboard.png?330|ClearOS 7}} */
/*  * [[:index:userguid7|ClearOS 7.x]] */

^[[:index:userguide7|ClearOS 7.x - Powerful IT Platform]]^ 
|[[:index:userguide7|{{:clearos7_interface_dashboard.png?330}}]]| 
^[[:index:userguide6|ClearOS 6.x (Current) - Powerful IT Platform]]^ 
|[[:index:userguide6|{{:clearos6_interface_dashboard.png?330}}]]| 
^[[:userguides:userguide5|ClearOS 5.2 (Previous)- EOL December 2015]]^ 
|[[:userguides:userguide5|{{:clearos5_interface_dashboard.png?330}}]]| 

Can't find the user guide for ClearOS 7 that you are looking for? Check out the [[:index:all7apps|complete list of user guides and apps for ClearOS 7]].

For ClearOS 6, check out this [[:index:all6apps|complete list of user guides and apps for ClearOS 6]].



=== User Guide for ClearSDN ===
For all versions of ClearOS versions listed above, the ClearOS Service Delivery Network (ClearSDN) will help you manage your server. There is only one version of the Service Delivery Network which is the current version. So no matter which version of ClearOS you are running, you will get more out of it if you are familiar and use the tools of the ClearSDN

^[[:userguides:sdn|Service Delivery Network - ClearCenter Portal]]^
|[[:userguides:sdn|{{:servicedeliverynetworkinterface.png?330|Service Delivery Network - ClearCenter Portal}}]]|

=== User Guide for ClearVM ===
ClearVM is a separate product from ClearOS. Think of ClearVM as a hypervisor that can run ClearOS or other operating systems under it. This way you can use one piece of hardware to run many, many more operating systems under it. This is a great solution if you are coming from another environment (like Windows Server® for example) and still need to still run that server and others. ClearVM allows for multiple operating systems running on the same box at the same time.

^[[:content:en_us:vm_start|ClearVM - Manage Hypervisor from the Cloud]]^
|[[:content:en_us:vm_start|{{:clearvmmanagementpage.png?330|ClearVM - Manage Hypervisor from the Cloud}}]]|

=== User Guides for this Website and Forums ===

/*  * [[:content:en_us-indexes:websitehelp|ClearOS Website and Forum Guides]] */
  * [[:knowledgebase:wiki:wiki|Wiki Style Guides]]

=== User Guides for Development and Other Things ===


Other guides have been assembled for those that like to roll up their sleeves and get under the hood of all things ClearOS. The following guides are useful for getting into the workings of ClearOS or this website.

  * [[:content:en_us:dev_start|Getting Started with Development]]
  * [[:index:developer|Developer Guides]]
  * [[:content:en_us:qa_start|Quality Assurance (QA) and Testing]]

===== Knowledge Base =====
The ClearOS Knowledge Base contains free and paid articles dealing with best practices, implementation guides and outlines, real-world deployment considerations, troubleshooting techniques and tools, and support beyond. 

It contains Howto documents designed to implement features. It also contains initiatives and helpful documentation for developers including prototyping, skunkworks, and works in progress.

  * [[:index:kb|Full Knowledge Base]]

Frequently used Knowledge Base sections:

  * [[:knowledgebase:faq:|Frequently asked questions (FAQ)]]
  * [[:knowledgebase:bestpractices|Best Practices, Whitepapers and Implementation Guides]]
  * [[:knowledgebase:troubleshooting:|Troubleshooting and error correction]]
  * [[:knowledgebase:skunkworks|Prototyping, unsupported apps, and tricks]]
  * [[:knowledgebase:hardware:|Hardware Compatibility Lists]]


===== Announcements =====
This section contains documents that are the result of announcements made by ClearCenter, ClearFoundation and others. These announcements are significant posts detailing direction, initiatives, and releases.

  * [[:content:en_us:kb_howto_upgrade_to_business|Upgrading ClearOS 7 Community to Business Edition]]
  * [[:announcements:|Announcements]]

Frequent announcements links

  * [[:announcements:releases:|Release Notes]]
  * [[:announcements:cvedatabase|CVE and Vulnerability Statements]]



===== Knowledge Base =====
The ClearOS Knowledge Base contains free and paid articles dealing with best practices, implementation guides and outlines, real-world deployment considerations, troubleshooting techniques and tools, and support beyond. 

It contains Howto documents designed to implement features. It also contains initiatives and helpful documentation for developers including prototyping, skunkworks, and works in progress.

  * [[:index:kb|Full Knowledge Base]]

Frequently used Knowledge Base sections:

  * [[:knowledgebase:faq:|Frequently asked questions (FAQ)]]
  * [[:knowledgebase:bestpractices|Best Practices, Whitepapers and Implementation Guides]]
  * [[:knowledgebase:troubleshooting:|Troubleshooting and error correction]]
  * [[:knowledgebase:skunkworks|Prototyping, unsupported apps, and tricks]]
  * [[:knowledgebase:hardware:|Hardware Compatibility Lists]]
==== Links ====
  * [[http://www.tldp.org|Linux Documentation Project]]
  * [[:index:allcontent|Complete list of ClearOS documentation]]

{{keywords>clearos, what is clearos, clearos documentation, clearos help, documentation, menu, index, howto, knowledgebase, announcement, announcements, release notes, support, clearos support, library}}
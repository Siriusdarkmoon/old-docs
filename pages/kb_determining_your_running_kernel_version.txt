====== Determining your Running Kernel Version in ClearOS ======

===== ClearOS 6.x =====

Navigate to the dashboard (https://IP_ADDRESS:81/app/dashboard).
The kernel version will be listed under System Details.

{{content:en_us:6_determining_your_running_kernel_version.jpg?1000}}

===== ClearOS 7.x =====

Navigate to 'Reports' > 'Performance and Resources' > 'System Report' (https://IP_ADDRESS:81/app/system_report)
The kernel version will be listed under System Details.

{{content:en_us:7_determining_your_running_kernel_version.jpg?1000}}
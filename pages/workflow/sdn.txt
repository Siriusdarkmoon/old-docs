===== ClearCenter Service Delivery Network (ClearSDN) =====
/**
 * This guide is dynamically updated. the following tags are used in the following sections:
 * userguide sdn categorydns
 */
ClearSDN (sometimes called the ClearCenter Portal) is a collection of services that enable coordinated control of your IT environment from the Cloud. These services have been available on this platform since 2000 and include a variety of services and products.

This guide will help you with the various ClearSDN services.

===== DNS Service =====
ClearCenter offers DNS Services as a registrar agent. This means that you can purchase your domain names from ClearCenter or transfer your domains to ClearCenter from your other provider (such as GoDaddy or Network Solutions). What makes ClearCenter's DNS services special is a tight integration with your ClearOS operating system that allows for services like Dynamic DNS and other delivered Software as a Service functions. Other services such as MX Backup and Domain hosting are also available and often included with your purchase of DNS from ClearCenter..

==== DNS Services ====
  * [[:content:en_us:sdn_dns_primer|DNS Primer]]
  * [[:content:en_us:sdn_domain_management_with_whois|Domain Management]]
  * [[:content:en_us:sdn_domain_registration|Domain Registration]]
  * [[:content:en_us:sdn_domain_renewal|Domain Renewal]]
  * [[:content:en_us:sdn_domain_transfer|Domain Transfer]]
  * [[:content:en_us:sdn_mail_backup_and_mx_backup|Mail Backup and MX Backup]]

==== DNS Records ====
  * [[:content:en_us:sdn_dns_a_records_and_host_records|A Records and Hosts Records]]
  * [[:content:en_us:sdn_dns_cname_records_and_alias_records|CNAME Records and Alias Records]]
  * [[:content:en_us:sdn_dns_mx_records_and_mail_records|MX Records and Mail Records]]
  * [[:content:en_us:sdn_dns_txt_records_and_spf_records|TXT Records and SPF Records]]

==== DNS Troubleshooting ====
  * [[:content:en_us:sdn_domain_management_with_whois|Domain Management]]
  * [[:content:en_us:sdn_registrant_verification_and_icann|Registrant Verification and ICANN]]


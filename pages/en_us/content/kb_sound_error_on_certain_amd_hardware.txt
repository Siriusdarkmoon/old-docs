===== snd_hda_intel Error on Certain Types of AMD Hardware =====
/**
 * 
 */
On certain AMD-based systems ClearOS may attempt to load a sound driver that appears to present itself as a standard Intel card. If this is the case, you may safely ignore the warning or remove the attempt to load the mismatched driver. To prevent the mismatched driver, run the following from command prompt:

  echo "blacklist snd_hda_intel" /etc/modprobe.d/mserver.conf

Once this is done, you can reboot to make it go into effect.

=====Sound Support in ClearOS=====
Presently there are no apps in the marketplace which require sound support. If you are a developer or hobbyist and wish to implement sound for your custom application when experiencing this error, you can download the driver as it is made available or wait until the driver is added to upstream support by AMD. Such drivers, if known, will be made available on this page once they are released.

{{keywords>clearos, clearos content, snd_hda_intel, AMD, kb, sound, audio, driver, clearos7, categoryinstall, maintainer_dloper}}

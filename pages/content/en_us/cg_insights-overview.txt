===== Insights - Overview =====
Insights is a cost and usage reporting and analytics service for multi and hybrid cloud infrastructure.

==== Filtering ====
=== Time Filter ===
Use the Time filters to select both the time type and time period for your report. For example, you can select Weekly (time type) and Last Week (time period). The Insights reports will display data for the time period selected.

[image]

=== Filter By ===
The Filter By filter will display data based on a specific filter type. There are 3 types: 

  - Stack
  - Cloud
  - Tag

When selected, these filters will provide the filter specific data and dynamically update the usage, inventory, and usage reports. Here is a deeper description for each filter type:

== Stack ==

The Stack filter provides cost and usage reports for a specific stack. Stacks in ClearGLASS relate to infrastructure that is tied to a specific application.

== Cloud ==

The Cloud filter provides cost and usage reports for specific clouds. When a specific cloud is selected the reports will dynamically populate with the cloud specific data.

[image]

== Tag ==

The Tag filter provides cost and usage reports for all tagged resources. 

[image]

===== Cost, Inventory, Usage, and Recommendations =====
==== Cost ====
The Cost summary displays two metrics:

  * The cost for a period of time.
  * How the cost compared to a previous period.
In the example below, the spend is $1075.52 per week and is 39.57% increase compared to the previous week.

[image]

==== Machine Count ====

The Machine Count displays the total machines provisioned for a specific time period. It also compares the total against a previous time period. In the example above, there is a total of 94 machines for the specific time period which is 4.08% decrease when compared to the previous week.

=== Average Load ===
The Average Load summary is a measure of the amount of computational work that a computer system performs. The summary displays an average load for all systems. This data point only measures the load on machines with monitoring enabled. Learn how load is calculated.

=== Recommendations ===
The Recommendations section is not yet available.

==== Cost Overview ====
The Cost Overview reports on:

  * Monthly Run Rate
    * The Monthly Run Rate projects the monthly cost of infrastructure based on current usage.
  * Cost
    * This is the projected cost of infrastructure based on the filter type selected.
  * Clouds
    * This report displays information related to your clouds, including the total clouds and name, total machines per cloud and costs. 
  * Tags
    * This report provides machine inventory and costs per tag and untagged machines.

[image]

==== Utilisation Overview ====
The utilisation reports on system load which is a measure of the amount of computational work that a computer system performs and represents the average system load over a period of time. A machine generates a metric of three "load average" numbers in the kernel, learn more about how load is calculated.

The utilisation reports work with machines that have ClearGLASS monitoring enabled. - if you have not enabled ClearGLASS monitoring on your machines the machines will NOT appear in this report.

The Utilisation Overview section provides a collection of metrics related to usage, including: 

  * Maximum - maximum system load for a time period.
  * Minimum - minimum system load for a time period
  * Average Load - average system load for a time period.
  * The Machines listed in this report having monitoring enabled. 
The report provides total cores and usage metrics. Plus, you can see total cores and usage metrics as they relate to tags and clouds enabling you to see utilisation across specific environments, applications, and even individuals.

[image]

==== Machine Overview ====
The Machine Overview report provides:

  * Total machines for a specific period of time.
  * Displays the:
    * Maximum simultaneously running machines for a specific time period.
    * The average number of simultaneously running machines for a specific time period.
    * Unique machines that have existed at any point during a specific time period. 
    * Here is an example to demonstrate how the numbers differ and relate.
  * Inventory of clouds, machines, and related costs.
  * Inventory of machines and costs as they relate to tags.

[image]
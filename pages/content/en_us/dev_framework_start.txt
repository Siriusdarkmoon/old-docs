===== ClearOS Apps and Framework =====
Welcome!  If you are developing **ClearOS Apps**, this is the place to bookmark.  Among other things, you will find tutorials, reference guides, coding standards and best practices.

{{:developer:framework:ci_logo_flame.jpg?80 |CodeIgniter Logo}} With the release of ClearOS 6, a new PHP-based application developer framework was launched.  Fortunately, re-inventing the wheel was not necessary.  There were already a number of frameworks and tools available that took care of the core requirements: speed, extensibility, rapid development and more.  An [[:content:en_us:dev_framework_history|analysis]] of various PHP, Python and perl (gasp!) frameworks took place in early 2010 and a selection was made.  To make a long story short, the ClearOS framework brings together the following toolkits:

  * [[http://www.codeigniter.com|CodeIgniter]] ((CodeIgniter logo, copyright and trademark EllisLab: [[http://codeigniter.com/]])) MVC PHP framework
  * [[http://jquery.com|jQuery]] Javascript Library
  * [[http://jqueryui.com|jQueryUI]] UI Widgets
  * [[http://jquerymobile.com|jQuery Mobile]] Mobile UI Widgets

===== Getting Started =====
If you are new to developing on ClearOS, the tutorials are a great place to start.  We'll take you from hello world right through to an Ajax-enabled App.

  * [[:content:en_us:dev_framework_getting_started|Getting Started]]
  * [[:content:en_us:dev_framework_tutorials_hello_world|Hello World Tutorial]] 
  * [[:content:en_us:dev_framework_tutorials_getting_started_with_clearos_on_github|Getting Started with ClearOS on GitHub]]

===== Reference Guide =====
The **[[:content:en_us:dev_framework_reference_guide_app_development_components|App Development Reference Guide]]** provides information on the following topics:

  * File System Layout
  * MVC - Model, View, Controller
  * Ajax
  * Widgets
  * Marketplace

===== Coding Standards =====
The **[[:content:en_us:dev_framework_coding_standards_start|Coding Standards]]** documentation provides information on standard practices used in ClearOS app development.

===== Security Practices =====
Best security practices have built right into the framework.  From XSS filtering to validation handling, the necessary tools are included in ClearOS.  
The Security Practices documentation provides information on:

  * XSS / cross-site scripting filter
  * CSRF / cross-site request forgery preventions
  * Input Validation
{{keywords>clearos, clearos content, dev, gettingstarted, apps, framework, maintainer_dloper}}

===== Wireless =====
The wireless module allows you to control and provision a qualified wireless networking device as an access point. The wireless card must both have network support under ClearOS as a wlan0 device and it must also support the 'master' mode configuration for wireless devices.

===== Installation =====
Currently the wireless module is installed only from command line using the following command:

  yum install app-wireless

Once installed the module will appear in Webconfig under <navigation>Network > Settings > Wireless Access Point</navigation>

===== Configuration =====
At the time of this writing the wireless app module will only provide the ability to stop and start the wireless services and will give informative information about how it is configured. To change settings you must edit the file by hand from command line:

  vi /etc/hostapd/hostapd.conf

==== WPA2 PSK mode ====
The following is an example of a valid hostapd.conf file which support WPA2 Preshared keys. This configuration assumes that the wlan interface has been added to the bridge network 'br0':

<code>
bridge=br0
interface=wlan0
driver=nl80211
hw_mode=g
channel=6
ssid= WIRELESS
auth_algs=1
ctrl_interface=/var/run/hostapd

###WPA
wpa=3
wpa_key_mgmt=WPA-PSK
wpa_passphrase=mypassword
wpa_pairwise=TKIP
rsn_pairwise=CCMP
wpa_group_rekey=300
wpa_gmk_rekey=640

# Wireless N
wme_enabled=1
ieee80211n=1
ht_capab=[HT40-][SHORT-GI-40][DSSS_CCK-40]
</code>
==== WPA2 Infrastructure mode ====
The following is an example of a valid hostapd.conf file which support connections to a the localhost RADIUS server using WPA2 Infrastructure mode.

<code>
bridge=br0
interface=wlan0
driver=nl80211
hw_mode=g
channel=6
ssid=WIRELESS
ieee8021x=1
auth_algs=1
ctrl_interface=/var/run/hostapd

###WPA
wpa=3
wpa_key_mgmt=WPA-EAP
wpa_passphrase=mypassword
wpa_pairwise=TKIP
wpa_group_rekey=300
wpa_gmk_rekey=640

# Wireless N
wme_enabled=1
ieee80211n=1
ht_capab=[HT40-][SHORT-GI-40][DSSS_CCK-40]

###Radius Setup
own_ip_addr=192.168.1.1
nas_identifier=ClearBOX-BUILT-IN
auth_server_addr=127.0.0.1
auth_server_port=1812
acct_server_addr=127.0.0.1
acct_server_port=1813
eap_server=0
rsn_pairwise=CCMP

auth_server_shared_secret=SECRETPASSWORD
acct_server_shared_secret= SECRETPASSWORD
</code>

{{keywords>clearos, clearos content, AppName, app_name, clearos6, userguide, xcategory, maintainer_dloper, maintainerreview_x,keywordfix}}

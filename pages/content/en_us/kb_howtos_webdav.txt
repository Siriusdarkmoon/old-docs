===== Webdav =====
WebDAV is a cool technology that allows you to share data securely without having to create a VPN. It can be configured to use SSL encryption to protect your data in the same way that your bank may transact your internet banking. It works with Mac, Windows, and Linux workstations. With this howto you can configure WebDAV for ClearOS 5.1 or Clarkconnect 5.0. This howto does NOT address compliance concerns for organizations who have to comply with change control, auditing, and other regulatory concerns.

WebDAV is an extension of ClearOS Web Services using Apache that allow you to access files using the HTTP protocol. For this howto, we will illustrate configuring this with HTTPS since transfering files across networks unencrypted is just silly.

===== What you will need =====
You will need the flexshares and web services modules installed on your ClearOS server. You will also need access to Webconfig and 'root' access to the command line of your ClearOS server. You should also be familiar with text editing tools for Linux.

===== Problems using WebDAV under ClearOS 5.1 and ClarkConnect 5.0 =====
Webconfig does not yet support WebDAV and will overwrite your /etc/httpd/conf.d/flex-443.ssl file each time you hit the update for the WebDAV flexshare. So if you make these changes be cognizant that this can happen. The symptom that this overwrite has occurred is that your users will be able to read files but not make changes or save new ones. If this occurs, please repeat the step below entitled 'Enable DAV in Apache.'

<note warning>Use this method only on systems where there is a high level of trust and sound password policies. Using WebDAV means that users can execute code as the user 'apache'. If you have concerns but need WebDAV, run this configuration on a stand alone system or in a virtual environement.</note>

===== Configuration =====
==== Start the Web Services ====
Make sure the Web Services are started and are configured to start automatically. <navigation>Server >> Web >> Web Server</navigation>

==== Setup a new group ====
It's a good idea to use a unique group for this flexshare because we have to assign the apache user to this group. So create a new group and add as many users to it as will be accessing this share. For my example, I call this group 'dav-group'.

==== Setup the Flexshare ====
Add a new flexshare <navigation>Server >> File and Print >> Flexshares</navigation>.

Name your flexshare (no spaces please) and give it a description (spaces ok) and assign the permissions to the group you just created. Click the Add button.

==== Enable Web Services on this Flexshare ====
Click the 'Web' tab. Change the 'Status' to enabled just above the 'Server Name' field. Change the 'Accessibility' if you want external (Internet enabled) access to this share. Enable 'Show Index'. Enable 'SSL/HTTPS'. Enable 'Authentication'.

Once these are selected click 'Update' at the bottom of the page.

After you have completed these steps, change the 'Status' for the general Flexshare at the top of the page just under the 'Name' field. Click the other 'Update' button in this section which is located next to the 'Return to Summary' button.

==== Honoring Flexshare permission structure ====
WebDAV is quite aggressive in how it addresses permissions. It will want to make all of the directories and files it modifies or creates to be stamped with Apache being the owner. This will break other flexshare protocols that may also want to share this data. To preserve the permission structure run the following (Assuming you entitled your directory 'my-share-name'):

  chmod g+s /var/flexshare/shares/my-share-name

<note>This only partially resolves the issue for honoring permissions. WebDAV will create files with the permissions of READ for the group and WRITE for itself. This means that users using WebDAV can access all the files but users using other protocols can only read the files after WebDAV has touched it.</note>

You also may want to add the following line to your crontab (crontab -e):

  *  */15  *  *  *  chmod -R g+w /var/flexshare/shares/my-share-name

==== Enable DAV in Apache ====
For this next part, you will need to log in as root to the command line of the server and edit /etc/httpd/conf.d/flex-443.ssl

  vi /etc/httpd/conf.d/flex-443.ssl

Find the entry for your Flexshare directory. If you entitled your directory 'my-share-name' it will appear as;

  <Directory /var/flexshare/shares/my-share-name>

in the file.

At the end of this section add 'dav on' as a line. Here is an example of the last 4 lines with this added:

        RemoveHandler .php
        AddType application/x-httpd-php-source .php
        dav on
  </Directory>

Now that this is added, restart the Apache Web Services with the following:

  service httpd restart

==== Making the share writeable ====
For this to work right, you will need the bind information from your /etc/kolab/kolab.conf file.

  cat /etc/kolab/kolab.conf

We will use the following for the example code to follow. You will need to modify the commands below to include the information specific to your system. In this file you will need the information contained in the following two lines:

  bind_dn : cn=manager,cn=internal,dc=system,dc=lan
  bind_pw : 5H73OlM6u8v4I9jm

At this point, you will not be able to write to the directory because the Apache Web Services does not have permission to the flexshare. To fix this, create a file called /root/apache.group.ldif.

  vi /root/apache.group.ldif

The file will contain code which will add the apache user to the group you made earlier. Here is what the file looks like and the text you should enter and modify:

  dn: cn=dav-group,ou=Groups,ou=Accounts,dc=system,dc=lan
  changetype: modify
  add: memberUid
  memberUid: apache

<note>Note that I use the group for the cn= that I specified earlier in the document</note>

Now, import the file to your ldap directory using the DN and credentials from /etc/kolab/kolab.conf (replace with your specific info)
  ldapmodify -h localhost -D "cn=manager,cn=internal,dc=system,dc=lan" \
  -x -w 5H73OlM6u8v4I9jm -f /root/apache.user.ldif

Restart the apache service for good measure:

  service httpd restart

===== Client access =====
This is all done from the server's perspective. Now you need to connect to this share using a user account that is a member of the group you created.

==== Determine the Address for your server ====
You can use the internal IP address or external IP address (if you configured the flexshare for external access) of the server. If my internal address was was 192.168.1.1 and my internal name resolution for my server was 'server1' and my external name resolution was 'server1.example.com' then I could use any of the following:

  https://192.168.1.1/flexshare/my-share-name
  https://server1/flexshare/my-share-name
  https://server1.example.com/flexshare/my-share-name

==== Mac ====
Mac OSX uses WebDAV very nicely. Open Finder or click on the desktop. <navigation>Go >> Connect to Server...</navigation>. In the address bar, type the address for the server. Mac works with WebDAV quite nicely because you can open documents from within applications easily. Nicely done Mr. Jobs.

Alternatively you can download and install [[http://cyberduck.ch/|CyberDuck]], the FTP/WebDAV client for Windows and Mac.

==== Windows ====
[[http://smallvoid.com/article/winnt-webdav-network-drive.html|This site]] has some very useful information on creating connections to WebDAV in Windows.

Windows XP can use WebDAV as a 'Network Place'. <navigation>My Network Places >> Add a network place >> Next >> Choose another network location >> Next</navigation>

<navigation>Type the URL for the flexshare >> Next >> Click View Certificate, on the certificate</navigation>

<navigation>Next >>Click, 'Install Certificate' >> Next >> Automatically select... </navigation>

<navigation>Next >>Finish >> Yes >> OK >> OK >> Yes</navigation>.

After a lot of that you will be asked for the username and password. You can select to remember the password. <navigation>Next >> Finish</navigation>



Supposedly, you can map it as a network drive using the following:

  net use m: "https://server1.example.com/flexshare/my-share-name" /User:username password /persistent:yes
  
<note>We're still testing this cause and cannot confirm that the command line method in Windows works</note>

{{keywords>clearos, clearos content, kb, howtos, maintainer_dloper, maintainerreview_x, keywordfix}}
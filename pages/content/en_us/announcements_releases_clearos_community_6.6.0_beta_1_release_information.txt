===== ClearOS Community 6.6.0 Beta1 Release Information =====
**Released: May 22, 2014**

ClearOS Community 6.6.0 Beta 1 has arrived!  Along with the usual round of bug fixes and enhancements, this release introduces Wordpress, Joomla, Tiki Wiki, WPAD and AppFirst.  You can also expect OwnCloud, SugarCRM, Vtiger and WPAD during the upcoming beta cycle.  Under the hood, the event system, IPv6 and ClearOS 7 compatibility have also been a focus of this release.

Some of the new apps in the beta are **NOT** available via the Marketplace but can be installed via **yum**.  You can find installation and app details further below.

<note>If you have not already done so, please review the [[:content:en_us:announcements_releases_test_releases|introduction to test releases]].</note>

===== What's New in ClearOS Community 6.6.0 =====
The following is a list of features coming in ClearOS Community 6.6.0.

  * Wordpress
  * Joomla
  * Tiki Wiki
  * OwnCloud Home
  * QoS
  * YouTube School ID support
  * AppFirst
  * WPAD (paid app)

For businesses and organizations, ClearOS Professional 6.6.0 also includes:

  * OwnCloud Business
  * SugarCRM Community
  * Vtiger

The full changelog can be found here:

  * [[http://tracker.clearfoundation.com/changelog_page.php?version_id=49|Changelog - ClearOS 6.6.0 Beta 1]]


===== Feedback =====
Please post your feedback in the [[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,39/func,showcat/|ClearOS Development and Test Release Forum]]. 


===== Download =====
==== ISO Images ====
^Download^MD5Sum^
|[[http://images.clearfoundation.com/clearos-images/community/6.6.0/images/iso/clearos-community-6.6.0-beta-1-i386.iso|32-bit]]|dcc65846b2bc9f0a7b1723a92e82c590|
|[[http://images.clearfoundation.com/clearos-images/community/6.6.0/images/iso/clearos-community-6.6.0-beta-1-x86_64.iso|64-bit]]|d344123335eebc4f75175298f5acf8ba|

==== Virtual Machine and Cloud Images ====
Virtual Machine and Cloud Images are not available for this beta.

==== Upgrade from Earlier ClearOS Community 6.x Releases ====
To upgrade an existing system to ClearOS 6.6.0 Beta 1, you can run the following command:

  yum --enablerepo=clearos-updates-testing upgrade
  

===== Beta Testing Details =====
==== WordPress ====
You can now install and start running a [[http://www.wordpress.com|WordPress]] site in just a few clicks.  To install WordPress, run:

  yum install app-wordpress

==== Joomla ====
You can now install and start running a [[http://www.joomla.org|Joomla]] site in just a few clicks.  To install Joomla, run:

  yum install app-joomla

==== Tiki Wiki ====
You can now install and start running a [[http://www.tiki.org|Tiki Wiki]] site in just a few clicks.  To install Tiki Wiki, run:

  yum install app-tiki

==== WPAD ====
WPAD provides automatic proxy detection and configuration support.  This paid app is available as a free trial.  Please contact sales@clearcenter.com for more information.

==== AppFirst ====
[[http://www.appfirst.com|AppFirst]] is a cloud-based network and server monitoring service that delivers unified visibility from many data sources.  The ClearOS AppFirst app is an integration from ClearCenter and available via the Marketplace.
{{keywords>clearos, clearos content, announcements, releases, clearos6.6, beta1, currenttesting, maintainer_dloper}}

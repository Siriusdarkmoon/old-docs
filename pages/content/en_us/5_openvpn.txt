===== OpenVPN =====
The OpenVPN server is a secure and cost effective way to provide road warrior VPN connectivity. The OpenVPN client is available at no cost.  Unlike the [[5_pptp_vpn|PPTP VPN server]], OpenVPN is more robust in getting through other firewalls and gateway.  

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:

<navigation>Network|VPN|OpenVPN</navigation>

===== Configuration =====

==== Configuring the Server ====
=== Organization Information ===
Before you can configure OpenVPN, you may be directed to the [[:content:en_us:5_organization|Organization]] configuration page in webconfig.  The information is required to create the necessary security certificates used in OpenVPN.

=== Domain ===
The default domain used by the OpenVPN client, for example example.com.

=== WINS Server ===
The Microsoft Networking WINS server used by the OpenVPN client. Depending on your network configuration, you may need to specify the WINS settings in VPN client configuration.  If you are using ClearOS as your WINS server, please specify the LAN IP address of the ClearOS system.

=== DNS Server ===
The DNS server used by the OpenVPN client.  If you are using ClearOS as your DNS server, please specify the LAN IP address of the ClearOS system.

=== Manage User Accounts ===
Users must be configured with OpenVPN access. To manage users, go to the [[:content:en_us:5_users|Users]] page in webconfig.  When a user is created, a certificate key/pair that is required for the OpenVPN system is automatically created.

==== Configuring the Client ====
To configure the Windows OpenVPN client:

  * Download and install the client software -- [[http://www.openvpn.net/index.php?option=com_content&id=357|download page]]
  * Login to webconfig as the OpenVPN user (not the root/administrator account!) 
  * Go to the <navigation>Directory|My Account|Security and Keys</navigation> in the menu system.  You will need to download the certificate, certificate authority, key and the OpenVPN configuration file into the //configuration// directory on your Windows system. 

{{keywords>clearos, clearos content, OpenVPN, app-openvpn, clearos5, userguide, categorynetwork, subcategoryvpn, maintainer_dloper}}

===== Linode =====
To add a Linode cloud, you'll need a valid API key. To get it, login to  your account in Linode and select 'my profile'. After you provide your password that is asked for confirmation, click on "Api Keys" and create a new API key. 

[image]

To use Linode with ClearGLASS, login to  https://clear.glass and click on the 'Add cloud' button at the bottom right

Select Linode from the provider’s list and paste your Linode API key.

[image]

Once you click on Add, ClearGLASS will try to authenticate with Linode. If all goes well, you should see your VMs listed in the Machines section.

==== Related Articles ====
  * [[content:en_us:cg_setting-up-clearglass|Quick Start Guide]]
  * [[content:en_us:cg_monitoring-on-linux|Setting up monitoring on Linux]]
  * [[content:en_us:cg_manage-infrastructure|Managing Infrastructure]]
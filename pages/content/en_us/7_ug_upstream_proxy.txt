===== Upstream Proxy =====
The **Upstream Proxy** app makes it possible to connect ClearOS to the Internet via upstream proxies.  This type of proxy connection is typically provided by Internet Service Providers (ISPs).
===== Installation =====
This app is currently in beta.  Once released, it will be included in the base install.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Network|Settings|Upstream Proxy</navigation>

===== Configuration =====
{{:omedia:upstream_proxy.png?440|ClearOS Upstream Proxy}}  The **Proxy Server** and **Port** are required for upstream proxies.  Typically, the port number is 3128, but check with your proxy server provider for details.

A **Username** and **Password** are sometimes required as well.  If so, please specify these in the configuration screen.  If not, please leave these fields blank.

If you ever need to delete the upstream proxy configuration, just leave all the fields blank and submit the changes.

===== Links =====
  * [[:content:en_us:6_ip_settings|IP Settings]]
{{keywords>clearos, clearos content, AppName, app_name, clearos7, userguide, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

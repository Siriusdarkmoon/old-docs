===== ClearOS Professional 6.4.0 Final Release Notes =====
**Released: April 11, 2013**

ClearOS Professional 6.4.0 has arrived!  Along with the usual round of bug fixes and enhancements, this release introduces a new reports engine, a storage manager, an antimalware file scanner, a basic POP/IMAP server, RADIUS, and mail retrieval (fetchmail). 

===== What's New in ClearOS Professional 6.4.0 =====
The following is a list of new features in ClearOS Professional 6.4.0.

  * [[http://www.clearcenter.com/support/documentation/user_guide/filter_and_proxy_report|Filter and Proxy Report]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/resource_report|Resource Report]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/network_report|Network Report]]
  *[[http://www.clearcenter.com/support/documentation/user_guide/storage_manager|Storage Manager]] (Amazon EC2 images only)
  * [[http://www.clearcenter.com/support/documentation/user_guide/mail_retrieval|Mail Retrieval]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/imap_and_pop_server|Basic POP/IMAP Server]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/antivirus_file_scan
|Antimalware File Scan]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/radius_server
|RADIUS Server]]
  * Amazon EC2 Images

There have also been a couple of apps already released since ClearOS 6.3.0:

  * [[http://www.clearcenter.com/support/documentation/user_guide/smart_monitor|SMART Monitor]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/dropbox|Dropbox]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/backuppc|BackupPC]]

===== Download =====
Please see the [[http://www.clearcenter.com/Software/clearos-professional-downloads.html|Downloads page]] for download information.

===== Upgrade from ClearOS Professional 6.3.0 and Earlier =====
ClearOS follows the same release pattern as upstream.  There is really only a single "ClearOS 6" version - the minor releases are just snapshots in time with bug fixes, enhancements, etc.  If your system has automatic updates enabled, your ClearOS system will have transitioned to ClearOS 6.4.0.  For those with automatic updates disabled, upgrading is as simple as running:

  yum upgrade


===== ClearOS 5.2 Differences =====
For ClearOS 6, the barrier for having an app available in the web interface was raised.  If the quality or features in ClearOS 5.2 were not up to the standards of a modern small business server/gateway, the feature was either upgraded or deprecated.

==== Zarafa for ClearOS ====
The default groupware and mail solution is now based on [[http://www.zarafa.com|Zarafa]] technology.

==== Horde/Kolab ====
Horde/Kolab has been deprecated and is no longer supported.  This policy may change in a future ClearOS 6 release.

==== Advanced Firewall ====
The Advanced Firewall app was marked as deprecated in version 5.2.  It is no longer available.  The [[http://www.clearcenter.com/support/documentation/marketplace/custom_firewall|Custom Firewall]] can be used as an alternative.

==== Administrators ====
The Administrators app does not fit into the group-based policy engine in ClearOS 6.  The underlying mechanism that existed in ClearOS 5 has been ported to version 6, but it requires command line configuration. 

==== IPsec ====
The [[http://www.clearcenter.com/Services/clearsdn-dynamic-vpn-6.html|Dynamic VPN]] app provides advanced IPsec support in ClearOS Professional.  The unmanaged IPsec tool has been replaced by the 3rd party [[http://www.clearcenter.com/support/documentation/user_guide/static_ipsec_vpn|Static IPsec VPN]] app.  The new app is much more powerful than what was provided in ClearOS 5.x!

==== Mail Archive ====
The [[http://www.clearcenter.com/support/documentation/clearos_enterprise_5.2/user_guide/mail_archive|Mail Archive]] module in 5.x has not been converted over to the new 6.x framework.  This app will be re-introduced into the ClearOS Marketplace in the future.  The Zarafa Professional Edition (ClearOS Professional Edition required) has [[http://www.zarafa.com/content/zarafa-archiver|Mail Archiving]] included for up to 20 users.  For additional mail archive 'seats', please contact Zarafa sales.

{{keywords>clearos, clearos content, announcements, releases, clearos6.4, final, previousfinal, maintainer_dloper}}


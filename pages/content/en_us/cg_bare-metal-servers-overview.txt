===== Bare-metal servers - Overview =====
ClearGLASS supports bare-metal servers. This means you can use ClearGLASS to manage bare-metal servers that reside in your private data center or with a service provider.

For on-demand bare metal provisioning, ClearGLASS already supports Packet.net and Vultr. Here are instructions for adding bare-metal servers to ClearGLASS:

==== Step 1 ====
Use the Add Cloud widget to add a bare-metal server. Select the Other Server option from the drop down

[image]

==== Step 2 ====
Complete the Add Cloud form. You will need to input a Title, HostName, operating system, and SSH Key. If you add an SSH Key to the server, you can use ClearGLASS monitoring and alerting, scripting, Ansible, and orchestration features.

Also, if the bare-metal server resides on a private network, a virtual private network will need to be setup to access the server. ClearGLASS provides a VPN service based on OpenVPN; learn more here: Tunnel.

[image]

When the server has been added to ClearGLASS, it will appear in the Machine page, alongside your other public and private infrastructure.

[image]

The benefit to using ClearGLASS to manage your bare-metal infrastructure is that you now can see and manage your public cloud and your bare-metal servers from one console.

==== Step 3 ====
You can manually set prices for your bare-metal servers so that they appear in the cost reporting widget. Now you can see your public cloud costs right next to your private or dedicated server costs, providing you a single view of all your hybrid cloud infrastructure costs.

[image]
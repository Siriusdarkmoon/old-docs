===== How do I reset my password =====
Updating your password regularly is something we strongly encourage you to do so. ClearGLASS lets you make this in a matter of seconds. First go to the dropdown menu on the top right corner of our platform and select "Account".

[image]

Afterwards open the Password tab and fill in the form by inserting your old password and the new one twice for verification purposes.

[image]

In case you have created an account by using social authentication then the procedure will be slightly different. This will be the first time you are going to register a password for your account so once you open the "Security" collapsible you will have to click the "Set Password" button and then follow the procedure.

Of course this procedure will be followed only for the very first time you want to add a password in case you have created an account with social authentication. From now on you are free to reset your password whenever you like.
===== Task Scheduling Example =====
ClearGLASS allows you to dynamically scheduled tasks and actions on your machines. Actions include start, stop, reboot, and destroy, while tasks are executable scripts/programs or ansible playbooks. Tasks and actions can be repeatable and can run on multiple machines simultaneously. 

You can schedule a script or action to run on specific machines or tags. 

==== Example ====

To schedule a script to run backups of your database, simply add a script to ClearGLASS, then visit the Schedules section on the sidebar menu. Fill in a name and optionally a description for your schedule and on the Task drop down, choose Run Script. 

[image]

On the Script drop down choose your backup script.

[image]

You can optionally pass any parameters on the script on the Parameters section. Next, choose the machines.  

[image]

or choose a group of machines by using a tag. In this case, we want the script to run on every machine that has the DB tag, as we want to be able to quickly spin or remove new machines to our database cluster without having to edit our task every time.

[image]

Now, you can define how often the task will be repeated. You can specify the interval in days, hours or minutes, or use a crontab format for a more detailed control. In this case, we want the script to run once per day.

[image]

Finally, you can specify when the task will take effect and if and when you want it to expire. You can also specify a maximum number of times that you want the script to run. In this case, we want it to start immediately and never expire as our databases will be left without backups.

[image]

That's it. Click Add and you will see an overview of the task and the affected machines.

[image]

==== Schedule actions on new servers ====
A scheduler task can be added when ClearGLASS deploys a machine through the create machine wizard. While creating the server you can select if a scheduler task will be created. In our example, we schedule the deletion of a machine at a specific time. 

[image]

Scheduling a destroy machine action

[image]

//Selecting the date time//
===== Manual GRUB install of Bootloader =====
Linked from: [[:content:en_us:7_c_configuring_partitions_and_raid|Configuring Partitions and RAID]]

We are almost done with the software RAID configuration.  Next, the installation wizard will ask for the boot loader settings.  

  * Select GRUB as your boot loader 
  * Disable the boot password (unless you really need it)

<note warning>If have trouble booting up your system with GRUB, you can use the LILO boot loader as an alternative.  However, you will need to type the following on the first installation screen: linux lilo.</note>

If the secondary disk fails (/dev/sdc), then the system will still be bootable.  If the primary disk fails (/dev/sda), then your system will not boot.  In order to make the secondary disk bootable as well, run the following command:
  
  grub-install /dev/sdc

{{keywords>clearos, clearos content, clearos7, userguide, categoryinstallation, subcategoryinstaller, subsubcategoryfullinstalliso, maintainer_dloper}}

{{ :userguides:mobile_demo.svg?80}}
<WRAP clear></WRAP>

===== Mobile Demo for Android =====
This application demonstrates how to access the ClearOS API framework from a mobile phone using a REST-like protocol.  IT IS INTENDED FOR DEVELOPERS ONLY.  The bundled application is for Android[TM] devices, but could be easily adapted to support iOS[TM] platforms.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/system/mobile_demo|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_mobile_demo|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_mobile_demo|here]].
==== Additional Notes ====

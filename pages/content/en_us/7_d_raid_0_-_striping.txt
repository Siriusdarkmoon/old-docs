===== RAID 0 - Striping =====
Linked from: [[7_c_configuring_partitions_and_raid|Configuring Partitions and RAID]]

===== Preparing the Hardware =====
For software RAID 0, you need at least two hard disks.  Since the RAID partition on both the hard disks must be of equal size, it is a good idea to use hard disks with (roughly) the same storage capacity.

In our example, we are using two SATA disks.  These hard disks are detected in Linux as:

  * /dev/sdb
  * /dev/sdc

For purposes of this demonstration we will assume that /dev/sda is used for the system partition, swap, and boot partition.

===== Deleting Partitions =====
Please refer to [[:content:en_us:7_deleting_partitions|Deleting Partitions]] if you need assistance in removing existing partitions.

===== Creating the RAID 0 Partition =====
We will create a RAID 0 volume for our MySQL server using /var/lib/mysql as our mount point. This will allow our database server to optimize for speed.

{{:omedia:ss_anaconda_raid5-menu.png?350 Partition RAID}}

  * Tab to the **New** button and hit return

{{:omedia:ss_anaconda_raid5-drive1.png?350 Partition RAID}}

  * Tab down to **File System type** and select **software RAID**
  * Tab to **Allowable Drives** and mark only sdb and take the mark off of all the other drives.
  * Tab down to radio buttons and select 'Fill all available space'
  * Tab down to **OK** and hit return.

Repeat the same process for sdc and, be sure to mark the correct **allowable drive** and take the marks off of all the other drives.

{{:omedia:ss_anaconda_raid0-ready-for-raid.png?350 Partition RAID}}

Now that we should have two identical configurations, we can create the software RAID disk:

{{:omedia:ss_anaconda_raid0-raid-create.png?350 Partition RAID}}

  * Tab to the **RAID** button and hit return
  * Type in /var/lib/mysql in the **Mount Point** field
  * Tab to **RAID Level** and select RAID0
  * Tab to **RAID Members** and make sure the two partitions created earlier are selected

{{:omedia:ss_anaconda_raid0.png?350 Partition RAID}}

===== Configuring the Boot Loader =====
Once completed, click OK and continue through the boot loader installation process.  Please refer to [[:content:en_us:7_boot_loader|Boot Loader]] document for if you require help with this step.

===== Links =====
  * [[:content:en_us:7_c_configuring_partitions_and_raid|Configuring Partitions and RAID]]
  * [[:content:en_us:7_d_raid_1_-_mirroring|RAID 1 - Mirroring]]
  * [[:content:en_us:7_d_raid_5_-_redundancy|RAID 5 - Redundancy]]
  * [[:content:en_us:7_d_raid_6_-_extra_redundancy|RAID 6 - Extra Redundancy]]
{{keywords>clearos, clearos content, clearos7, userguide, categoryinstallation, subcategoryinstaller, subsubcategoryfullinstalliso, maintainer_dloper}}

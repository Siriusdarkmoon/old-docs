{{ :userguides:flexshare.svg?80}}
<WRAP clear></WRAP>

===== Flexshare =====
A Flexshare is a flexible and secure collaboration utility which integrates four of the most common methods of accessing files or content - Web, FTP, File sharing (SAMBA) and Email (as attachments).

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/server/flexshare|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_flexshare|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_flexshare|here]].
==== Additional Notes ====

===== Intrusion Detection =====
The intrusion detection package is included with ClearOS to make users more aware of some of the daily hostile traffic that can pass by your Internet connection. The software is able to detect and report unusual network traffic including attempted break-ins, trojans/viruses on your network, and port scans.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Gateway|Intrusion Protection|Intrusion Detection</navigation>

===== Intrusion Protection Updates and ClearSDN =====
{{ :omedia:clearsdn-icon-xxs.png}} The ClearSDN [[http://www.clearcenter.com/Services/clearsdn-intrusion-protection-4.html|Intrusion Protection Updates]] service provides weekly signature updates to improve the effectiveness of the intrusion detection system.  These signatures are compiled from third party organizations as well as internal engineering resources from ClearCenter.  We keep tabs on the latest available updates and fine tune the system so you can focus on more important things.  

===== Configuration =====
The intrusion detection system includes a daily report.  Do not panic when you see alerts in this daily report.  In fact, it would be quite unusual //not// to see anything reported.  Hostile traffic is a normal part of today's Internet and it is one of the reasons firewalls are necessary.

<note warning>Intrusion detection does require some horsepower. If you find your system sluggish, you might want to consider disabling the software.</note>

==== Security and Policy Rules ====
There are two different types of rules for the intrusion detection system.  The **Security** rules detect issues related to overall system security, while **Policy** rules detect issues related to your organization's Internet usage policies.  For example, the //chat// policy rules will detect instant messaging traffic that goes through your ClearOS system.

===== Links =====
  * [[http://www.snort.org/|Snort Intrusion Detection website]]
{{keywords>clearos, clearos content, Intrusion Detection, app-intrusion_detection, clearos5, userguide, categorygateway, subcategoryintrusionprotection, maintainer_dloper}}

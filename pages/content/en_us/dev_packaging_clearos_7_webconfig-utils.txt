===== webconfig-utils =====
The webconfig-utils package contains simple tools and PHP modules for webconfig.

  * ifconfig module - a network helper
  * statvfs module - a file help
{{keywords>clearos, clearos content, dev, pacaging, version7, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

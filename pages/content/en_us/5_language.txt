===== Language =====
The **Language** feature is used to change the language used in [[:content:en_us:5_webconfig|Webconfig]] and other ClearOS modules that support internationalization.

===== Installation =====
This feature is part of the core system and installed by default.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>System|Settings|Language</navigation>

===== Configuration =====
Keeping this page simple and easy to find is important since you may have to look for it in a foreign language!  Simply select your language and click on <button>Update</button>
{{keywords>clearos, clearos content, Language, app-language, clearos5, userguide, categorysystem, subcategorysystemsettings, maintainer_dloper}}

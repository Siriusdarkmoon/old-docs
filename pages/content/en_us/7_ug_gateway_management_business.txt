===== Gateway Management - Business =====
==== Product Overview and Availability ====
Gateway.Management is a next-generation content filter and Internet gateway designed with a purposeful architecture to maximize on-premise performance with cloud-based intelligence. DNS, as a fundamental building block of the Internet, is at the core of this software along with strategic and dynamic firewall rules designed to allow a homeowner, a business owner, or an enterprise IT team to enforce desired policies without the use of proxies, SSL inspection, or other traditional techniques.

Gateway Management Business gives you granular control over each user’s Internet experience.  The following features can be applied network wide or on a device by device basis:
 
  * Block inappropriate content
  * Block phishing and identify theft attempts
  * Block ads (even on mobile devices connected to WiFi)
  * Block behavioural profiling
  * Manage shared and custom lists
                     
To maintain speed, Gateway Management Business performs all filtering on your ClearOS box. It is controlled with a Cloud based interface.  For extended information see the [[https://gateway.management|Gateway.Management website]] for more details.

<div classes #id width uk-grid :language>
<div classes #id width uk-width-medium-3-1 :language>[[https://gateway.management/wp-content/uploads/2017/09/Product-Overview-and-Availability.pdf|{{content:en_us:7_ug_gatewaymanagement_1.png?200|Click Link below for PDF}}]]<div classes #id width pdflink :language>[[https://gateway.management/wp-content/uploads/2017/09/Product-Overview-and-Availability.pdf|PDF]]</div>
</div><div classes #id width uk-width-medium-3-2 :language>{{ youtube>large:ZOWpNPAdfLI | Product Overview and Availability }}</div>
</div>

<note>For Gateway Management to be effective, it must be the DNS server for for your LAN devices. If you have a separate DNS server on your LAN, any devices using the separate DNS server for their queries will not be protected by Gateway Management.\\
If you want to run a separate DNS server on your LAN and have Gateway management protect the LAN, only ClearOS should point to it. Also the ClearOS DHCP server should hand out its own LAN IP as the DNS server to the clients.</note>

<note warning>Please note that the Proxy/Content Filter and Gateway Management apps are mutually exclusive. If you have the Proxy/Content Filter running, you should not use Gateway Management and vice-versa</note>
===== Getting Started =====
==== Procurement, Introduction and Pre-installation ====
Before you begin the installation, you will want to document the following items that will help you with your deployment and configuration:

  * Document all your internal domains on your local area network(s) (eg. mycompany.local)
  * Document all external domains for the purpose of split DNS administration. (eg. remote.example.com)
  * Document your current DHCP range(s) and statically assigned devices (eg. 192.168.5.100-200) 
  * Document all your reverse DNS zones (eg. 5.168.192.in-addr.arpa)
  * Document your internal DNS servers (eg. 192.168.5.2, 192.168.5.3)
  * Document all your internal Active Directory and Samba Directory Servers if you have them (eg. 192.168.5.4, 192.168.5.5)
  * Document any proxy servers in use (if any)
  * List all your core business applications which require Internet access (eg. accounting, banking, and applications with their own Internet update mechanisms)
  * Verify you have layer 2 visibility of the machines under management from the gateway devices
  * Document your IPv4 and IPv6 configurations and setting on your network.

<div classes #id width uk-grid :language>
<div classes #id width uk-width-medium-3-1 :language>[[https://gateway.management/wp-content/uploads/2017/09/Introduction-pre-installation-survey-and-procurement.pdf|{{content:en_us:7_ug_gatewaymanagement_2.png?200|Click Link below for PDF}}]]<div classes #id width pdflink :language>[[https://gateway.management/wp-content/uploads/2017/09/Introduction-pre-installation-survey-and-procurement.pdf|PDF]]</div>
</div><div classes #id width uk-width-medium-3-2 :language>{{ youtube>large:hC7voT23l6o | Procurement, Introduction and pre-installation }}</div>
</div>

===== Installation and deployment =====
To deploy this app you will need to make arrangements with your IT staff to decommission certain services and functions that may be running. The following items should be done to accomplish the transition to Gateway.Management:

  * If this is an additional gateway, you will want to stop the DHCP services on the original gateway.
  * Start the DHCP server on the same server that is running Gateway.Management assuming that this is the new, single gateway for your network. If this does not describe your network configuration, you may need to contact support or visit the forums for assistance and recommendations.
  * Install and activate the software. Follow the steps below for finding the app in the Marketplace and navigating to that app. Once the installation is complete, enable the app and then start the app. Unless the IP address listed in the 'Block Page IP' conflicts with network resources in your LAN, or Enterprise, please use that IP listed by default. If you need to change it, you may need to contact technical support.
  * Check the email account on record if this is your first installation. Then validate the software status and verify that your gateway device is online at the [[https://dashboard.gateway.management/dashboard/signin|Gateway.Management portal]]
  * Run through the Quality Assurance steps which include:
    * Edit the list for your device to ensure that you can view all logs
      * Confirm this with the [[http://mytools.management|Who Am I tool]]
      * Verify that the dashboards are links in the log files
    * Verify that the 'MyBox' tool links back to your device locally. (eg. https://192.168.5.1:81)
    * Validate an end-user device
      * Make sure that the MAC address is correct. If not, you likely don't have layer 2 visibility to all your devices from your gateway.


Plan on a maintenance window of 30-60 minutes for the deployment and initial configuration of this app.

<div classes #id width uk-grid :language>
<div classes #id width uk-width-medium-3-1 :language>[[https://gateway.management/wp-content/uploads/2017/09/Installation-and-deployment.pdf|{{content:en_us:7_ug_gatewaymanagement_3.png?200|Click Link below for PDF}}]]<div classes #id width pdflink :language>[[https://gateway.management/wp-content/uploads/2017/09/Installation-and-deployment.pdf|PDF]]</div>
</div><div classes #id width uk-width-medium-3-2 :language>{{ youtube>large:Hb0A5sZIGpU | Installation and deployment }}</div>
</div>

==== Downloading and Installing the App from the Marketplace ====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

When installed and configured, an email will be sent to the account of record for the registration of your ClearOS server. If you are unsure about what email address this is, [[https://secure.clearcenter.com/portal/profile.jsp|please log into your ClearSDN portal for your account.]]

==== Finding the App in the Menu ====
You can find this feature in the menu system at the following location:
 
<navigation>Gateway | Filtering | Gateway Management Business</navigation>

{{content:en_us:7_ug_gatewaymanagement_marketplace_2.png?550|Gateway Management in the Marketplace}}

==== Configuration ====
Once you have Gateway Management Business installed on the server, it is time to configure and initialize the service.

{{content:en_us:gm_setup.png?500|Setup}}

<note warning>Note that some features cannot be enforced in Standalone mode. For example, in Gateway mode firewall rules are used to ensure that Gateway Management cannot be bypassed by changing a device's DNS server. In Standalone mode this cannot be enforced. The recommended configuration is to use this app in Gateway mode under ClearOS.</note>

=== Box ID ===
The Box ID is a unique identifier used by the Gateway Management cloud system. You may need this information if you re-provision your ClearOS system.

=== Available LAN IP for Block Page ===
When a web page is blocked, Gateway Management displays a block page. This allows you to select from which IP that page will be displayed. This IP must NOT be on the same subnet as the LAN interface and it must not be in use. You typically want to use the IP address given by default unless that address lies in a range already used by your environment.
 
==== Logging into the Gateway Management Dashboard ====
The Gateway Management dashboard controls all of your related settings.
 
After completing the setup, visiting the Gateway Management Business page in your ClearOS web-based configuration tool will give you a link to open your cloud dashboard.

{{:content:en_us:gm_cloud_link.png|Dashboard}}

You can also visit https://dashboard.gateway.management directly.

===== Using the Web at Gateway.Management =====
==== Lists, Rule Sets and Devices ====
Key to the management of your new gateway's management is the use of policies that you will create. These lists include authoritative lists, black lists, white lists, and rainbow lists. The following order of operations exists for all policies:

  * Authoritative
  * Rainbow
  * Black
  * White

Once a match has been achieved, the other policy rules are disregarded for a particular DNS request.

<div classes #id width uk-grid :language>
<div classes #id width uk-width-medium-3-1 :language>[[https://gateway.management/wp-content/uploads/2017/09/Lists-Rule-Sets-and-Devices.pdf|{{content:en_us:7_ug_gatewaymanagement_4.png?200|Click Link below for PDF}}]]<div classes #id width pdflink :language>[[https://gateway.management/wp-content/uploads/2017/09/Lists-Rule-Sets-and-Devices.pdf|PDF]]</div>
</div><div classes #id width uk-width-medium-3-2 :language>{{ youtube>large:qKstHohONO0 | Lists Rule Sets and Devices }}</div>
</div>

==== Don't Talk to Strangers - Feature ====
One important additional security feature provided by Gateway.Management is the "Don't Talk to Strangers" feature (or DTTS.) This feature makes it so that connections that do not have a DNS resolution are proactively blocked by the gateway. This means that a number of applications and services that try to circumvent DNS (like many Bittorrents, unauthorized VPNs, botnets, et al.) will be blocked.

To create bypass rules, look for the DTTS Bypass feature.

<div classes #id width uk-grid :language>
<div classes #id width uk-width-medium-3-1 :language>[[https://gateway.management/wp-content/uploads/2017/09/Don%E2%80%99t-Talk-To-Strangers-DTTS.pdf|{{content:en_us:7_ug_gatewaymanagement_5.png?200|Click Link below for PDF}}]]<div classes #id width pdflink :language>[[https://gateway.management/wp-content/uploads/2017/09/Don%E2%80%99t-Talk-To-Strangers-DTTS.pdf|PDF]]</div>
</div><div classes #id width uk-width-medium-3-2 :language>{{ youtube>large:fyDCcH_Gkpw | Don’t Talk To Strangers (DTTS) }}</div>
</div>


==== Customization and Tailoring ====
The more you customize your environment the more difficult it will be for internet abusers to circumvent or exploit your network.

<div classes #id width uk-grid :language>
<div classes #id width uk-width-medium-3-1 :language>[[https://gateway.management/wp-content/uploads/2017/09/Customization-and-Tailoring.pdf|{{content:en_us:7_ug_gatewaymanagement_6.png?200|Click Link below for PDF}}]]<div classes #id width pdflink :language>[[https://gateway.management/wp-content/uploads/2017/09/Customization-and-Tailoring.pdf|PDF]]</div>
</div><div classes #id width uk-width-medium-3-2 :language>{{ youtube>large:kvGIm-wSwgg | Customization and Tailoring }}</div>
</div>

=== Active Directory Considerations ===
If you are using Active Directory in your environment, you will want to customize and tailor the mechanisms of your DNS resolution to properly configure Gateway.Management and AD DNS to work together.

Please see this [[https://www.dnsthingy.com/support/active-directory-configuration/|DNSThingy]] support article for setting up Gateway management in an Active Directory domain.

Please see the [[:content:en_us:7_ug_gateway_management_business&#no_internet_rule_set_with_active_directory_domains|note below]] about the **No Internet rule** in an AD environment.
=== Customizing Device names ===
You can edit the device names of devices behind your gateway to make them more manageable and to increase visibility in reporting. In the Devices section, click the 'edit' button to manage individual devices.

You can also add devices manually if layer 3 discovery is not in place. Simply add the device.

=== Tags ===
Tags can be used to group larger networks for more efficient management

=== Block Page Assistant ===
The [[https://chrome.google.com/webstore/detail/block-page-assistant/pkimhjnhalcimiegkknnidjmmoiedhon|Block Page Assistant]] is a plugin for Google Chrome that allows your secure browser to give a descriptive block message instead of a security warning.

=== Unblocking ===
Users who are blocked can request unblocks from their block page. These will queue in your 'Unblock Requests' page in the Gateway.Management portal. You can manually or automatically whitelist the requests.

You can only auto-whitelist as long as the page is:

  * Listed under Google SafeBrowsing
  * In an acceptable Category site
  * Is not adult content
  * Is of known good reputation

=== Managers ===
You can add Managers so that additional people can manage your whitelists and unblock requests can be processed by multiple trusted individuals.

=== Additional Resources ===

  * [[http://mytools.management|MyTools.Management]] - Starting point for end users and new admins to the system

=== Customized Block Page ===
You can customize your block page in the interface. Don't forget to whitelist your block page!!!

==== No Internet Rule Set with Active Directory Domains ====
This is a somewhat draconian rule and will disable Rainbow lists. If you have an Active Directory environment, it probably also cut off your Intranet including shares and anything which needs the AD DNS server for name resolution. It will, therefore, stop logging onto an Active Directory domain as well.

It may be more appropriate to:
  * Create a new Rule Set of the Whitelist type
  * Add your Domain to the list and possibly your reverse domain (e.g. 1.168.192.in-arpa)
  * Enable any Rainbow rule for Active Directory Domain Controllers.
  * Assign your devices to this Rule Set rather than the No Internet rule set
===== Help =====
  * [[https://gateway.management/wp-content/uploads/2017/09/Product-Overview-and-Availability.pdf|Product Overview and Availability]]
  * [[https://gateway.management/wp-content/uploads/2017/09/Introduction-pre-installation-survey-and-procurement.pdf|Procurement, Introduction and pre-installation]]
  * [[https://gateway.management/wp-content/uploads/2017/09/Installation-and-deployment.pdf|Installation and deployment]]
  * [[https://gateway.management/wp-content/uploads/2017/09/Lists-Rule-Sets-and-Devices.pdf|Lists, Rule Sets and Devices]]
  * [[https://gateway.management/wp-content/uploads/2017/09/Don’t-Talk-To-Strangers-DTTS.pdf|Don’t Talk To Strangers]]
  * [[https://gateway.management/wp-content/uploads/2017/09/Customization-and-Tailoring.pdf|Customization and Tailoring]]
===== Links =====
  * [[https://gateway.management/#gatewayfeatures|Features]]
  * [[https://gateway.management/#gatewayfaq|FAQ]]
  * [[https://dashboard.gateway.management/dashboard/signin|Sign In to Gateway.Management]]

{{keywords>clearos, clearos content, gateway.management, app-gateway-management-business, clearos7, userguide, categorygateway, subcategoryfiltering, maintainer_dloper}}

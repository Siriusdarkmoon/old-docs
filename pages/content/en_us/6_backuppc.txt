===== BackupPC =====
BackupPC is a module for ClearOS that allows you to backup Windows and Linux workstations using your ClearOS server as the data storage and manager for those backups.

===== Installation =====
The BackupPC app is installed from the ClearCenter [[:content:en_us:6_Marketplace|Marketplace]]. Upon installation you can find the app in the <navigation>Server > Backup > BackupPC</navigation>

===== Configuration =====
You will need to start the BackupPC service before it will be useful. Click **Start** in the information section on the right.

You will also need to specify a password for the administration of BackupPC.

Once these things are set, click the **Go to BackupPC Management Tool** button to launch a window for the administration of BackupPC.

==== Online Manual ====
The online manual will help you properly administer this great open source tool. You can access it here:

  * [[http://backuppc.sourceforge.net/faq/BackupPC.html|BackupPC Online Manual]]

===== Storage =====
BackupPC will store the data in the folder "/var/lib/BackupPC". If you would like to store the data in a different volume, please reference the [[http://www.clearfoundation.com/docs/developer/architecture/centralized_user_data/start|Centralized User Data model]] for ClearOS and use Bind mounts to place this data in that other storage.

To migrate the framework perform the following after your volume is available under /store/dataX (in this example we will use /store/data0):

  yum -y install rsync
  mkdir /store/data0/live/server1/backuppc
  chown backuppc /store/data0/live/server1/backuppc
  service backuppc stop
  rsync -av /var/lib/BackupPC/. /store/data0/live/server1/backuppc/.
  rm -rf /var/lib/BackupPC/*

Add the backuppc bind mount to /etc/fstab using your favorite editor:

  /store/data0/live/server1/backuppc                /var/lib/BackupPC   none bind,rw 0 0

Mount and validate that BackupPC is mounted in the correct location.

  mount /var/lib/BackupPC/ && mount |grep BackupPC

It should return:

  /store/data0/live/server1/backuppc on /var/lib/BackupPC type none (rw,bind)

Start BackupPC in webconfig or by the following:

  service backuppc start

===== Clients =====
==== Windows ====
[[http://sourceforge.net/projects/backuppc/files/cygwin-rsyncd/|Download]] the rsyncd package from the BackupPC project page. Download the zip package and extract it to the C:\rsyncd directory on the workstation.

Open and edit the C:\rsyncd\rsyncd.conf. You can edit the file how you want or you can replace it with this file and modify appropriately (you can also backup this file as a reference an then just replace it with this).

<code>
use chroot = false
max connections = 1
pid file = c:/rsyncd/rsyncd.pid
lock file = c:/rsyncd/rsyncd.lock

[c_drive]
 path = c:
 comment = Entire C: Drive
 auth users = backuppc
 secrets file = c:/rsyncd/rsyncd.secrets
 hosts allow = 192.168.1.1
 strict modes = false
 read only = true
 list = false

[d_drive]
 path = d:
 comment = Entire D: Drive
 auth users = backuppc
 secrets file = c:/rsyncd/rsyncd.secrets
 hosts allow = 192.168.1.1
 strict modes = false
 read only = true
 list = false
</code>

Modify the **'hosts allow'** parameter and create any and all paths that you require. This can also be merely the Documents and Settings directory. 

<code>
use chroot = false
max connections = 1
pid file = c:/rsyncd/rsyncd.pid
lock file = c:/rsyncd/rsyncd.lock

[docs]
 path = c:\Documents and Settings
 comment = Documents and Settings
 auth users = backuppc
 secrets file = c:/rsyncd/rsyncd.secrets
 hosts allow = 192.168.1.1
 strict modes = false
 read only = true
 list = false
</code>

Next, Create a batch file that you can distribute to allow your server to backup your workstations. Call it **'backuppc-fw-vista.bat'**.

<code>
rem backuppc-fw-vista.bat - Install firewall rules for rsycnd

set REMOTE=192.168.1.1,LocalSubnet
netsh advfirewall firewall add rule name=”RSync Program” dir=in action=allow program=”C:\rsyncd\rsync.exe” enable=yes remoteip=%REMOTE% profile=domain
netsh advfirewall firewall add rule name="RSync Related Port" dir=in action=allow protocol=TCP localport=873
netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow
pause
</code>

Be sure to update the IP address for the **'REMOTE'** variable before continuing.

For Windows XP, call it **'backuppc-fw-xp.bat'**.
<code>
rem backuppc-fw-xp.bat - Install firewall rules for rsycnd

set REMOTE=192.168.1.1,LocalSubnet
netsh firewall set allowedprogram program = c:\rsyncd\rsync.exe name = "RSync Program" = enable scope = CUSTOM addresses = %REMOTE%
netsh firewall set portopening protocol = TCP port = 873 name = "RSync Related Port" = enable scope = CUSTOM addresses = %REMOTE%
netsh firewall set icmpsetting 8 enable
pause
</code>

Next, update the password file called **'rsyncd.secrets'**. This file need only contain one line. For example:

  backuppcuser:supersecretpassword

Now, Run the batch program called **'service.bat'** to install and start the rsync daemon. You can validate that it is running by going to the Services section and looking for the running **'rsyncd'** service.

Now, log into the Webconfig BackupPC module and connect to the BackupPC administration console. Click on Edit Hosts and add the machine name or IP address of the workstation. Also, add the username who should get notifications of the backup status. Click the Save button.

Click on the XFer link. Change the method from **'rsync'** to **'rsyncd'**. For North America, use the code page of **'cp1252'**. Change the RsyncShareName to be the name listed in the config file (ie. docs, c-drive, d-drive). Add as many Shares as specified. Set the RsyncdUserName and RsyncdPasswd to be the values specified in the rsyncd.secrets file previously created.

If you backup an entire Windows drive you will need to provide a list of exclusions:

**Vista and Windows 7**
<code>
$Conf{BackupFilesExclude} = {
 'c-drive' => [
 '*.lock',
 '/$AVG',
 '/$RECYCLE.BIN',
 '/$Recycle.Bin',
 '/BOOTSECT.BAK',
 '/Boot',
 '/Documents and Settings',
 '/Intel',
 '/MSOCache',
 '/Program Files',
 '/ProgramData/Application Data',
 '/ProgramData/Desktop',
 '/ProgramData/Documents',
 '/ProgramData/Favorites',
 '/ProgramData/Microsoft/Search',
 '/ProgramData/Microsoft/Windows Defender',
 '/ProgramData/Start Menu',
 '/ProgramData/Templates',
 '/SWSetup',
 '/System Volume Information',
 '/System.sav',
 '/Users/*/AppData/Local/Application Data',
 '/Users/*/AppData/Local/History',
 '/Users/*/AppData/Local/Microsoft/Windows Defender/FileTracker',
 '/Users/*/AppData/Local/Microsoft/Windows/Explorer/thumbcache_*.db',
 '/Users/*/AppData/Local/Microsoft/Windows/History/History.IE5/MSHist*',
 '/Users/*/AppData/Local/Microsoft/Windows/Temporary Internet Files',
 '/Users/*/AppData/Local/Microsoft/Windows/UsrClass.dat*',
 '/Users/*/AppData/Local/Microsoft/Windows/WER',
 '/Users/*/AppData/Local/Mozilla/Firefox/Profiles/*/Cache',
 '/Users/*/AppData/Local/Mozilla/Firefox/Profiles/*/OfflineCache',
 '/Users/*/AppData/Local/Temp',
 '/Users/*/AppData/Local/Temporary Internet Files',
 '/Users/*/AppData/Roaming/Microsoft/Windows/Cookies',
 '/Users/*/AppData/Roaming/Microsoft/Windows/Recent',
 '/Users/*/Application Data',
 '/Users/*/Cookies',
 '/Users/*/Documents/My Music',
 '/Users/*/Documents/My Pictures',
 '/Users/*/Documents/My Videos',
 '/Users/*/Local Settings',
 '/Users/*/My Documents',
 '/Users/*/NTUSER.DAT*',
 '/Users/*/NetHood',
 '/Users/*/PrintHood',
 '/Users/*/Recent',
 '/Users/*/SendTo',
 '/Users/*/Start Menu',
 '/Users/*/Templates',
 '/Users/*/ntuser.dat*',
 '/Users/All Users',
 '/Users/All Users/Documents',
 '/Users/All Users/Favorites',
 '/Users/All Users/Start Menu',
 '/Users/All Users/Templates',
 '/Users/Users/All Users/Application Data',
 '/Users/Users/All Users/Desktop',
 '/Users/Users/Default User',
 '/Windows',
 '/autoexec.bat',
 '/bea',
 '/bootmgr',
 '/config.sys',
 '/eclipse',
 '/hiberfil.sys',
 '/pagefile.sys',
 'Cache*',
 'IconCache.db',
 'Thumbs.db',
 'cache*'
 ],
'd-drive' => [
 '*.lock',
 '/$RECYCLE.BIN',
 '/$Recycle.Bin',
 '/Documents and Settings',
 '/Users/*/AppData/Local/Application Data',
 '/Users/*/AppData/Local/History',
 '/Users/*/AppData/Local/Microsoft/Windows Defender/FileTracker',
 '/Users/*/AppData/Local/Microsoft/Windows/Explorer/thumbcache_*.db',
 '/Users/*/AppData/Local/Microsoft/Windows/History/History.IE5/MSHist*',
 '/Users/*/AppData/Local/Microsoft/Windows/Temporary Internet Files',
 '/Users/*/AppData/Local/Microsoft/Windows/UsrClass.dat*',
 '/Users/*/AppData/Local/Microsoft/Windows/WER',
 '/Users/*/AppData/Local/Mozilla/Firefox/Profiles/*/Cache',
 '/Users/*/AppData/Local/Mozilla/Firefox/Profiles/*/OfflineCache',
 '/Users/*/AppData/Local/Temp',
 '/Users/*/AppData/Local/Temporary Internet Files',
 '/Users/*/AppData/Roaming/Microsoft/Windows/Cookies',
 '/Users/*/AppData/Roaming/Microsoft/Windows/Recent',
 '/Users/*/Application Data',
 '/Users/*/Cookies',
 '/Users/*/Documents/My Music',
 '/Users/*/Documents/My Pictures',
 '/Users/*/Documents/My Videos',
 '/Users/*/Local Settings',
 '/Users/*/My Documents',
 '/Users/*/NTUSER.DAT*',
 '/Users/*/NetHood',
 '/Users/*/PrintHood',
 '/Users/*/Recent',
 '/Users/*/SendTo',
 '/Users/*/Start Menu',
 '/Users/*/Templates',
 '/Users/*/ntuser.dat*',
 '/pagefile.sys',
 'Cache*',
 'IconCache.db',
 'Thumbs.db',
 'cache*'
 ]
};
</code>

**Windows XP**
<code>
$Conf{BackupFilesExclude} = {
 '*' => [
 '*.lock',
 '/AUTOEXEC.BAT',
 '/BOOTSECT.BAK',
 '/CONFIG.SYS',
 '/Documents and Settings/*/Cookies',
 '/Documents and Settings/*/Local Settings/Application Data/Microsoft/Windows/UsrClass.dat*',
 '/Documents and Settings/*/Local Settings/Application Data/Mozilla/Firefox/Profiles/*/Cache',
 '/Documents and Settings/*/Local Settings/Application Data/Mozilla/Firefox/Profiles/*/OfflineCache',
 '/Documents and Settings/*/Local Settings/Temp',
 '/Documents and Settings/*/Local Settings/Temporary Internet Files',
 '/Documents and Settings/*/My Documents/My Dropbox',
 '/Documents and Settings/*/NTUSER.DAT*',
 '/Documents and Settings/*/Recent',
 '/Documents and Settings/*/ntuser.dat*',
 '/MSOCache',
 '/Program Files/',
 '/RECYCLER',
 '/System Volume Information',
 '/WINDOWS',
 '/hiberfil.sys',
 '/pagefile.sys',
 'Cache*',
 'IconCache.db',
 'Thumbs.db',
 'cache*'
 ]
};
</code>

===== Testing everything out =====
To trigger your first backup. Click the pulldown on the left side and select a host. Choose **'Start Full Backup'**.

===== Links =====
  * [[http://www.systemajik.com/blog/setting-up-backuppc-on-windows/|Kudos to SystemMajik Consulting for their Windows Guide.]]
{{keywords>clearos, clearos content, BackupPC, app-backuppc, clearos6, userguide, categoryserver, subcategorybackup, maintainer_dloper}}

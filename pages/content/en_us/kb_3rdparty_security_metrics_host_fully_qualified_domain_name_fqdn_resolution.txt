===== Host Fully Qualified Domain Name FQDN Resolution =====
This entry from Security Metrics indicates that they are able to resolve the host using Fully Qualified Domain Name information. This is typically done by doing a reverse lookup on the associated IP address.

===== ClearCenter response =====
==== Short response ====
This is not a security risk.

==== Long response ====
In many cases, reverse PTR records and other FQDN names are required for proper security, especially with email servers and properly implemented SSL sites. This is not a vulnerability or a risk.

==== Resolution ====
No action required.

===== Links =====

{{keywords>clearos, clearos content, 3rd party, security metrics, non-cve, maintainer_dloper}}

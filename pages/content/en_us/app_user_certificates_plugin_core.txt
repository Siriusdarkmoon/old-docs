{{ :userguides:user_certificates_plugin_core.svg?80}}
<WRAP clear></WRAP>

===== User Certificate Plugin =====
Account manager plugin for PPTP VPN.  This app is required for the master node only in a master/slave environment.  The app allows an administrator to define VPN access via the PPTP protocol.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/system/user_certificates_plugin_core|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_user_certificates_plugin_core|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_user_certificates_plugin_core|here]].
==== Additional Notes ====

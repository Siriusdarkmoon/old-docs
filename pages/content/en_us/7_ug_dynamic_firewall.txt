===== Dynamic Firewall =====

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:7_ug_Marketplace|install the module]].
===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Network|Firewall|Dynamic Firewall</navigation>
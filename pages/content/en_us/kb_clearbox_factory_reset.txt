===== ClearBOX Factory Reset =====
/**
 * Details howto reset a ClearBOX to factory settings
 */

ClearBOX comes with utilities that can help you start over from scratch. These utilities are contained on a Compact Flash (CF) boot environment that your ClearOS system passes through each time it boots, or were included with your original box as a USB drive.

To determine if your box has the CF method (most do), reboot the machine with a monitor and keyboard attached. You will presented with a menu sometime on boot between the Power On Self Test (POST) and the ClearOS GRUB kernel boot menu. The ClearBOX Utilities menu is on a 5 second countdown timer. You will need to interrupt the countdown timer before it switches over to the GRUB kernel menu. You can do this by pressing the arrow keys on your keyboard.

===== Install Options =====
The menu will give you several options including install options. The two typical install options are to reinstall the entire system with a wipe and to reinstall and preserve user data. The reinstall of the entire system should put your ClearBOX back to factory conditions with a full wipe. This wipe is NOT performed according the a method which ensures complete data removal but rather will reset the partitions and begin to write new data.

If you choose the option to 'preserve user data' then the data partition will remain untouched. This option is ONLY supported if the compact flash you are using was the one that installed the prior system.

===== USB =====
If you use a USB for your ClearBOX Utilities, you may have to press a key at the POST to direct your BIOS to boot from USB instead of the hard disk.

===== Install =====
A reinstall should run through the install process. In older models, you may have to guide the install through some menu items but on newer models, the system will reinstall and reboot automatically.

===== Updating ClearBOX Utilities =====
The compact flash or USB that you use to reinstall ClearOS may be outdated or may contain an older version of ClearOS (like ClearOS 5). You can update the Compact Flash or USB used by your ClearBOX in one of two ways:

  * Send the CF or USB back to us
  * Have us send you a new one

If you ship your prior CF back to us, we can image it with the latest Compact Flash Utilities for a newer version of ClearOS. You can also re-purchase the lastest Compact Flash or USB and we will ship it out to you.



{{keywords>clearos, clearos content, clearbox, clearbox100, clearbox300, kb, howto, factoryreset, maintainer_dloper}}

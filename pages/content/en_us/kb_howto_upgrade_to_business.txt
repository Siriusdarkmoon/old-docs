===== How to Upgrade ClearOS 7 Community Edition to Business Edition =====
/**
 * Comments can be given in this format.
 */

/* Comments can also be given in this format */

As ClearOS 7 Community is designed for Linux experts who are familiar with lifting up the hood and participating in new code testing, beta release testing, and may be required to troubleshoot issues on their own, **production environments are strongly recommended to use ClearOS 7 Business Edition**. This edition provides quality tested updates, performance optimization, cheaper prices on commercial add-ons (usually bundled with Business subscriptions), as well as being fully-supported through ClearCARE (with a Gold or Platinum subscription).

{{ :content:en_us:upgrade-to-business.png?750 }}

During the first 30 days of a ClearOS 7 Community installation, standard community / testing repos are disabled in order to provide a small window for upgrading to Business Edition without negative recourse from untested code being dynamically installed.  (Should you decide to upgrade after the 30 day window, the ClearCenter Support team will assist you in making sure the unverified repos are disabled.)

\\

**Simply follow the 2 steps below to upgrade today:**

\\
\\



===== Step 1 =====
  * [[https://www.clearos.com/products/clearos-editions/clearos-7-business#pricing|Purchase]] a ClearOS 7 Business subscription. 

\\

===== Step 2 =====
  * Submit a ClearCARE Support Ticket [[https://secure.clearcenter.com/portal/ticket_manage.jsp|here]].


{{keywords>clearos, clearos community, clearos business, clearos 7 community, clearos 7 business, clearosx, categoryx, subcategoryx, subsubcategoryx, maintainer_x, maintainer_cjones, maintainerreview}}

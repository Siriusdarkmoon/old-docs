===== DNSthingy =====
DNSthingy gives you granular control over each user’s Internet experience.  The following features can be applied network wide, or on a device by device basis:
 
  * Block ads (even on mobile devices connected to WiFi)
  * Block inappropriate content
  * Create and easily manage whitelists (block all, allow some)
  * Force Google Safe Search (across the entire network, on every browser)
  * Access Geo-blocked content from other countries (Netflix, Hulu, etc)
  * Block behavioural profiling
                         
To maintain speed, DNSthingy performs all filtering on your ClearOS box. It is controlled with a Cloud based interface. See the DNSthingy (https://www.dnsthingy.com/) website for more details.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Cloud|Content Filter and Proxy|DNSthingy</navigation>

===== Installation =====

Installing the DNSthingy app can be done via the Marketplace. Search for 'DNSthingy' to find this app.

===== Configuration ====
Once you have DNSthingy installed on the server, it is time to configure/initialize the service.

==== Administrator ====
As root (or a sub-admin with required ACL), navigate to the DNSthingy configuration.
=== Setup ===
Here you will see the initial setup form.

{{:userguides:clearos_7.x:dnsthingy_1.png?350|Installation}}

Often the default settings will be all you need.

=== Network Mode ===
This displays whether the ClearOS device is functioning in Gateway mode or in Standalone mode.

<note warning>Note that some features cannot be enforced in Standalone mode. For example, in Gateway mode firewall rules are used to ensure that DNSthingy cannot be bypassed by changing a device's DNS server. In Standalone mode this cannot be enforced.</note>

=== LAN Interface ===
This selects which interface DNSthingy is to filter.

=== Available LAN IP for Block Page ===
When a http page is blocked, DNSthingy displays a block page. This allows you to select from which IP that page will be displayed. It must be on the same subnet as the LAN interface and it must not be in use.
 
After saving, you will be taken to a screen to create your DNSthingy account.

==== Creating Your Account ====
DNSthingy is controlled from a cloud-based account. There are two options to consider.

=== Register a New Account ===

{{:userguides:clearos_7.x:dnsthingy_subscribe.png|New Account}}

If you do not yet have a DNSthingy account, fill out this form. You will receive an email with a link to verify your account.

=== Add This Box to an Existing Account ===

{{:userguides:clearos_7.x:dnsthingy_add_box.png|Existing}}

If you already have a DNSthingy account, you can add this box to that account. This allows you to manage all of your devices with one username and password.
 
Click on "Already have an account?" Fill out the form and submit it.

===== Logging into the DNSthingy Dashboard =====
The DNSthingy dashboard controls all of your DNSthingy related settings.
 
After completing the setup, visiting the DNSthingy page in your web config will give you a link to open your DNSthingy dashboard.

{{:userguides:clearos_7.x:dnsthingy_link.png|Dashboard}}

You can also visit https://www.dnsthingy.com/dashboard directly.

{{:userguides:clearos_7.x:dnsthingy_signin.png|Login}}

===== Help =====
  * [[http://support.dnsthingy.com/|Support and Documentation]]
  * [[https://dnsthingy.com/faq|FAQ]]
===== Links =====
  * [[https://dnsthingy.com/signup|Create a DNSthingy Account]]

{{keywords>clearos, clearos content, DNSthingy, app-dnsthingy, clearos7, userguide, categorycloud, subcategorycontentfilterandproxy, maintainer_bchambers}}

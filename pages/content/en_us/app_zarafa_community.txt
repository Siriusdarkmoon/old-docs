{{ :userguides:zarafa_community.svg?80}}
<WRAP clear></WRAP>

===== Zarafa Community Edition for ClearOS =====
Zarafa Community is an open-source, full-featured mail storage and groupware solution.  Zarafa includes a web-based interface for performing mail, address book and calendar functions.  The Community Edition can support any number of users via the web-based interface or through a mail client using IMAP/POP3 protocols.

 This version does not support the MAPI protocol for admins looking for a drop-in replacement to MS Exchange[TM].

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/server/zarafa_community|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_zarafa_community|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_zarafa_community|here]].
==== Additional Notes ====

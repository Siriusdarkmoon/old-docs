===== ClearOS Enterprise 5.2 Alpha 1 Release Information =====
**Released: June 4, 2010**

If you do not want to read these release notes (you should!), please at least read the following two bullet items:

  * **//Please do not run this in a production environment//**
  * **//Please do not upgrade from previous versions//**

For those wanting to run a ClearOS 5.2 test build in a real world environment, please wait for the beta release next week.  This alpha release is just a sneak preview.  

<note>If you have not already done so, please review the [[:content:en_us:announcements_releases_test_releases|introduction to test releases]].</note>

===== What's New in ClearOS Enterprise 5.2 =====
The ClearOS Enterprise 5.2 Alpha 1 release is here!  Along with updating the base system to CentOS 5.5, this release adds several administrator tools.  In addition, the release provides improved integration with third party mail services; notably Google Apps and Zarafa.

  * Password policy engine 
  * Detailed disk usage reporting
  * Network traffic analyzer tool
  * Custom firewall tool
  * H323 support
  * Mail aliases added to directory (improved support Google Apps, Zarafa)
  * Base system upgraded to CentOS 5.5
  * Bug fixes

In addition, the following features are in the pipeline

  * 64-bit version (shipping later this year)
  * Basic RADIUS support
  * [[:content:en_us:dev_architecture_central_management_start|Central management]] beta 1 milestone

===== Feedback =====
Please post your feedback in the [[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,39/func,showcat/|ClearOS Development and Test Release Forum]]. 

===== Download =====
[[http://download.clearfoundation.com/clearos/enterprise/5.2/iso/clearos-enterprise-5.2-alpha1.iso|Download ClearOS Enterprise 5.2 Alpha 1]] 

MD5: 94a711df4eb86283b889623c185472e7


===== Feature Details =====
==== Password Policy Engine ====
Webconfig Menu: <navigation>Directory|Setup|Password Policies</navigation>

{{ :release_info:clearos_enterprise_5.2:password_policies.png|ClearOS Password Policies}} The password policy engine allows an administrator to enforce password policies:

  * Password minimum length
  * Password history
  * Password maximum age (expiration)
  * Password minimum age
  * Password strength

This feature is mostly functional in the alpha, but there are some rough edges.  If you do not see the password policy engine in the menu system, you can install the module with **yum install app-password-policies**

==== Network Traffic Analyzer ====
Webconfig Menu: <navigation>Reports|Network|Network Traffic</navigation>

The network traffic analyzer gives you a view into what is going on at the network level.  This can be a handy administrator tool for:

  * Locating virus infected desktops sending out spam, viruses, etc
  * Pinpointing network hogs (who is downloading that HD movie?)
  * Checking for unusual network activity

To install the traffic analyzer, run **yum install app-jnettop**
  
{{:release_info:clearos_enterprise_5.2:network_traffic.png|ClearOS Network Traffic Analyzer}}

==== Mail Aliases in LDAP / Directory ====
Webconfig Menu: <navigation>Directory|Accounts|Users</navigation>

{{ :release_info:clearos_enterprise_5.2:aliases_to_ldap.png|Mail Aliases in ClearOS Directory - LDAP}}
Mail aliases are now part of the ClearOS Directory (LDAP).  Why is this a "Good Thing"?  With mail aliases in the Directory, it is possible to integrate alternative mail solutions including Google Apps (to be released later this year) and Zarafa.  You can manage these aliases through the usual User Manager found in webconfig.  The adjacent screenshot shows two aliases for username **tim**:

  * thorton
{{keywords>clearos, clearos content, announcements, releases, clearos5.2, alpha1, previoustesting, maintainer_dloper}}

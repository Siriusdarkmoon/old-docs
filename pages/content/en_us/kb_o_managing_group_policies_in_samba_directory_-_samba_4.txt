===== Managing Group Policies in Samba Directory =====
Samba Directory / Samba 4 includes group policy (GPO) support.  You can manage group policies using the standard Windows desktop tools described below.  

**Please Note** -- Use of these Microsoft Tools is conditional upon your use and acceptance of their Terms of Service and End User License Agreements.

===== RSAT for Windows 10 =====
You can download the Remote Server Administration Tools for Windows 10 from here:

  * [[https://www.microsoft.com/en-us/download/details.aspx?id=45520|Remote Server Administration Tools (RSAT) for Windows 10]]

===== RSAT for Windows Vista and later =====
For to use the Remote Server Administration Tools (RSAT), follow these instructions:

  * [[https://support.microsoft.com/en-us/kb/2693643|Remote Server Administration Tools (RSAT)]]

===== Windows 7 Install =====
You can download and install the Remote Administration Tools for Windows 7 here.

  * [[https://www.microsoft.com/en-us/download/details.aspx?id=7887|Windows 7 Remote Administration]]

===== Windows XP Install =====
You can install the Microsoft Group Policy Management Console (GPMC) here:
  * [[
https://www.microsoft.com/en-us/download/details.aspx?id=21895|Group Policy Management Console with Service Pack 1]]  

You will need to be using Service Pack 1 or with Microsoft .NET 1.1 installed.  You can install this older version of .NET in parallel with existing later versions.  [[http://www.microsoft.com/en-us/download/details.aspx?id=26|Microsoft .NET 1.1 Download]]


{{keywords>clearos, clearos content, version7, app-samba-directory, categoryserver, kb, howtos, maintainer_dloper}}
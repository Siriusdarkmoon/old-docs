===== ClearBOX User Guide =====
ClearBOX is a powerful line of server appliances specifically selected to utilize the most out of ClearOS. This gives you the assurance that all drivers, network operations, and features of hardware a available and tested against ClearBOX hardware.

Additionally, ClearBOX comes with some features which combine hardware and software solutions that you won't find on ClearOS natively.

===== Models Specific Guides =====
Most features of ClearBOX are common and are discussed here. There are some specific items depending on your ClearBOX model. Additionally, there are some useful guides for specific ClearBOX issues Please refer to the links below:

=== User Guides ===

  * [[:content:en_us:ClearBOX 100|ClearBOX 100 User's Guide]]
  * [[:content:en_us:ClearBOX 300|ClearBOX 300 User's Guide]]

=== Deployment and Troubleshooting Guides ===

  * [[:content:en_us:ClearBOX 300 Gen 3 BIOS Update|BIOS update for ClearBOX 300 Gen3]]

===== Getting Started =====
Your ClearBOX will ship in a default configuration. The installation of ClearOS onto your ClearBOX has already been performed so you can get started with your ClearBOX right away.


When you first boot ClearBOX, you can either manage the interface through a USB keyboard and a VGA monitor, or you can connect a laptop to ClearBOX's last interface with a network cable. ClearBOX will give DHCP on this interface so you can begin setting up the box for your network. This last network is yours to use so after you get the network interfaces of ClearBOX set to your specific needs, you can utilize this interface however you desire.

When you boot ClearBOX, you will be presented with a menu at the beginning of the boot cycle. From this menu you can reinstall, run utilities, or troubleshoot general problems. We will discuss this in detail below.

The default password on ClearBOX is **'clearbox'** on older models, and **'password'** on all newer models. The administrator account is **'root'**. You should change the password immediately using the Webconfig interface or from the command line by using the 'passwd' command.

ClearBOX will have a sticker indicating the ClearBOX ID of your ClearBOX. This ID is important for all communications with ClearCenter in regards to your ClearBOX hardware. If you ever have an issue with your ClearBOX hardware and it is under warranty, simply open a ticket in your ClearSDN portal and include this ClearBOX ID and the nature of your problem.

===== Initial Management =====
As indicated earlier, your ClearBOX can be managed by connecting a keyboard and monitor via USB and VGA respectively. It can also be managed through the network using the last network card. 

==== Network Management ====
The initial DHCP address given out by ClearBOX to your laptop or other connected system begins with 172.22.22.x If you get an address in this range then you can manage your system by navigating to [[https://172.22.22.22:81]] in your browser. You can also reach the command line via 172.22.22.22 using SSH.

==== Console Management ====
ClearBOX will start and be available via VGA/Keyboard by launching either the Graphical User Interface (GUI), or the Text-based User Interface (TUI). The ability to launch the GUI is largely dependent on the ability of ClearBOX to interact with your monitor. If the GUI will **not** launch, try using a different monitor.

You can also switch between console screens of your ClearBOX when connected at the console by using the keys for Control, Alt, and the Function keys. Here is a list of the various Screen and the key combinations to access them:

  * Ctrl+Alt+F1 - Console session. This is unusable when the GUI is active.
  * Ctrl+Alt+F2 - Command line session 2.
  * Ctrl+Alt+F3 - Command line session 3.
  * Ctrl+Alt+F4 - Command line session 4.
  * Ctrl+Alt+F5 - Command line session 5.
  * Ctrl+Alt+F6 - Command line session 6.
  * Ctrl+Alt+F7 - GUI session. This is unusable when the TUI is active.

===== Initial boot menu =====
The initial ClearBOX boot menu is presented just a few seconds after ClearBOX is started and is on a 5 second countdown timer. After it is completed it turns over control to the ClearOS boot menu which is on another separate 5 second countdown timer. If your monitor does not properly display the boot menu long enough to see it, use the space bar or arrow keys after the BIOS POST messages stop the countdown timer.

==== Default Boot ====
The default operation is to boot ClearOS, ClearBOX does this by assigning control the the boot loader on the main hard drive.

==== Installation Menu ====
If you select the Install Menu from the boot screen, you will be able to do a default install or a custom install. Custom install is not recommended. The option for the default install is to either install ClearBOX in a wipe (This reinstalls ClearBOX cleanly and completely destroying any existing data on all partitions) or by data preservation which reinstalls the OS but leaves user data unaffected. During a data preservation installation ClearOS is installed without destroying data on the /store/data0 volume. Typically this is used for:

  * /home
  * /var/flexshare/shares
  * /var/spool/imap
  * /var/lib/mysql
  * /root/support
  * /var/samba/drivers
  * /var/samba/netlogon
  * /var/samba/profiles
  * /var/www/cgi-bin
  * /var/www/html
  * /var/www/virtual

Most user data is stored in these directories, so a re-install should not affect this data when using the 're-install' option.

If you install modules that use the above directories, you should assign storage to the directories using the [[http://www.clearcenter.com/support/documentation/clearos_guides/storage_manipulation_using_bindmounts|bindmount method]].

<note warning>You should always backup data before proceeding with any type of reinstall. The reinstall option is designed for convenience and not as a data integrity replacement.</note>

=== Modes of Install (ClearOS 6) ===
Because of the transition to the marketplace, modes of install are not available in the installation process but can be mimicked easily once ClearOS is install. Please contact support if you require assistance setting up a mode that was available under ClearOS 5.x.

=== Modes of Install (ClearOS 5.x) ===
== Full Install ==
For ClearBOX, a Full Install includes installing all of the typical services available on ClearOS. This is the default install.

Typical to this install is the ability to use ClearBOX as a gateway. The first NIC is Internet facing, uses DHCP by default and is firewalled. You can change this to suit your needs if you are using a static address. The second NIC is for the local LAN. The network address of this NIC is 192.168.1.1 and should be changed to suit your environment. If this address appears on an interface called **br0** in Webconfig, you should change the IP address in command line to preserve the bridge. The reason for this is that it is sharing this IP with other network interfaces. Please refer to the Bridge support section below to make changes to the bridge or bridge interfaces.

== Gateway ==
This mode is available on ClearBOX 100 and 300 series.

In gateway mode, ClearBOX is configured similar to that of a typical consumer router. The first interface is for WAN access and the remaining interfaces are on a bridge. For convenience, ClearBOX is configured to hand out IP addresses in the 192.168.1.x range in this mode. Gateway services are installed by typical server services are not. You can add any service you like in the modules section of Webconfig.

== Inline Transparent Bridge ==
This mode is available on ClearBOX 100 and 300 series.

This mode is designed to sit between the network and router and capture, log, and filter traffic using ClearOS' content filtration engine. The first two interfaces are on the bridge. Plug the connection from the router into the first port and the connection to the network switch into the second port. 

In this mode, ClearBOX is configured with the IP address of 192.168.1.9 and uses the gateway 192.168.1.1 by default. You will need to change the address if this does not match your network. This is discussed in the Bridge support section below.

This mode operates as a trusted gateway and a bridge. Its purpose is to capture port 80 traffic as data flows over the interfaces. Additionally, the bridge will use the bypass feature of your ClearBOX if it is available. Please refer to your ClearBOX specific guide as there are settings that must be done on the motherboard or BIOS to enable this mode to fail open when the ClearBOX is powered off or the services required to operate are not available.

== Mission Critical Gateway ==
This mode is available on ClearBOX 100 and 300 series but is only recommended on ClearBOX 300.

This mode allows you to have a redundant gateway infrastructure by having two ClearBOX servers similarly configured to act as redundant gateways. By default they will communicate with each other and when the backup server detects a problem with the primary, it will usurp control and 'fence' the primary. Fencing is the means whereby the backup server can physically remove the primary from the network and take control of the physical interfaces.

The backup server and the primary server will communicate to each through the serial port on ClearBOX by default. It is possible to move this communication link to a network interface if the serial port is needed. The connection requires a Serial Null modem rollover cable which is not provided. Either purchase an RJ45 Serial Null Modem cable or construct one yourself.

^ Jack 1 ^ Jack 2 ^
| Pin 1 | Pin 8 |
| Pin 2 | Pin 7 |
| Pin 3 | Pin 6 |
| Pin 4 | Pin 5 |
| Pin 5 | Pin 4 |
| Pin 6 | Pin 3 |
| Pin 7 | Pin 2 |
| Pin 8 | Pin 1 |

The serial port is and RJ 45 port located near the USB ports on ClearBOX. This is NOT an Ethernet jack.

With the serial connected, you need to then connect the primary and backup servers. During the installation you will have selected primary or backup server. The backup server of ClearBOX has the majority of the connections. This is because it must bypass connections to the primary if the primary is working properly. 

This table describes the cabling for ClearBOX 300.

^ Backup Server Ports ^ Destination of cable ^
| eth0 | to Internet (WAN) |
| eth1 | to eth0 on primary |
| eth2 | to network switch (LAN) |
| eth3 | to eth2 on primary |
| eth4 | to eth4 on primary |
| eth5 | available for management or other purpose |

^ Primary Server Ports ^ Destination of cable ^
| eth0 | to eth1 on backup |
| eth1 | -unused- |
| eth2 | to eth3 on backup |
| eth3 | -unused- |
| eth4 | to eth4 on backup |
| eth5 | available for management or other purpose |

This table describes the cabling for ClearBOX 100.

^ Backup Server Ports ^ Destination of cable ^
| eth0 | to Internet (WAN) |
| eth1 | to eth0 on primary |
| eth2 | to network switch (LAN) |
| eth3 | to eth2 on primary |

^ Primary Server Ports ^ Destination of cable ^
| eth0 | to eth1 on backup |
| eth1 | -unused- |
| eth2 | to eth3 on backup |
| eth3 | -unused- |

==== Utilities Menu ====
The Utilities menu has a few items that are useful if your ClearBOX ever has any trouble. These are designed for advanced troubleshooting and testing.

=== Bypass bootloader ===
In the unlikely situation that your ClearBOX should suffer corruption of the boot sector of the hard drive, the bypass bootloader option is useful for starting ClearOS. This option is useful especially when kernel modificatons have occurred and the proper updates to the bootloader were NOT applied. It is rare that you will use this option unless you are a bit of a hacker and you messed up.

=== Rescue mode ===
This tool allows you to boot ClearOS using the rescue image on the initial boot media. You will be taken to a short menu to determine network interfaces and the system will attempt to mount your ClearOS install in /mnt/sysimage. This will allow you to fix problems which may affect booting.

=== Memory tests ===
The memory testing utility is also included with ClearBOX. This will allow you to run a battery of tests against the available memory on the ClearBOX.

=== Disk utilities ===
Some versions of ClearBOX come with an image of Parted Magic. This is useful for troubleshooting some common disk errors, performing disk images and other disk troubleshooting related activities.

===== Wireless Management =====
Many ClearBOX models come with wireless management. If your ClearBOX came preloaded with wireless support then your ClearBOX will use WPA2 infrastructure mode by default. This mode ties wireless authentication into the directory structure of ClearOS to provide distinct username and password authentication. If you want to switch from WPA2 infrastructure mode to WPA2 pre-shared key support, you will need to change the hostapd file on the server and restart the hostapd service, or restart the server.

=== /etc/hostapd/hostapd.conf preshared key support ===

  bridge=br0
  interface=wlan0
  driver=nl80211
  hw_mode=g
  channel=6
  ssid= WIRELESS
  auth_algs=1
  ctrl_interface=/var/run/hostapd
  
  ###WPA
  wpa=3
  wpa_key_mgmt=WPA-PSK
  wpa_passphrase=mypassword
  wpa_pairwise=TKIP
  rsn_pairwise=CCMP
  wpa_group_rekey=300
  wpa_gmk_rekey=640
  
  # Wireless N
  wme_enabled=1
  ieee80211n=1
  ht_capab=[HT40-][SHORT-GI-40][DSSS_CCK-40]

=== /etc/hostapd/hostapd.conf WPA2 infrastructure support ===

  bridge=br0
  interface=wlan0
  driver=nl80211
  hw_mode=g
  channel=6
  ssid=WIRELESS
  ieee8021x=1
  auth_algs=1
  ctrl_interface=/var/run/hostapd
  
  ###WPA
  wpa=3
  wpa_key_mgmt=WPA-EAP
  wpa_passphrase=mypassword
  wpa_pairwise=TKIP
  wpa_group_rekey=300
  wpa_gmk_rekey=640
  
  # Wireless N
  wme_enabled=1
  ieee80211n=1
  ht_capab=[HT40-][SHORT-GI-40][DSSS_CCK-40]
  
  ###Radius Setup
  own_ip_addr=192.168.1.1
  nas_identifier=ClearBOX-BUILT-IN
  auth_server_addr=127.0.0.1
  auth_server_port=1812
  acct_server_addr=127.0.0.1
  acct_server_port=1813
  eap_server=0
  rsn_pairwise=CCMP
  
  auth_server_shared_secret=SECRETPASSWORD
  acct_server_shared_secret=SECRETPASSWORD

===== Bridge support =====
Many ClearBOX models and modes use bridge technology which is not generally supported in ClearOS' 5.2 Webconfig interface. You will need to manage these devices and their associated NICs using command line tools instead of Webconfig.

To remove the bridge completely and manage the NICs individually simply remove the bridge configuration file and use Webconfig. The bridge configuration file is located at:

  /etc/sysconfig/network-scripts/ifcfg-br0

To change the IP address of the bridge, modify the **ifcfg-br0** file and restart the server. Modifying the configuration of any device using Webconfig will remove it from the bridge. This is useful if you want the wireless or other NIC on the bridge to be on a separate network segment.
===== Configuration Backup =====
The backup/restore feature lets you take a snapshot of all the configuration files and save them to a separate system for safe keeping.  If a ClearOS system needs to be restored, you can re-install the ClearOS system and then restore all the configuration settings from the backup.

===== Installation =====
This feature is part of the core system and installed by default.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>System|Backup|Configuration Backup and Restore
</navigation>

===== Remote Server Backup and ClearSDN =====
{{:omedia:clearsdn-icon-xxs.png }} The backup/restore settings tool does **not** save user data, logs or mailboxes.  Use [[http://www.clearcenter.com/Services/clearsdn-remote-server-backup-6.html|Remote Server Backup]] to keep your data safe.  The remote server backup system is able to backup MySQL databases, mail stores, LDAP, file shares as well as specified files and directories.  The solution takes all the complexity out of doing a backup and protects vital data from being lost.

===== Configuration =====
There are no configurable options for this app

The backup/restore tool saves a rolling 10 days of all the configuration information available through [[:content:en_us:7_webconfig|webconfig]] including:

  * Usernames and passwords
  * Network configuration
  * Firewall configuration
  * Software configuration (for example, content filter)

If you have installed third party applications on your system, you will need to take extra steps to save configuration data.

The backup is created automatically overnight.

==== Restore From Archive ====
<note warning>At the moment there is a problem restoring an Active Directory Connector configuration into a fresh installation ClearOS7. In order for it to work correctly you **must** install the Windows Networking (Samba) app from the Marketplace **before** you do the configuration restore. If you do not do this you will need to reinstall from scratch. Installing the Windows Networking (Samba) app after a config restore then doing another config restore does not fix the problem.\\
You may then need to start winbind and set it to restart automatically on boot:
<code>systemctl start winbind.service
systemctl enable winbind.service
</code>
If you still have problems please consult the [[:content:en_us:kb_troubleshooting_the_ad_connector|Active Directory Connector Troubleshooting]] guide, especially the "wbinfo -t" section.</note>

{{7_ug_configuration_backup_restore.png}}

This allows you to restore a backup from any location accessible from your desktop.

==== Archives ====
{{7_ug_configuration_backup_archives.png}}

This lists the last 10 archives which are still available on the server.

The <button>Backup Now</button> button allows you to manually create a backup.

The <button>Download</button> button allows you to download a backup to any location accessible from your desktop. You may want to do this if you want to keep more than the 10 rolling copies.

The <button>Restore</button> button allows you to restore the selected backup from the file held on the server.

The <button>Delete</button> button allows you to delete the selected backup from the server.

===== Troubleshooting =====
During the restore procedure, the original network settings will be restored, but not activated.  Consider this scenario:

  * The system settings on a live ClearOS gateway have been saved.
  * Due to a hard disk failure, ClearOS was temporarily replaced with a basic router.
  * ClearOS was re-installed on another server while connected to your LAN.
  * The restore procedure is then used on the newly installed ClearOS system.

The network settings are now in limbo.  The restored network configuration is expecting to be connected as a gateway, but the system is temporarily running as a standalone system on your LAN.  In this scenario, you will either need to put the system back into its role as a gateway, or, reconfigure the network.

If you restore into a new machine, almost certainly your Network Interface names will change so you will need to set them up again. You will need to re-input the 1-to-1 NAT, Network, DHCP Setver and ibVPN settings and, possibly, the Radius settings.

{{keywords>clearos, clearos content, Configuration Backup, app-configuration-backup, clearos7, userguide, categorysystem, subcategorybackup, maintainer_nhowitt}}

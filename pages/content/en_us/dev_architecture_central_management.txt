===== Central Management - Under Development =====
All of the information in this section and these sub-sections is subject to change and represents open development.  Please feel free post feedback in the [[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,39/func,showcat/|ClearOS Development Forum]].

===== What is Central Management? =====
The easiest way to describe central management is to walk through a real world scenario.  Let's say you are the network manager for the following organization:

  * 20 office locations: 10 in the Americas, 5 in Europe and 5 in Africa.
  * Each geographic area (Americas, Europe and Africa) has a regional head office.
  * A total of 300 people are employed across the organization.

Each of these locations needs to have two ClearOS systems: one installed as the Internet gateway, the other installed as a local file server.  Managing these 40 systems (2 systems x 20 locations) by logging into every single system is time consuming (read: expensive), tedious, error prone and - quite frankly - just not a fun job!

The solution:  **Central Management**

==== Sample Requirements ====
Before describing the design and scope of Central Management, let's take a look at some example requirements.  The following scenarios are just examples of what can happen on a day-to-day basis in this organization.  With a fully implemented ClearOS Central Management solution, these tasks should be done with only a few clicks of the mouse and keyboard. 

=== New Employee ===
A new salesperson is expected to be hired in the near future.  With Central Management, the following policies should be deployed for the new employee:
  * VPN access to all locations in the Americas
  * A medium level content filtering policy across all ClearOS gateways
  * File server access to all "sales" shares in all 3 regional headquarters
  * A Google Apps account
  * Windows PDC login via the ClearOS file server in Toronto

=== Mesh VPN ===
{{ :omedia:vpn_network.png|ClearOS Mesh VPN}} VPN tunnels do not require much in the way of computer system resources in this day and age.  For this reason, the 20 locations will be connected to each other in a peer-to-peer/mesh VPN.  This type of configuration is not only more bandwidth efficient, but also more robust.  Without central management, adding a new office location would require creating a new VPN connection on //every single existing ClearOS gateway//.  Again, Central Management should resolve this inefficiency.

=== Web Access Policy - No Facebook ===
Looking through the web proxy log reports, the amount of time spent browsing Facebook during office hours looked outrageously high.  Management may decide to limit Facebook access to lunch hours only.  

=== Virus Outbreak - Shutdown VPNs ===
A virus outbreak is an inevitability on a large network.  As a precaution, all VPN connections to an infected remote site will need to be shut down.

===== Overview =====
{{ :omedia:directory_users.png|ClearOS Central Management - Users}}
In the past versions ClearOS, we have been working on laying a foundation for Central Management.  Now that ClearOS 5.1 has been released and we look to the future, we are still overwhelmed at the lack of solid options and solutions in the Small Business and Distributed Enterprise market for truly integrated Linux and multi-platform solutions. 

===== People =====
The following people are currently involved in the Central Management feature development:

  * John Terpstra - Architecture/Oversight/Testing
  * Darryl Sokoloski - ClearOS integration/Development/UI/Testing
  * Peter Baldwin - Development/Oversight/Integration/Testing
  * Dave Loper - Specification/Architecture/External OS integration/Testing

John has worked with Samba and LDAP integration for years and has created scalable/integrated LDAP solutions with PDC/BDC functionality (in fact he's [[http://www.samba.org/samba/docs/man/Samba-Guide/|written]] a [[http://www.amazon.com/Samba-3-Example-Practical-Successful-Deployment/dp/0131472216|book]] on it.) Dave has worked with Directory server and identity integration on multiple platforms including OpenLDAP, AD, eDir and others.  Peter is a Linux coder and brilliant integrator and has more ClarkConnect/ClearOS knowledge than nearly anyone.  Darryl is a brilliant developer who silently produces some of the best solutions available on ClearOS.

===== Design and Scope =====
Central Management creates complexity with design.  Servers must be aware of each other, share tasks, update each other when necessary, provide failover, and handle ad hoc services under the same distributed services model.  We can separate Central Management into three distinct categories.  Don't worry, we'll explain what these mean in a moment:

  * Global Elements
  * Group Policies
  * Server Policies

==== Global Elements ====
{{ :omedia:directory.png|}} In a distributed environment, there is a need to have global information available across the entire organization.  So what kind of global elements do we have in our 20 office scenario?  For one, all usernames and passwords should be available on all ClearOS systems.  Whether a user authenticates to a remote VPN or opens a file on a local file server, the information needs to be stored on every ClearOS system in the network.

For example, our new salesperson -- username //t.horton// -- will not only be travelling to various locations, but will also need access sales documents in branch offices (via VPN).  Though //t.horton// may not have any privileges to access //all// the systems in the entire network, his user information will need to exist on all ClearOS systems (and you will see why in the next topic: Group Policies). 

The global elements in the Central Management architecture include:

  * Users
  * Groups
  * Systems
  * Security and Keys
  * Password Policies
  * Organization Information

For those who work in the technology space, it will come as no surprise that LDAP is the technology used for managing this type of global data.  Fortunately, LDAP was designed with replication in mind.  In our example company, one master LDAP directory will exist on a single ClearOS system and then be replicated to the other 39 ClearOS slave systems.  These slave systems provide read-only access to the directory data, and can run just fine when the master directory is unavailable (for example, during a network outage).

So that's the **Global Elements** layer of Central Management.  This part of the implementation was released as a beta in ClearOS 5.2.  For all intents and purposes, everything that you see in the web-based interface under the **Directory** menu (see adjacent screenshot) is part of this global layer.  You can find more technical specifications on this topic here:

  * [[:content:en_us:dev_architecture_central_management_ldap_replication_framework|LDAP Replication Framework]]
  * [[:content:en_us:dev_architecture_central_management_openldap_replication_settings|OpenLDAP Replication Settings]]

==== Group Policies ====
Getting back to our 20 office scenario, there is a need to give a new salesperson (//t.horton//) VPN access to all locations in the Americas.  With our master/replicate LDAP in place, username //t.horton// exists on all ClearOS systems.  However, we would still need to login to every single gateway in the Americas to configure VPN access.  That's 10 servers in all.  Clearly, that's not practical and this is where **Group Policies** can help.

Since every salesperson in the Americas requires the same access rules, the following was already put in place.

  * A group called **Sales Americas** was created
  * All ClearOS gateways in the Americas had VPN enabled for the **Sales Americas** group

This second step is a **Group Policy**.  With Central Management, this step should be completed in one location.  In other words, an administrator should not have to login to 10 gateways to enable VPN for the **Sales Americas** group.  And once this policy is in place, it is now simply a matter of adding //t.horton// to the **Sales Americas** group.  

So what features in ClearOS need to be integrated into the Group Policy layer?  Any feature that requires username and/or group information needs to be aware of **Group Policies**.  Here is a sample of ClearOS modules that fall into this category:

^Feature^Example Policy^
|Access Control|Block //Support Group// from accessing the web during certain hours|
|Content Filter|Add //Students// to strict content filter group|
|Flexshare|Create a //Marketing// share on all headquarter file servers|
|OpenVPN|Allow //Engineering Group// to connect to all ClearOS gateways|
|PPTP VPN|Allow //Sales Group// to connect to all sales offices|

==== Server Policies ====
We have now discussed two layers of the Central Management architecture:

  * **Global Elements** using LDAP master/replicate
  * **Group Policies** using groups and policies in LDAP

The final layer in our proposed solution is **Server Policies**.  

Revisiting our 300 person organization, there are some day-to-date tasks that need to be taken care of.  The task of software/patch management is certainly a good example.  On a 40-server network, it is certainly prudent to roll out software updates in a conservative manner.  For example, an administrator may want to install updates on 3 systems used by the engineering team first, and then roll out the updates to other locations when on-site.

Quite a few administrative tasks fall under this category of **Server Policies**.  Here is a sample of ClearOS modules that fall into this category:

^Feature^Example Policy^
|Antivirus|Set //block encrypted files// policy for all systems|
|Antiphishing|Set //blocked cloaked URLs// policy for selected systems|
|Intrusion Detection|Enable the //network scan// rule set for all systems|
|Intrusion Prevention|Add IP 1.2.3.4 to exempt list on all systems|
|Protocol Filter|Block //Skype// on systems located in North America|
|Software and Patch Management|Install latest software updates for selected systems|
{{keywords>clearos, clearos content, dev, architecture, central management}}

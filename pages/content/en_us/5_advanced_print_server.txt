===== Advanced Print Server =====
ClearOS includes the an advanced printer server for your network.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:


<navigation>Server|File and Print|Advanced Print Server</navigation>

===== Configuration =====
From this webconfig page, you can:

  * Start and stop the advanced print server
  * Access the link to the web-based advanced print server manager

Once the advanced print server is running, a link to the web-based manager will be shown.  Follow the link and login with the //root// account.  You can find detailed information on how to use this software [[http://www.cups.org/documentation.php?VERSION=1.3|here]].

<note>As a security precaution, the printer web interface is only accessible from a trusted (LAN) network.  You can not access the web interface from a remote Internet connection.</note>

==== Adding Printers - Quickstart  ====
To add a printer, you first need to access the print server manager:
  * Login to the web-based print server manager with the //root// account
  * Click on <button>Add Printer</button>

=== Step 1 - Add New Printer ===
On this step, you configure general settings for the name of the printer:
  * Enter a descriptive name, for example: LaserJet
  * Enter a location (optional), for example: Room 101
  * Enter a description (optional), for example: Black and White LaserJet

=== Step 2 - Device Selection === 
A printer can be connected via parallel port, USB, and other means.  In fact, you can add remote printers as well. 

=== Step 3 - Device Details ===
Depending on your selection, you may need to configure more details.  In some some cases, you will skip straight to the next step.

=== Step 4 - Make/Manufacturer ====
In this step, you can configure the printer drivers for your make and model.  If you cannot find your printer listed, you can check the [[http://www.openprinting.org/printers|Open Printing Database]] for driver support.  If a driver is available, you will be able to upload it during this step.

After this step, your printer will be active.

===== Supported Printers =====
To find information on printer compatibility, please visit the [[http://www.openprinting.org/printers|Open Printing Database]] web site.  You will find detailed driver information on hundreds of printers.

===== Integrating with Windows Networks =====
When you configure a new printer, it will appear as a shared printer on your Windows Network (if [[:content:en_us:5_windows_settings|Windows Networking]] is installed). However, you will may need to restart the Windows network service after adding a new printer.

===== Links =====
  * [[http://www.cups.org/documentation.php?VERSION=1.3|Printer Manager Documentation - CUPS]]
  * [[http://www.openprinting.org/printers|Open Printing Database]]
{{keywords>clearos, clearos content, Advanced Print Server, app-print_server, clearos5, userguide, servercategory, maintainer_dloper, maintainerreview_x, categoryserver, subcategoryfileandprint}}

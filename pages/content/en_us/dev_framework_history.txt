===== History =====
The web application framework is used to develop new features/apps for ClearOS.  The framework used in the 5.x release was designed in 2004.  That's about 100 years ago in computer years!  In 2010, the time came to re-engineer a solution that was more robust, easier to use, and flexible.  

===== Purpose =====
ClearFoundation is focused on building a strong, secure and stable ClearOS core.  Though we could have gone on adding more and more features, we realized that the best plan of action was to open up ClearOS to developers.  That's why everything in the core ClearOS is open source.  That's why a not-for-profit organization now takes care of the source code.  That's why we took the time and resources to re-engineer the web framework.

Of course, there were other reasons too:

  * Rapid development
  * Extensibility
  * Improved web application security
  * Path to create better usability

===== Story =====
Step back in time to 2006.  Ajax and Web 2.0 (sorry) were quickly gaining traction.  A new kid on the block called [[http://rubyonrails.org|Ruby on Rails]] was getting off the ground.  We knew even back in 2006 that the web application framework in ClearOS (ClarkConnect) was getting long in the tooth, so a plan was put in place to modernize in two steps:

  * Refactoring the Software API for 4.0 
  * Implementing a new web application framework for 5.0

The software API changes were completed and have since matured.  However, the web application framework changes slated for 5.0 were pushed back.  The pesky Samba/LDAP integration consumed quite a bit of development resources in 5.0 and there was not enough time to re-engineer the framework.  If you want to take a look at the old way of doing things, take a look at a short document outlining the [[:content:en_us:dev_framework_old_framework|old framework]].

===== Requirements =====
Like good engineers, we put together a requirements document before embarking on the selection process.  Here's the short version of this document: 

==== Required ====
  * Good translation support
  * Good performance
  * Good documentation
  * Ajax integration
  * Extensible

==== Nice to Have ====
  * Plays well with IDEs (e.g. Eclipse, Zend)

==== Common ====
The following are common requirements that are universal across all frameworks.  They are listed here for completeness:

  * Rapid development cycle
  * MVC framework
  * Basic security features (XSS, forms)
  * Themeable
   
===== The Contenders =====
{{ :developer:source code:android_monitor02.png?250}}

The old API was written in PHP but it was designed to be RPC (remote procedure call) friendly.  You didn't see pass-by-reference in too many places and almost all methods were stateless.  In other words, it was straightforward to add a [[http://en.wikipedia.org/wiki/Representational_State_Transfer|REST]] or [[http://en.wikipedia.org/wiki/SOAP|SOAP]] layer over the API.  In fact, both technologies were already been implemented in parts of ClearOS 5.x!  That's how the adjacent screenshot of a real-time ClearOS monitoring on [[http://www.android.com|Google Android]] mobile phone was implemented.

Please be open to the possibilities!  Though it is a very large undertaking to change the underlying language from PHP to Ruby or Perl, discussing the merits is important.

==== PHP ====
  * [[http://www.codeigniter.com|CodeIgniter]]
  * [[http://www.kohanaphp.com|Kohana]]
  * [[http://www.cakephp.org|CakePHP]]

==== Perl ====
  * [[http://mojolicious.org/|Mojo - Mojolicious]]

==== Ruby ====
  * [[http://rubyonrails.org|Ruby on Rails]]

==== Others ====
{{keywords>clearos, clearos content, dev, framework, maintainer_dloper, maintainerreview_x, keywordfix}}

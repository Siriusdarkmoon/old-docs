==== ClearCenter DNS and Domain Services ====
The following provides information on the range of cloud-based DNS and Domain services offered by ClearCenter.

=== Internet Domains ===
  * [[:userguides:clearsdn:clearcenter_dns_service:domain_transfer|Domain Transfer]]
  * [[:userguides:clearsdn:clearcenter_dns_service:domain_registration|Domain Registration]]
  * [[:userguides:clearsdn:clearcenter_dns_service:domain_renewal|Domain Renewal]]
  * [[:userguides:clearsdn:clearcenter_dns_service:domain_management_with_whois|Domain Management with Whois]]
  * [[:userguides:clearsdn:clearcenter_dns_service:registrant_verification_and_icann|Registrant Verification and ICANN]]

=== DNS ===
  * [[:userguides:clearsdn:clearcenter_dns_service:dns_primer|DNS Primer]]
  * [[:userguides:clearsdn:clearcenter_dns_service:dns_a_records_and_host_records|DNS A Records]]
  * [[:userguides:clearsdn:clearcenter_dns_service:dns_cname_records_and_alias_records|CNAME Records]]
  * [[:userguides:clearsdn:clearcenter_dns_service:dns_mx_records_and_mail_records|MX Records]]
  * [[:userguides:clearsdn:clearcenter_dns_service:dns_txt_records_and_spf_records|TXT and SPF Records]]
  * [[:userguides:clearsdn:clearcenter_dns_service:dns_ptr_records_and_reverse_dns_records|Reverse DNS Records]]

=== Mail ===
  * [[:userguides:clearsdn:clearcenter_dns_service:mail_backup_and_mx_backup|Mail Backup]]

{{keywords>userguide}}

===== Clearos Professional 6.2.0 Final Release Notes =====
The following release notes provide an overview of the ClearOS Professional 6.2.0 release. 

===== What's New =====

==== Base System ====
  * Marketplace
  * Core system derived from Red Hat Enterprise Linux <nowiki>*</nowiki> 6.x source code
  * 64-bit support
  * Graphical installer
  * Windows BDC support
  * App developer framework

==== New Apps ====
  * Google Apps Synchronization
  * Active Directory Connector
  * Account Synchronization
  * Gateway Antimalware Premium powered by Kaspersky
  * File Antimalware Premium powered by Kaspersky
  * Zarafa Standard and Community (coming soon)
 
===== Upgrading =====
A pilot program for upgrading ClearOS 5.x was started with the release of 6.2.0.  Once this pilot program has completed, upgrades may become available at that time.

===== Notices =====
<nowiki>*</nowiki> Company names, brand names, trademarks and logos are the property of their respective owners.  Red Hat provides source code for their Enterprise Linux which - once trademarks have been removed - is used to build core components of ClearOS.  We are not Red Hat.  Red Hat does in no way endorse ClearOS.

{{keywords>clearos, clearos content, announcements, releases, clearos6.2, final, previousfinal, maintainer_dloper}}

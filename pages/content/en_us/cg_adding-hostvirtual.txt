===== HostVirtual =====
In order to connect your  HostVirtual account with ClearGLASS, you will need an API Key. You can find the API key on the account section on HostVirtual. Login to https://www.hostvirtual.com and on the top right select Account. There select 'API keys' and copy the API KEY that will be there for you.

[image]

On ClearGLASS, press 'Add cloud' and select HostVirtual from the drop list. Enter the API key and press Add.

[image]

You should now be able to see your HostVirtual machines, neatly tagged for easy access. By selecting a machine you can run actions (reboot, stop, destroy). To provision new VMs, click on the Create button on the Machines section. By providing an ssh key on the Keys section, you will be able to connect through a web console, and enable monitoring to be able to see performance diagrams and set alerts and automated actions.

==== Related Articles ====
  * [[content:en_us:cg_setting-up-clearglass|Quick Start Guide]]
  * [[content:en_us:cg_manage-infrastructure|Managing Infrastructure]]
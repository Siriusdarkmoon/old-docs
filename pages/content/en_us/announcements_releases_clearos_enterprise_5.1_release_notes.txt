===== ClearOS Enterprise 5.1 Release Notes =====
The following release notes provide an overview of the ClearOS Enterprise 5.1 release. All users should read this document, but special attention should be made by users upgrading their systems to this release.

===== What's New =====
  * [[:content:en_us:5_windows_settings|Windows 7 Support]] with Samba 3.4.3
  * [[:content:en_us:5_antiphishing|Antiphishing]]
  * Graphical console tool
  * Improved usability in the web-based interface 
  * NTP server support
  * Core system upgraded to CentOS 5.4
  
In addition, the following new ClearOS services are available:
  * [[http://www.clearcenter.com/Services/clearsdn-antimalware-updates-7.html|Antimalware Updates]] (Antivirus, Antiphishing, etc)
  * [[http://www.clearcenter.com/Services/clearsdn-antispam-updates-8.html|Antispam Updates]] 
  * [[http://www.clearcenter.com/Services/clearsdn-remote-server-backup-6.html|Remote Server Backup]]

===== Service Packs =====
From time to time, Service Packs are released with all the latest updates and bug fixes included. If you have the original release or an older service pack installed, there is no need to re-install or upgrade! All you need to do is visit the Software Updates page in webconfig to install all the available updates.  

  * Released March 5, 2010 - [[:content:en_us:announcements_releases_clearos_enterprise_5.1_service_pack_1|Information on ClearOS Enterprise 5.1 Service Pack 1]]

===== Upgrading =====
ClearOS 5.x supports upgrades from ClarkConnect 4.x and later.  Upgrades from earlier versions (or systems originally installed with an earlier version) are not supported.  When you run the ClearOS installer, make sure you select the //upgrade// option.  As with any upgrade, please backup any critical data.

In addition, users who have installed third party software packages or used the command-line to install software should also run the following commands:

  yum upgrade

==== Upgrading from ClarkConnect 5.0 ====
For those of you upgrading from ClarkConnect 5.0, a live command line upgrade is available:

  yum install app-upgradeto51
  rpm -e --justdb --nodeps system-logos
  yum upgrade


===== Download =====
You can download ClearOS Enterprise 5.1 from the [[http://www.clearfoundation.com/Software/downloads|downloads]] section of the web site.

  * ClearOS Enterprise 5.1 First Release - MD5Sum: bbaff882fc04cdba58cc19772a2f0a45
  * [[:content:en_us:announcements_releases_clearos_enterprise_5.1_service_pack_1|ClearOS Enterprise 5.1 Service Pack 1]] - MD5Sum: 3b3358c754311a003d70df05d0198888

===== Creating Install/Upgrade Media =====
There are three ways to install or upgrade ClearOS:

  * Bootable CD
  * USB key (requires Internet access)
  * PXE-enabled network card 

===== Deprecated Packages =====
The following key packages are no longer included in this release:

==== 4.x to 5.x ====
  * apt (replaced by yum)
  * bpalogin (deprecated)
  * dev (replaced by udev)
  * dspam (deprecated)
  * gallery (unsupported, but still available)
  * pcmcia-cs (deprecated) 
  * squirrelmail (unsupported, but still available)  

===== Software Package Notes - ClarkConnect 4.3 to ClearOS 5.1 =====
<note>If you have only used the ClarkConnect web-based administration tool to configure and manage your system, the required changes for the upgrade are handled automatically. If you have customized configuration files or other aspects of your system, you may need to intervene to re-integrate your custom changes.</note>

==== Apt ====
Apt has been replaced by yum.

==== Bandwidth ====
The bandwidth management system is very different in ClarkConnect 4.x.  Old bandwidth rules will need to be re-configured using the new [[:content:en_us:5_bandwidth|Bandwidth]].  Port ranges are no longer supported.

==== Flexshare ====
Some options in the Flexshare system have been simplified.  In ClarkConnect 4.2 and earlier, it was possible to configure different groups for a single Flexshare. This option is no longer available -- a Flexshare can only be assigned to a single group.

==== Kernel ====
Support for very old CPUs has been dropped (i586 / Pentium 2 or earlier).

==== LAN Backup/Restore ====
The Bacula LAN Backup/Restore feature will be deprecated in a future release.  You can still use this powerful backup tool in ClearOS, but we expect to replace it with a simpler backup tool.  For those with fast networks, the [[http://www.clearcenter.com/Services/clearsdn-overview/Remote-Server-Backup.html|Remote Server Backup]] solution is a good option.

==== Peer-to-Peer Engine ====
The new [[:content:en_us:5_protocol_filter|Protocol Filter]] tool handles peer-to-peer blocking (among many other protocols).  If you have enabled peer-to-peer blocking in ClarkConnect 4.x, you should review and reconfigure your peer-to-peer policy 

==== Udev ====
The /dev/ system has been replaced by udev.

==== Web Proxy ====
The web proxy software has been upgraded to 2.6.STABLE21. I mportant features (notably IP reporting for the content filter) are not yet available in Squid 3.0.  For this reason, ClearOS will continue to use the same source code version used by Red Hat Enterprise Linux / CentOS.

==== Windows / Samba ====
The Windows/Samba software is now completely integrated into the ClearOS LDAP/directory system.  If you have configured your 4.x system as a primary domain controller (PDC), you will need to rejoin every workstation to the 5.x system.  This is an unfortunate but unavoidable requirement in the 4.x to 5.x upgrade.

Just to re-iterate the warning message above: if you have customized the Windows/Samba configuration outside of the webconfig, you will need to carefully review and possibly re-integrate those custom changes.  If you enabled roaming profiles in ClarkConnect 4.x, then we're looking at you. 

===== Software Package Notes - 4.0/4.1/4.2 =====
If you are upgrading from 4.0, 4.1 or 4.2, please review the ClarkConnect 5.0 release notes.
{{keywords>clearos, clearos content, announcements, releases, clearos5.1, final, previousfinal, maintainer_dloper}}

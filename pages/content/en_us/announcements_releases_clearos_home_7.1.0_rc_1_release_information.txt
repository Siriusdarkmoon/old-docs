===== ClearOS 7.1.0 Home Release Candidate 1 - Release Information =====
**Released: 8 October 2015**

ClearOS 7.1.0 Home RC1 is [[http://mirror.clearos.com/clearos/7/iso/x86_64/ClearOS-DVD-x86_64-7.1.0.iso|here]]!

ClearOS version 7.1 introduces:

  * ClearOS Home Release Candidate 1
  * ClearOS Business Release Candidate 1
  * IPv6 Ready (requires command line expertise)
  * Streamlined Theme System
  * Dynamic Dashboard
  * Updated Antispam and Antivirus Engines
  * Updated IDS and IPS Engines
  * Event and Alert Notification Framework
  * Improved Internationalization

==== ClearOS Home and ClearOS Business ====
This release introduces [[https://www.clearos.com/products/clearos-editions/clearos-7-comparison|ClearOS Home and ClearOS Business]] and allows you to select between versions using the same install media. At the time of release, qualified updates start. Meaning, Users of ClearOS Home and ClearOS Business from day one will have the same updates as Community users but as the release candidate cycle for Home and Business gets underway, users will have better and better quality assured updates as they are phased in throughout the Release Candidate cycle for Home and Business. Meaning, you can start using ClearOS for Home and Business today!

==== New Upstream Features Include ====

  * XFS and BTRFS Filesystem Support
  * Improved VM Support, including Microsoft Hyper-V

ClearOS 7 will feature a Community, Home, and Business version. All versions of ClearOS will install from the same install image. You will be required to select your version at the time of installation in the wizard.

==== Change Log ====
The full changelog can be found here:

  * [[https://tracker.clearos.com/changelog_page.php?version_id=142|Changelog - ClearOS 7.1.0 ]]


===== Known Issues =====

  * Owncloud (Coming soon, check back in the marketplace)
  * [[https://tracker.clearos.com/roadmap_page.php?version_id=132|Known bugs]]

===== Feedback =====
Please post your feedback in the [[https://www.clearos.com/clearfoundation/social/community/clearos-7-1-final-released-discussion|ClearOS 7.1 Final Discussion Forum]]. 


===== Download =====
==== ISO Images ====
^Download^Size^SHA-256^
|[[http://mirror.clearos.com/clearos/7/iso/x86_64/ClearOS-DVD-x86_64-7.1.0.iso|64-bit Full]]|797 MB|  4a187657e599a2a58d77f4411c1788e6400f9067d0414156e51b65e2a173b34a |
|[[http://mirror.clearos.com/clearos/7/iso/x86_64/ClearOS-netinst-x86_64-7.1.0.iso  |64-bit Net Install]]|373 MB|  b8df0dfdfbf1efe7c0bd0648a52ef00aec36e15b4c027798c6c31b22c4591d78 |


==== Virtual Machine and Cloud Images ====
Virtual Machine and Cloud Images are not yet available for this release.

==== Upgrade from Earlier ClearOS Community 6.x Releases ====
There is no supported method yet from upgrading from ClearOS 6 to ClearOS 7. Additionally, the layout for ClearOS 7 Beta 3 is different due to the changes in the build system to Beta 2. Upgrades from Beta 2 require changes to the repo content and performance of a dist-upgrade. There may be support for migration of configuration backups.


{{keywords>clearos, clearos content, announcements, releases, clearos7.1, final, clearos7.1.0, maintainer_dloper}}

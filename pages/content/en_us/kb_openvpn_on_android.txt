===== Connecting from Android to ClearOS over OpenVPN =====
/**
 * Comments can be given in this format.
 */

/* Comments can also be given in this format */

Connecting from your Android to your ClearOS OpenVPN server is fast, easy, secure, and relatively painless.

===== Preparation =====
You will need two things to make this work. You will need to download the VPN configuration from your ClearOS server. Log into ClearOS Webconfig using your username and password. Contact your administrator for the URL for Webconfig. You will need to log in as your user, not as root. Your Administrator should have already added you to the client certificate plugin group and also the openvpn plugin group. Once logged in, click on your username at the top right of the screen and choose “User Certificates”. If you have not set a password for your user certificate, do so now.

Next, you will need to download 4 files and get them to a folder on your Android device. You will need the ‘Certificate’, ‘Certificate Authority’, ‘Private Key’, and the Configuration File in ‘Linux’ for your Android phone. You will need to figure out how to get these to your phone. I copied the files to a folder and uploaded them to my home directory on ClearOS. Then I used the ES File Explorer app on Android to download the file to my SD Card’s Documents folder.

Next, you will need to download the app from the Android ‘Play Store’. I used the ‘OpenVPN for Android’ app by Arne Schwabe. These instructions will demonstrate that app.

{{:content:en_us:openvpn_for_android_download_and_install.png?550}}

===== Configuration =====
When you start the app for the first time, it will tell you that there are no profiles configured.

{{:content:en_us:openvpn_for_android_no_profiles.png?550}}

Click Import, to add the OpenVPN config file and certificates that you moved as a folder to your device. Navigate your device and find the files. When you find the ‘.ovpn’ file, click on it.

{{:content:en_us:openvpn_for_android_locate_config_and_certificates.png?550}}

This will bring up a confirmation screen. If the certificate files and keys are all in the same directory as the ‘.ovpn’ file, it will show you some of the headers of those files. Click ‘Import’.

The DNS name located in the ‘.ovpn’ file will typically be used to identify this connection. You can rename it if you wish. From here, click on the configuration.

{{:content:en_us:openvpn_for_android_supply_credentials.png?550}}

You are prompted for username and password for this connection. Supply the credentials and click ‘OK’. 

{{:content:en_us:openvpn_for_android_connected.png?550}}

Validate that your connection works by clicking on a resource. I did my validation using a Ping and DNS app to connect to a server on the inside of the network.

{{:content:en_us:openvpn_for_android_validated.png?550}}

All done. Click ‘Disconnect’ when you want to disconnect your OpenVPN session.



{{keywords>clearos, clearos content, OpenVPN, Android, app-openvpn, clearos7, categorynetwork, subcategoryvpn, maintainer_dloper}}

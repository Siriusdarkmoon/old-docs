===== Tutorials Adding User or Device Limits Simple Paid App =====
This document outlines how to implement [[developer:framework:tutorials:adding_user_or_device_limits|user limits]] for a paid app.  If plan on having multiple tiers for an app (for example, a free and a paid version), please read through this example and then jump to the [[:content:en_us:dev_framework_tutorials_adding_user_or_device_limits_multiple_tiers|Multiple Tiers]] example.

===== Subscription Class =====
In this example, we have a single paid app called **Acme** limited to 10 users.  As described in the [[:content:en_us:dev_framework_tutorials_adding_user_or_device_limits|user limits]] document, the **Acme** app requires a subscription class with a single //get_info()// method.  Here is an example to get you started:

<code php>
    public function get_info()
    {
        clearos_profile(__METHOD__, __LINE__);
 
        // Fill in the total, used and available data with actual data.
 
        $info = array(
            'app_name' => lang('acme_app_name'),
            'type' => Subscription_Engine::TYPE_USER,
            'total' => 10,
            'used' => 10,
            'available' => 0,
        );
 
        // The Acme app uses an LDAP extension: acme_extension. For the 
        // "User" app, we specify the attribute that should be disabled 
        // once the limit has been reached.
 
        $info['user_limit']['extension']['acme'] = array('account_flag');
 
        return $info;
    }
</code>

You can find the full source code here:

  * [[:content:en_us:dev_framework_tutorials_adding_user_or_device_limits_acme_subscription|Acme Subscription]] 

Please take special note of the following line in the //get_info()// method:

  $info['user_limit']['extension']['acme'] = array('account_flag');

This definition will prevent the "account_flag" in the Acme LDAP extension from being set when the limit has been reached.  Remember, this is just a convenience feature for the end user to prevent accidentally going over user limits.

===== Configlet =====
With the **Acme_Subscription** class completed, it can be activated by creating the **acme** file in /var/clearos/clearcenter/subscriptions.

  touch /var/clearos/clearcenter/subscriptions/acme

As a sanity check, go to the <navigation>System|Accounts|Users</navigation> in the menu.  Manually set the number of available licenses in your subscription class to return zero, and you should see a warning in the Users app similar to the screenshot below.

To finish up your paid app, make sure the equivalent of the /var/clearos/clearcenter/subscriptions/acme file is included in your apps deploy/info.php file!

{{keywords>clearos, clearos content, dev, framework, maintainer_dloper, maintainerreview_x, keywordfix}}

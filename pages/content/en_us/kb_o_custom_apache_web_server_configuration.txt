===== Custom Apache Web Server Configuration =====
This document describes how to customize an Apache configuration file for the default web site or virtual hosts.

=====
In some circumstances, you may want to customize the Apache configuration files for your web sites.  For the default web site (not virtual hosts), cut the relevant sections out of **/etc/httpd/conf.d/flex-443.conf** and **/etc/httpd/conf.d/flex-80.conf** and paste them into **clearos.default.conf**.  Here is a sample from a test system:


  <VirtualHost *:443>
    ServerName clear6.lan
    DocumentRoot /var/www/html
    ErrorLog /var/log/httpd/error_log
    CustomLog /var/log/httpd/access_log combined
    SSLEngine on
    SSLCertificateFile /etc/pki/tls/certs/localhost.crt
    SSLCertificateKeyFile /etc/pki/tls/private/localhost.key
    # No weak export crypto allowed
    # SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP:+eNULL
    SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:!EXP:+eNULL
    SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown downgrade-1.0 force-response-1.0
  </VirtualHost>
  
  <VirtualHost *:80>
    ServerName clear6.lan
    DocumentRoot /var/www/html
    ErrorLog /var/log/httpd/error_log
    CustomLog /var/log/httpd/access_log combined
  </VirtualHost>
  
  <Directory /var/www/html>
    Options +Indexes -FollowSymLinks -IncludesNOExec
    AllowOverride All
    Order deny,allow
    Allow from all
    AddType text/html .php
    AddHandler php5-script .php
  </Directory>

You can change this configuration file meet your needs and the ClearOS [[:content:en_us:6_web_server|Web Server]] app will not overwrite it.  Please do not forget to restart the web server after making the custom changes.

When you go to the Web Server app in ClearOS, you will see a warning message about your custom configuration.  You can still change the upload access permissions via the ClearOS web interface, but the Apache configuration will remain unchanged.

{{keywords>clearos, clearos content, AppName, app_name, kb, howto, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

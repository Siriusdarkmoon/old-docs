===== Events Framework =====

The ClearOS Events framework was introduced in version 7 as a way to track important system events and make them available to administrators via Webconfig or email alert.

In the past, if it was desirable to have an alert created and sent to an admin, each individual app had to incorporate its own code, email variable and forms to handle this requirement.  The Events framework was introduced to make it simple for developers to hook into a master event system that was capable of logging events, their priority and send out alerts in specific cases.

===== Installation =====

The Events app (app-events and it's supporting library, app-events-core) is installed by default and can be assumed to be available since it has dependencies hooked back to the core Webconfig engine.

===== Libraries =====
==== Events Class ====

The Events class mainly handles the end-user requests for displaying, acknowledging and deleting events.  To a developer creating an app and integrating the use of Events in their app, this library is not the most useful.

==== Event_Utils ====

In contrast, the Event_Utils class provides a set of convenience function for creating alerts and changing status (for events that are 'auto-correcting').

===== Registering Events =====

Before an event can be created, it must be registered.  Registered events are defined in an apps deploy/info.php file.  Typically, you would create the event with the following syntax:

  BASENAME_UNIQUE-DESCRIPTOR

Example:

  /////////////////////////////////////////////////////////////////////////////
  // App Events
  /////////////////////////////////////////////////////////////////////////////
  
  $app['event_types'] = array(
      'REGISTRATION_UNREGISTERED',
  );
  
Add as many unique events as you wish.

On an app install or upgrade, these events will automatically get registered to the Events engine.  You can check using the following command:

  eventsctl -t list

===== Creating Events =====

There are two ways in which you can create an event:
  *  Via convenience functions in API
  *  Via the events log watch and regex matching engine

==== API ====

=== Example ===

  <?php
  
  /**
   * Users controller.
   *
   * @category   apps
   * @package    users
   * @subpackage controllers
   * @author     ClearFoundation <developer@clearfoundation.com>
   * @copyright  2011 ClearFoundation
   * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
   * @link       http://www.clearfoundation.com/docs/developer/apps/users/
   */
  
  ///////////////////////////////////////////////////////////////////////////////
  //
  // This program is free software: you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation, either version 3 of the License, or
  // (at your option) any later version.
  //
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  //
  // You should have received a copy of the GNU General Public License
  // along with this program.  If not, see <http://www.gnu.org/licenses/>.
  //
  ///////////////////////////////////////////////////////////////////////////////
  
  ///////////////////////////////////////////////////////////////////////////////
  // D E P E N D E N C I E S
  ///////////////////////////////////////////////////////////////////////////////
  
  use \clearos\apps\accounts\Accounts_Engine as Accounts_Engine;
  use \clearos\apps\events\Event_Utils as Event_Utils;
  use \clearos\apps\events\Events as Events;
  use \clearos\apps\accounts\Accounts_Not_Initialized_Exception as Accounts_Not_Initialized_Exception;
  use \clearos\apps\accounts\Accounts_Driver_Not_Set_Exception as Accounts_Driver_Not_Set_Exception;
  use \clearos\apps\groups\Group_Engine as Group_Engine;
  
  ///////////////////////////////////////////////////////////////////////////////
  // C L A S S
  ///////////////////////////////////////////////////////////////////////////////
  
  /**
   * Users controller.
   *
   * @category   apps
   * @package    users
   * @subpackage controllers
   * @author     ClearFoundation <developer@clearfoundation.com>
   * @copyright  2011 ClearFoundation
   * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
   * @link       http://www.clearfoundation.com/docs/developer/apps/users/
   */
  
  class Users extends ClearOS_Controller
  {
      /**
       * Users overview.
       *
       * @return view
       */
  
      function index()
      {
          // Show account status widget if we're not in a happy state
          //---------------------------------------------------------
  
          $this->load->module('accounts/status');
  
          if ($this->status->unhappy()) {
              $this->status->widget('users');
              return;
          }
      }
  ...
  ..
  .
      /**
       * Destroys user.
       *
       * @param string $username username
       *
       * @return view
       */
  
      function destroy($username)
      {
          // Load libraries
          //---------------
  
          $this->load->factory('users/User_Factory', $username);
          $this->load->library('events/Events');
          $this->load->library('events/Event_Utils');
  
          // Handle form submit
          //-------------------
  
          try {
              $this->user->delete();
              Event_Utils::add_event(
                  lang('base_administrator') . ' ' . $this->session->userdata('username') . ': ' . lang('users_deleted_account') . ' "' . $username . '"',
                  Events::SEVERITY_WARNING, 'USERS_DELETE_USER', 'users'
              );
              $this->page->set_status_deleted();
              redirect('/users');
          } catch (Exception $e) {
              $this->page->view_exception($e);
              return;
          }
      }
  ...
  ..
  .
  }

==== Convenience Functions ====

=== Info Event ===

  Event_Utils::info('Hello World', EVENT_TYPE);

=== Warning Event ===

  Event_Utils::warning('Hello World', EVENT_TYPE);

=== Critical Event ===

  Event_Utils::critical('Hello World', EVENT_TYPE);


==== Event Log Watch ====

=== Example ===

{{keywords>clearos, clearos content, dev, framework, maintainer_dloper, maintainerreview_x, keywordfix}}

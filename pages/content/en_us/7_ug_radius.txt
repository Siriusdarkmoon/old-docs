===== RADIUS server =====
The **RADIUS Server** app provides a hook into your ClearOS accounts system for RADIUS clients.  This app can be used to allow external devices to authenticate against ClearOS:

  * Wireless access points
  * NAS devices
  * 3rd party appliances
  * Any other RADIUS-ready system

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Network|Infrastructure|RADIUS Server</navigation>

===== Configuration =====
After installing the RADIUS server, you can add client access settings.  When we talk of the client we are talking about a remote system that uses the RADIUS server.  For instance, if you are configuring RADIUS for wireless access, the wireless access point is the RADIUS client, //not// your workstation that uses the wireless.

==== Certificates ====
Although there is a folder /etc/raddb/clearos-certs/ containing certificates, these are not currently used. The certificates in use are in /etc/raddb/certs/.

When radius is installed the initial bootstrap certificates are created with a 60 day validity. Windows 10 will object with a "ERROR: TLS Alert read:fatal:access denied" message when these expire. To regenerate them, firstly, if you want, change any key fields you want to contain meaningful data in your certificates such as the organizationName by editing /etc/raddb/certs/ca.cnf and /etc/raddb/certs/server.cnf (and /etc/raddb/certs/client.cnf if you want client certificates). You probably want to change the default_days (certificate validity period) to something greater than 60 days. Then regenerate the certificates with a:
  cd /etc/raddb/certs
  rm -f *.pem *.der *.csr *.crt *.key *.p12 serial* index.txt*
  make all
  systemctl restart radiusd.service
==== Clients ====
Every device on your system that uses RADIUS should have client settings configured in ClearOS.  The following describes each parameter in detail.

=== IP Address ===
This is the host IP address or hostname of the remote system, for example your wireless access point.

=== Nickname ===
The nickname is just a simple word to describe the client configuration, for example **wireless_ap**.

=== Password ===
This is the shared secret between the server and the client.  For security reasons, you should //not// use the same password as you do for other administrative accounts (mysql, root, etc).
{{keywords>clearos, clearos content, AppName, app_name, clearos7, userguide, categorynetwork, subcategoryinfrastructure, maintainer_dloper, maintainerreview_x}}

==== Windows 10 Domain Workstation ====

<note important>
At this point, you should have already joined your Windows 10 workstation to the Windows Networking (Samba) domain.
</note>

Go to 'Control Panel' > 'Network and Internet' > 'Network and Sharing Center'.\\
Click on 'Set up a new connection or network'.\\
{{content:en_us:7_radius_windows10_config_01.png}}\\
Select 'Manually connect to a wireless network' and click on 'Next'.\\
{{content:en_us:7_radius_windows10_config_02.png}}\\
Enter the SSID of the wireless network in the 'Network name' field.\\
{{content:en_us:7_radius_windows10_config_03.png}}\\
Select 'WPA2-Enterprise' in the dropdown for 'Security type'.\\
You can choose to enable or disable the parameters 'Start this connection automatically' and 'Connect even if the network is not broadcasting'.\\
Click on 'Next'.\\
{{content:en_us:7_radius_windows10_config_04.png}}\\
Click on 'Change connection settings'.\\
{{content:en_us:7_radius_windows10_config_05.png}}\\
Click on the 'Security' tab.\\
{{content:en_us:7_radius_windows10_config_06.png}}\\
Ensure 'Microsoft: Protected EAP (PEAP) is selected under 'Choose a network authentication method:'.\\
You can choose to enable or disable the parameter 'Remember my credentials for this connection each time I'm logged on'.\\
Click on 'Settings'.\\
{{content:en_us:7_radius_windows10_config_07.png}}\\
Deselect 'Verify the server's identity by validating the certificate' if you do not have a certificate installed via Active Directory.
(Otherwise, you will receive a prompt asking you whether you would like to connect to the network.)\\
{{content:en_us:7_radius_windows10_config_08.png}}\\
Click 'OK' to return to the 'Wireless Network Properties' dialog box.\\
Click on 'Advanced settings'.\\
{{content:en_us:7_radius_windows10_config_09.png}}\\
Select 'Specify authentication mode:'.\\
{{content:en_us:7_radius_windows10_config_10.png}}\\
Click 'OK' to return to the 'Wireless Network Properties' dialog box.\\
Click 'OK' in the 'Wireless Network Properties' dialog box.\\
{{content:en_us:7_radius_windows10_config_11.png}}
===== Central Management OpenLDAP Replication Settings =====
OpenLDAP replication is one of the building blocks of [[:content:en_us:dev_architecture_central_management_start|Central Management]] and [[:content:en_us:dev_architecture_central_management_ldap_replication_framework]].  This document outlines details specific to OpenLDAP and its configuration. 

===== Howto =====
NOTE: these settings are for two servers on the same network segment. It will take additional configuration for servers on separate subnets.

  * Use Webconfig to publish policy on master server
  * On replicate server, edit /etc/openldap/slapd.conf
  * Comment the following lines
    * moduleload syncprov.la
    * overlay syncprov
    * syncprov-nopresent TRUE
    * syncprov-reloadhint TRUE
    * overlay syncprov
    * syncprov-checkpoint 100 5
    * overlay accesslog
    * logdb cn=accesslog
    * logops writes
    * logsuccess TRUE
    * logpurge 32+00:00 01+00:00


  * Add the following lines after the index section (index  kolabDeleteflag pres,eq). Change the text between the double-asterisk to values specific to your system

   #syncrepl consumer
  syncrepl
  rid=0
  provider=ldaps://**ip address of the master**:636
  bindmethod=simple
  searchbase="dc=**domain name**,dc=**domain tld**"
  binddn="cn=manager,cn=internal,dc=**domain name**,dc=**domain tld**"
  credentials=**add the password from the bind_pw line in /etc/kolab/kolab.conf**
  filter="(objectclass=*)"
  schemachecking=on
  type=refreshAndPersist
  syncdata=accesslog
  retry="15 +"
  logbase="cn=accesslog"
   # Refer updates to the master
  updateref               ldap://**ipaddress of master**

  * On the replicate server, you should also set the "is_master" flag in /etc/kolab/kolab.conf to false.
  * Restart the LDAP Database on the replicate server with 'service ldap restart'

===== Notes and Code =====

==== Sync Provider module on Replicate ====

Need to find if we need this on the replicate as it acts as a replicate. Peter's code works with it. Dave's code works without it.

  moduleload syncprov.la

This module loads the synchronization overlay capability. Replication works in Dave's version because the replicate does not need to provide synchronization. Because Peter's does not use the 'overlay syncprov' directive, this module is not used. If the replicate becomes the master, this directive should be placed back in.

==== Replica parameters ====

=== Examples ===

I've only listed the key differences and talking points from Dave and Peter's prototypes. Compare the following:

== Peter's ==

  rid=0
  provider=ldap://192.168.2.248:389
  attrs="*"
  schemachecking=off
  scope=sub
  type=refreshOnly
  interval=00:00:00:10

== Dave's ==

  rid=123
  provider=ldap://10.10.10.1:389
  schemachecking=on
  type=refreshAndPersist
  syncdata=accesslog
  retry="15 +"
  logbase="cn=accesslog"

=== Parameters ===

== rid ==

//rid identifies the current syncrepl directive within the replication consumer site. It is a non-negative integer not greater than 4095 (limited to three hexadecimal digits).//

It appears that this value is arbitrary to the point that it just needs to be unique within the specific replicate server and does not need to be coordinated between replica servers in such a way as to be unique.

== provider ==

We both used the same provider mechanisms for the replica in our test. This should, however be on LDAPS (port 636). The line should be in a final version:

  provider=ldaps://ip_address:636/

== attrs ==

Peter had this directive in his configuration. The default value is similar. I'm still looking to see what the '+' part does. It may be useful just to drop the definition and use the default.

//The content of the syncrepl replica is defined using a search specification as its result set. The  consumer  slapd  will  send search requests to the provider slapd according to the search specification. The search specification includes searchbase, scope, filter, attrs,  attrsonly, sizelimit, and timelimit parameters as in the normal search specification. The scope defaults to sub, the filter defaults to (objectclass=*), while there  is  no  default  searchbase.  The attrs  list  defaults to "*,+" to return all user and operational attributes, and attrsonly is unset by default.  The sizelimit and timelimit only accept "unlimited" and positive  integers, and  both  default  to  "unlimited".  The sizelimit and timelimit parameters define a consumer requested limitation on the number of entries  that  can  be  returned  by  the  LDAP  Content Synchronization  operation;  as such, it is intended to implement partial replication based on the size of the replicated database and on the time required by  the  synchronization.   Note, however,  that  any  provider-side limits for the replication identity will be enforced by the provider regardless of the limits requested by the  LDAP  Content  Synchronization  operation, much like for any other search operation.//

== schemachecking ==

//The  schema  checking  can  be enforced at the LDAP Sync consumer site by turning on the schemachecking parameter. The default is off. Schema checking on means that replicated entries must have a structural objectClass, must obey to objectClass requirements in terms of required/allowed  attributes,  and that naming attributes and distinguished values must be present.  As a consequence, schema checking should be off when partial replication is used.//

This may be useful for making sure only valid entries make it to the replicate.


== scope ==

The default parameter is 'sub' for scope, probably not necessary to specify it here.

== type ==

//The LDAP Content Synchronization protocol has two operation types.  In the refreshOnly  operation,  the  next  synchronization  search operation  is  periodically  rescheduled  at  an  interval  time  (specified  by  interval  parameter;  1  day  by default) after each synchronization operation finishes.  In the refreshAndPersist operation, a synchronization search remains persistent in  the  provider slapd.   Further  updates  to  the master replica will generate searchResultEntry to the consumer slapd as the search responses to the persistent synchronization search.//

This deserves some discussion.


== interval ==

If we use 'refreshOnly' then we need to specify this parameter. We don't need this with 'refreshAndPersist' since the sync would be persistent.

== retry ==

//If an error occurs during replication, the consumer will attempt to reconnect according to the retry parameter which is a list of  the <retry interval> and <# of retries> pairs.  For example, retry="60 10 300 3" lets the consumer retry every 60 seconds for the first 10 times and then retry every 300 seconds for the next 3 times before stop retrying. The ‘+’ in <# of retries> means indefinite number of retries until success.//

This parameter is useful for controlling how 'chatty' the synchronization is when there is a failure. We may not need this if we use 'refreshOnly'. In addition this should be tunable from Webconfig depending on whether the replicate server is local or across a WAN/VPN.

== syncdata ==

// Rather than replicating whole entries, the consumer can query logs of data modifications. This mode of operation  is  referred  to  as delta  syncrepl.  In addition to the above parameters, the logbase and logfilter parameters must be set appropriately for the log that will be used. The syncdata parameter must be set to either "accesslog" if the log conforms to the slapo-accesslog(5)  log  format,  or "changelog"  if  the  log conforms to the obsolete changelog format. If the syncdata parameter is omitted or set to "default" then the log parameters are ignored.//

This may improve performance.

== logbase ==

See above *syncdata*.

Should be used together if used.

===== Related Links =====
  * [[:content:en_us:dev_architecture_central_management_start|Central Management]]
  * [[:content:en_us:dev_architecture_central_management_ldap_replication_framework]]
{{keywords>clearos, clearos content, dev, architecture, maintainer_dloper}}

===== ClearOS 7.5.0 Community Release Information =====
**Released: 5 July 2018**

ClearFoundation and ClearCenter are proud to announce the community release of ClearOS 7.5.0 This is a minor release of ClearOS and focuses on important security updates as well as new applications in the ClearCenter Marketplace.

==== New Features Included in 7.5.0 Release and Since 7.4.0 ====

  * LetsEncrypt
  * Dynamic Firewall
  * 2-Factor Authentication for Webconfig
  * ClearGLASS

==== New Upstream Features and improvements Include ====

  * Samba rebased to version 4.7.1
  * parted can now resize partitions using the resizepart command

ClearOS 7 features a Community, Home, and Business version. All versions of ClearOS will install from the same install image. You will be required to select your version during the initial setup wizard on the first boot after ClearOS is installed.

==== Change Log ====
The full change logs can be found here:

  * [[https://tracker.clearos.com/changelog_page.php?version_id=381|Changelog - Changes since ClearOS 7.4.0 ]]
  * [[https://tracker.clearos.com/changelog_page.php?version_id=421|Changelog - ClearOS 7.5.0 ]]


===== Roadmap =====


=== Roadmap Tracker and Open Bugs ===
  * [[https://tracker.clearos.com/roadmap_page.php|Known bugs]]

===== Feedback =====
Please post your feedback in the [[https://www.clearos.com/clearfoundation/social/community/clearos-7-5-community-release|ClearOS 7.5 Community Discussion Forum]]. 


===== Download =====
==== ISO Images ====
The ISO image will be available after the full release of ClearOS 7.5.0 when it can support Home and Business versions in addition to Community.

==== Virtual Machine and Cloud Images ====
Virtual Machine and Cloud Images are not yet available for this release.

^ Platform ^ Image ^ Proceedure ^
| Amazon EC2 | clearos-7.2.0 - ami-8fb03898 | Install and run updates to 7.5 |

==== Upgrade from Earlier ClearOS Community 6.x Releases ====
There is no supported method yet from upgrading from ClearOS 6 to ClearOS 7. 

Make a copy of your configuration data. Backup your user data as well or retire your ClearOS 6 disk. ClearOS 7 now supports the import of your configuration data but not your user data.

==== Upgrade from Earlier ClearOS Community 7.x Releases ====
Upgrades from 7.1.0 on properly running systems will happen automatically. To manually upgrade from ClearOS 7.1.0 or to validate that you are at the latest version run:

  yum update

{{keywords>clearos, clearos content, announcements, releases, clearos7.4, final, clearos7.4.0, maintainer_dloper}}

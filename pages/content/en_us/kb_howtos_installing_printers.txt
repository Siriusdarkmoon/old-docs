===== Installing Printers on the ClearOS server =====
  - Go to Advanced Printer Services in the <COS Server> control page. Click “Configure”
  - Login with “root” 
  - Click “Find New Printers” 
  - If the printer is found then:
  -   - Follow the instructions. 
  -   - If the driver is not found then
  -   -   - Search the web for a PPD file for it and upload it.
  - If the printer is not found then: 
  -   - Go back and click “Add Printer” 
  -   - Fill in the data (Name should be less than 16 characters) 
  -   - Pick the device 
  -   - Enter the URL (click “Network Printers” near the bottom for known entries) 
  -   - Pick a make and model or upload a PPD file.

===== Uploading the printers' Windows drivers =====
  - Next log into a Windows client with “winadmin”. 
  - Launch MS Windows Explorer. Navigate in the left panel. Click My Network Places → Entire Network → Microsoft Windows Network → <Domain> → <COS Server>. Click on <COS Server> Printers and Faxes. 
  - Identify a printer that is shown in the right panel. Let us assume the printer is called ps01-color. Right-click on the Printer-01 icon and select the Properties entry. This opens a dialog box that indicates that “The printer driver is not installed on this computer. Some printer properties will not be accessible unless you install the printer driver. Do you want to install the driver now?” **It is important at this point you answer No.**
  - The printer properties panel for the Printer-01 printer on the <COS Server> is displayed. Click the Advanced tab. Note that the box labeled Driver is empty. Click the New Driver button that is next to the Driver box. This launches the “Add Printer Wizard”. 
  - The “Add Printer Driver Wizard on <COS <COS Server>>” panel is now presented. Click Next to continue. From the left panel, select the printer manufacturer. In the right panel, select the printer or browse to folder if you had to download the drivers. Click Next, and then Finish to commence driver upload. A progress bar appears and instructs you as each file is being uploaded and that it is being directed at the network server \\<COS <COS Server>>\printer-01.". 
  - The driver upload completes in anywhere from a few seconds to a few minutes. When it completes, you are returned to the Advanced tab in the Properties panel. You can set the Location (under the General tab) and Security settings (under the Security tab). Under the Sharing tab it is possible to load additional printer drivers. 
  - Click OK. It will take a minute or so to upload the settings to the server. You are now returned to the Printers and Faxes on <COS Server> monitor. 
  - Right-click on the printer, click Properties → Device Settings. Now change the settings to suit your requirements. BE CERTAIN TO CHANGE AT LEAST ONE SETTING and apply the changes even if you need to reverse the changes back to their original settings. This is necessary so that the printer settings are initialized in the Samba printers database. Click Apply to commit your settings. Revert any settings you changed just to initialize the Samba printers database entry for this printer. If you need to revert a setting, click Apply again. 
  - Verify that all printer settings are at the desired configuration. When you are satisfied that they are, click the General tab. Now click the Print Test Page button. A test page should print. Verify that it has printed correctly. Then click OK in the panel that is newly presented. Click OK on the Printer-01on massive Properties panel. 
  - You must repeat this process for all network printers (i.e., for every printer on each server).

===== Installing the printers on the Windows Clients =====
  - Log into the client with an account that has Administration rights.
  - Click My Network Places → Entire Network → Microsoft Windows Network → <Domain> → <COS Server>. Click on <COS Server> Printers and Faxes.
  - Select the printer(s), Right mouse click and select "Connect".
  - The printer(s) and driver(s) will be automatically installed. Fast and simple.
{{keywords>clearos, clearos content, kb, clearos5, clearos6, categoryserver, howtos, printers, app-print_server, maintainer_dloper}}

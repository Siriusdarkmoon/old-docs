===== Advanced Firewall =====
The advanced firewall tool can be used to create special firewall rules.  For instance, you can use this tool to allow connections to webconfig from the Internet -- but only from a particular IP address.  Please use with caution!  

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:

<navigation>Network|Firewall|Advanced</navigation>

===== Configuration =====
<note warning>An invalid advanced rule will cause the firewall to go into a lock-down mode -- all other firewall rules will not be active in this mode.</note>

===== Examples =====

==== Allowing Access to Port X from Single Remote IP Address ====
  * Scenario: Opening a port for remote administration but only for a particular IP address.
  * Example: Webconfig access for remote IP 69.90.141.13
{{:omedia:advanced_remote_ip.png?600|Allowing Access to Port X from Single Remote IP Address}}

==== Allowing Access to Port X on a Virtual IP Address ====
  * Scenario: Opening a port for a virtual IP address configured on the system.
  * Example: Secure web server access on virtual IP 1.2.4.5
 {{:omedia:advanced_virtual_ip.png?600|Allowing Access to Port X on a Virtual IP Address}}

==== Forwarding Port X from Single Remote IP Address ====
  * Scenario: Forwarding a port to the local network but only for a particular IP address.
  * Example: SSH access to 192.168.2.16 on the LAN, but only from 69.90.141.13 

{{keywords>clearos, clearos content, Advanced Firewall, app-advanced_firewall, clearos5, userguide, categorynetwork, subcategoryfirewall, maintainer_dloper}}

{{ :userguides:mail_notification.svg?80}}
<WRAP clear></WRAP>

===== Mail Notification =====
Many apps and services in the Marketplace use email to notify administrators of events that may require their attention.  The mail notification app provides an MTA to relay messages sent from the server - useful if you are not running a full SMTP (mail) service on the server.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/system/mail_notification|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_mail_notification|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_mail|here]].
==== Additional Notes ====

{{ :userguides:mail_antispam.svg?80}}
<WRAP clear></WRAP>

===== Mail Antispam =====
An open-source antispam scanner for the SMTP Mail Server app. This app will scan mail during messaging handling and perform functions defined by rules configured by the administrator.  For commercial applications, ClearCenter offers additional spam rules that are continually updated as a service.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/server/mail_antispam|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_mail_antispam|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_mail_antispam|here]].
==== Additional Notes ====

===== Manipulating Autoresponder Behavior in Zarafa =====
This guide covers manipulation of the auto-responder behavior in Zarafa on ClearOS. By default, the auto-responder will only send 'Out of Office' messages to the person emailing (sender) the Zarafa account (recipient) for the first email within each time period reached (1 day by default). Other methods are also tunable under the following parameters:

  * recipient is in CC
    * DISABLED
  * reply even if recipient address is unstated, (ie. BCC, mail lists)
    * DISABLED
  * Time to live (TTL) before sending additional out of office message
    * 24 hours (24*60*60= 86400 seconds or 24 hours)
  * Location of TTL database
  * Location of TTL database temporary files
  * Location of SMTP engine to use to send data
  * Parameters to use for mail send

The autoresponder configuration file is located in **/etc/zarafa/autorespond**

===== Configuration =====
Admins will typically only need to change the first 3 values and typically only the first and 3rd value. The default file looks like this:

<code>
# -*- Mode: sh -*-
###########################################
# Zarafa autoresponder settings
###########################################

# Autorespond if we were only in the Cc of a message 
AUTORESPOND_CC=0

# Autorespond if we were not in the message at all
AUTORESPOND_NORECIP=0

# Only send reply to same e-mail address once per 24 hours
TIMELIMIT=$[24*60*60]

# File which contains where vacation message was sent
SENDDB=${TMP:-/tmp}/zarafa-vacation-$USER.db

# Tempfile containing message that will be send
SENDDBTMP=${TMP:-/tmp}/zarafa-vacation-$USER-$$.tmp

# Customize your actual mail command, normally sendmail
# Input to this command is the message to send
SENDMAILCMD=/usr/sbin/sendmail

# Additional parameters for the $SENDMAILCMD
# The last parameter added to the $SENDMAILCMD is $FROM,
# so take that into account for the $SENDMAILPARAMS
SENDMAILPARAMS="-t -f"
</code>

<note warning>After making changes to the autorespond configuration file, you will need to restart the zarafa-dagent process: **service zarafa-dagent restart**</note>
|
=== AUTORESPOND_CC ===
To make the auto-responder reply to CC messages, change the value from 0 to 1.

=== TIMELIMIT ===
The default value is listed mathematically (ie. 24*60*60). The value should be represented in seconds. In this case 24*60*60 would be the same as 84600.

<note warning>To avoid your system becoming subject to a denial of service attack, please do NOT set the time limit too low. This would allow attackers to inundate your server to a forced response</note>


===== Links =====
  * [[http://doc.zarafa.com/7.1/Administrator_Manual/en-US/html/_autoresponder.html#Zarafa Autoresponder]]

{{keywords>clearos, clearos content, Zarafa, app-zarafa-community, app-zarafa-small-business, app-zarafa-home, clearos6, clearos7, categoryserver, subcategorycommunicationandcollaboration, maintainer_dloper}
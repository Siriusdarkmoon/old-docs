===== Time Class Source Code =====
To give you a feel for the code in the ClearOS API, here is a sampling of the Time class -- the Time.GetTimeZoneList() method is shown below.

<code php>
class Time extends Engine
{
    ... snip ...
    
    /**
     * Returns a list of available time zones on the system.
     * 
     * @return array a list of available time zones
     * @throws EngineException
     */

    public function GetTimeZoneList()
    {
        if (COMMON_DEBUG_MODE)
            $this->Log(COMMON_DEBUG, "called", __METHOD__, __LINE__);

        $zonelist = array();

        $zoneinfo = array();

        foreach (explode(" ", self::PATH_ZONEINFO_SUBDIR) as $subzone) {
            try {
                $folder = new Folder(self::PATH_ZONEINFO . "/" . $subzone);
                $subzones = $folder->GetListing();
            } catch (Exception $e) {
                throw new EngineException($e->GetMessage(), COMMON_ERROR);
            }

            foreach ($subzones as $zone) {
                $md5 = md5_file(self::PATH_ZONEINFO . "/$subzone/$zone");
                $zoneinfo["fullzone"] = "$subzone/$zone";
                $zoneinfo["subzone"] = $subzone;
                $zoneinfo["zone"] = $zone;
                $zoneinfo["md5"] = $md5;
                $zonelist[] = $zoneinfo;
            }
        }

        return $zonelist;
    }

    ... snip ...
}
{{keywords>clearos, clearos content, dev, framework, maintainer_dloper, maintainerreview_x, keywordfix}}

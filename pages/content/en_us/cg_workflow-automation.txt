===== Workflow Automation =====
ClearGLASS provides a set of tools that can be used to automate the configuration of simple and complex applications. 

==== Who should consider workflow automation? ====
  * Software development teams that want to automate the configuration of newly created systems or modify existing servers.
  * IT operations that want to automate the setup of complex environments, including the app, networking, devices, etc.
==== How it works? ====
  - Add your infrastructure.
  - Add your bash scripts, Ansible playbooks, and/or Cloudify blueprints.
  - Run them on the clouds and machines you added to ClearGLASS.
**Tip**: Combine workflow automation with RBAC to enable self-service for development and ops teams.
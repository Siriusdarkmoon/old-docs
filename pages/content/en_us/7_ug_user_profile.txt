===== User Profile =====
The user profile page gives end users a place to change their password and other personal settings. All users, by default, have access to this app.

===== Configuration =====
An end user or admin can access their profile page by providing their username and password in [[:content:en_us:7_ug_webconfig|Webconfig]] by navigating to The user can navigate to:
  https://IP_ADDRESS_OR_HOSTNAME:81

<note>Change the 'IP_ADDRESS_OR_HOSTNAME' part of the URL listed here to the actual IP address or hostname of your ClearOS server.</note>

{{7_ug_user_profile.png}}

From here they can change their password.

They can also access their [[:content:en_us:7_ug_user_certificates|User Certificates]] (if they have been allowed) by clicking on their username and accessing the dropdown menu.

{{7_ug_user_profile_user_certificates.png}}


{{keywords>clearos, clearos content, User Profile, app-user-profile, clearos7, userguide, categorysystem, subcategorymyaccount, maintainer_dloper}}

===== Upgrading ClearGLASS Community to ClearGLASS Business =====

Essentially, you can upgrade to Business by purchasing and installing ClearGLASS Business from the ClearOS Marketplace.\\
\\
For volume pricing or more information about the ClearCenter Partner program, please contact the Sales team at <sales@clearcenter.com>.
===== PPP =====
The ppp package contains the PPP (Point-to-Point Protocol) daemon and documentation for PPP support. The PPP protocol provides a method for
transmitting datagrams over serial point-to-point links. PPP is usually used to connect to an ISP (Internet Service Provider) via PPPoE or to provide PPTP VPN services.

===== Patches =====
==== ppp-x-devname.patch ====
{{keywords>clearos, clearos content, AppName, app_name, version6, dev, packaging xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

{{ :userguides:dns.svg?80}}
<WRAP clear></WRAP>

===== DNS Server =====
The DNS Server app provides a localised DNS server for the LAN.  DNS provides resolution for mapping IP addresses to easily remembered hostnames (i.e. db1.lan, web1.lan etc.).

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/network/dns|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_dns|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_dns|here]].
==== Additional Notes ====

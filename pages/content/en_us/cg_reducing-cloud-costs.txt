===== Reducing Cloud Costs =====
ClearGLASS provides a set of easy-to-use tools that you can use to manage and reduce your cloud infrastructure costs.

==== Who should use cost management tools? ====
  * Software development teams that use multiple clouds to do development, testing, and QA.
  * IT Operations that manage heterogeneous infrastructure.
==== How does it work? ====
  - Add your infrastructure.
  - ClearGLASS dynamically provides pricing details for all environments.
  - Add tags to machines to group infrastructure by app, groups, people, etc.
  - Schedule non-critical machines to turn off automatically to avoid costs.
  - These tools can work across all your environments, making it easy to implement and manage.
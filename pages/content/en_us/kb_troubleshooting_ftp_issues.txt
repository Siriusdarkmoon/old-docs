===== Client FTP Issues =====
FTP is an old protocol. Its emphasis, when it was created, was not on security and so there are some aspects of it that is more suited to a kinder, gentler internet. 

===== Issues =====
FTP can be a tricky protocol to work with some times because it doesn't use a symmetrical communications stack.

The first aspect of FTP is the command channel. The second is the data channel. 

=== Command Channel ===
The command is used for authentication and the issuance of commands. The command channel port is what we typically associate with FTP. Its port is usually port 21. Through the command channel we talk to the server, authentication, and issue commands.

=== Data Channel ===
The data channel is more difficult to identify because, by design, it uses random ports.

If you can log in to the resource but cannot list the directories then it is a good sign that your command channel is functioning but that the data channel is failing. This is one of the most common issue with FTP.

==== Modes ====
There are two styles of FTP that exist to handle the data channel: Active and Passive.

=== Active ===
Active mode means that when you establish communication with the server the server will declare a port on the client side that should be opened in order to receive the data. This can be a problem for some firewalls because they will let data out but not data in. Most modern firewalls watch the communications on port 21 and try to catch the data message. Dynamically they will open the port. However, when a port other than port 21 is used for the command channel (e.g. 2121), the firewall at the client's side may not be aware that it should engage in this behavior. For this reason Active mode will fail for most users.

=== Passive ===
Under passive mode the server will open the data port on its side and send information to the client as to which random port this will be. Under this mode a range of ports that the FTP server can be used should be opened at the firewall in order to facilitate this random data port. Passive mode is suggested for FTP servers which use a port other than port 21.

===== Links =====

===== Help =====
==== Links ====
  * [[:content:en_us:7_ug_ftp|ClearOS 7 FTP Server app]]
  * [[:content:en_us:6_ftp|ClearOS 6 FTP Server app]]

{{keywords>clearos, clearos content, app-ftp, troubleshooting, maintainer_dloper}}

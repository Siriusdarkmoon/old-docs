===== Protocol Filter =====
The **Protocol Filter** feature is used to block unwanted traffic from your network.  The feature is commonly used to make sure employees, students or end users are using their Internet access for its intended productive use.  The filter can block dozens of different protocols, including: 

  * peer-to-peer traffic
  * VoIP and Skype
  * streaming audio and video

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:6_Marketplace|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:
 

<navigation>Gateway|Protocol Filter|Protocol Filter Configuration</navigation>

===== Configuration =====
==== Protocols ====
By default, the protocol filter displays the full list of protocols and the status of each one.  Since there are well over 100 protocols defined, you can filter the display by type:

  * Document Retrieval
  * File Types
  * Instant Messaging/Chat
  * Mail
  * Monitoring
  * Networking
  * Peer-to-Peer
  * Printing
  * Remote Access
  * Security Protocols
  * Streaming Audio
  * Streaming Video
  * Time Synchronization
  * Version Control
  * Video Games
  * VoIP
  * Worm/Virus

At any point, you can click on <button>Block</button> or <button>Unblock</button> to set your **Protocol Filter** policy for your network.  When blocked, all access to and from your network will be blocked by the ClearOS gateway. 

{{:omedia:protocol_filer_overview.png}}

==== Bypass ====
In some circumstances, you may want to allow specific systems to bypass the protocol filter system.  For example, you may want to block voice-over-IP (VoIP) traffic for your entire network, except for the dedicated VoIP/PBX server.  In these circumstances, you can add IP addresses to the protocol filter bypass configuration:

  * Click on the **Bypass** 
  * Specify a **Nickname** (for example, voip_server) and the **IP address**
  * Click on <button>Add</button>

===== Performance =====
<note warning>The protocol filter can slow down network performance!</note>

If your connection to the Internet is faster than 5 Mbps, we do not recommend using the protocol filter at this time.  You should also be aware that not all protocol filter rules are created equal.  Some rules have little effect on performance while others can have a significant impact.

===== References =====
  * [[http://l7-filter.sourceforge.net|L7 Filter]]
  * [[https://www.netify.ai/features/protocol-detection|Netify Protocol Detection Engine]]

{{keywords>clearos, clearos content, AppName, app_name, clearos6, userguide, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

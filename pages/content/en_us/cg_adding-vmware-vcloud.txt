===== VMware vCloud =====
To add VMware vCloud, you will need to provide the username, password, name of your organization and the hostname of the vCloud host.

Log in to ClearGLASS and click on the " Add cloud" button. Select "VMware vCloud" from the providers' list.

[image]

Enter the same username and password you use to login to your vCloud Director, the hostname, and your organization. 

Click on the add button. If the credentials are correct, vCloud will be added to ClearGLASS and you will be able to see your VMs in the Machines section.

[image]

==== Related Articles ====
  * [[content:en_us:cg_setting-up-clearglass|Quick Start Guide]]
  * [[content:en_us:cg_monitoring-on-linux|Setting up monitoring on Linux]]
  * [[content:en_us:cg_manage-infrastructure|Managing Infrastructure]]
  * [[content:en_us:cg_monitoring-on-windows|Setting up monitoring on Windows]]
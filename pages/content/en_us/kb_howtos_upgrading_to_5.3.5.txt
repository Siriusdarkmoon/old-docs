===== Upgrading to PHP 5.3.5 =====

<note warning>5.3.5 is not fully backwards compatible with 5.2.x so ensure that you don't need any options from 5.2.x before upgrade. 
</note>

Instructions below to upgrade PHP to 5.3.5

Please note these are not the official packages, and have been put together by rpms.famillecollet.com/ for RHEL5 distributions (thank you!). I have rebuilt these so they install for ClearOS with the addition of two RPM's (libedit and sqlite2).

Thefore please consider these as beta / testing quality.
WARNING: 5.3.x is not entirely backwards compatible with older versions of PHP. Therefore if your application / website / scripts do not specify compatibility then it may break!

If you are a website developer then you may want to review the changes between 5.2 and 5.3 here
[[http://uk2.php.net/migration53]]


<note important>
In order to apply this upgrade, you will need to add Tim's Repo to your system. 

[[http://www.clearfoundation.com/docs/howtos/adding_tim_s_repo]]

</note>

==== Options ====

You can also install many other PHP features through YUM.

YUM install php-* 

YUM list php-* for all the available app you can add. 

==== Download & Install  ====

=== GET THE FILES ===

I will assume you already have installed the repo (As above)

== 2. INSTALL ==

<code>
yum --enablerepo=timb-testing upgrade php
service httpd restart
php -v
</code>

== 3. PHP VERSION INFO ==

<code>
Create a file called phpinfo.php in /var/www/html that looks like the following

<HTML>
<BODY>
<?php
phpinfo();
?>
</BODY>
</HTML>
</code>

<code>
Point your browser at example.org/phpinfo.php
</code>

Enjoy! 

==== Removal =====

<code>

Yum remove php-(Enter module to remove)

</code>


==== Forum Page =====

You can find that here; 

[[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,40/func,view/id,11888/]]

Post issues there! 

{{keywords>clearos, clearos content, kb, howtos, php, maintainer_dloper, maintainerreview_timb}}

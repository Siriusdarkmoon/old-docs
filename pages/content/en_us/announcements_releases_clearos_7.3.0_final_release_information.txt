===== ClearOS 7.3.0 Final Release Information =====
**Released: 3 March 2017**

ClearFoundation and ClearCenter are proud to announce the full release of ClearOS 7.3. This is a minor release of ClearOS and focuses on important security updates as well as increased compatibility with computers running UEFI.

==== New Features Included in 7.3.0 Release and Since 7.2.0 ====

  * Reverse Proxy App
  * Domoticz
  * Application Filter
  * Protocol Filter
  * APC™ Battery Backup Manager 
  * Full support for UEFI booting

==== New Upstream Features and improvements Include ====

  * Improvements to [[https://www.open-scap.org/|OpenSCAP]] framework (for developers).
  * [[https://en.wikipedia.org/wiki/IEEE_802.1AE|MACsec]] is now available for development.
  * Flushing capabilities in the auditd daemon
  * IdM library improvements for developers
  * HMM Technology Preview
  * Lightweight kernel support improvements to Open vSwitch (for developers)
  * Developer Support for Coherent Accelerator Process Interface flash block adapter
  * Technology preview for Kernel SCHED_DEADLINE
  * Support for NVDIMMs through libnvdimm
  * CephFS kernel module
  * pNFS SCSI file sharing
  * IoT support improved to support Bluetooth Low Energy (LE) devices.
  * IoT Controller Area Network device drivers now supported in kernel
  * Kernet support for embedded MMC interface v5

ClearOS 7 features a Community, Home, and Business version. All versions of ClearOS will install from the same install image. You will be required to select your version during the initial setup wizard on the first boot after ClearOS is installed.

==== Change Log ====
The full change logs can be found here:

  * [[https://tracker.clearos.com/changelog_page.php?version_id=171|Changelog - Changes since ClearOS 7.2.0 ]]
  * [[https://tracker.clearos.com/changelog_page.php?version_id=331|Changelog - ClearOS 7.3.0 ]]


===== Roadmap =====

  * Samba 4 Directory (Still, very much in beta)
  * Localization
  * Usability and Ease of Use improvements

=== Roadmap Tracker and Open Bugs ===
  * [[https://tracker.clearos.com/roadmap_page.php|Known bugs]]

===== Feedback =====
Please post your feedback in the [[https://www.clearos.com/clearfoundation/social/community/clearos-7-3-fully-released|ClearOS 7.3 Fully Released Discussion Forum]]. 


===== Download =====
==== ISO Images ====
^Download^Size^SHA-256^
|[[http://mirror.clearos.com/clearos/7/iso/x86_64/ClearOS-DVD-x86_64-7.3.0.180516.iso|64-bit Full]]|844 MB|  0ba358dddf233f1631db8bf48911c1a7838887a1c854980c5a1d17c1a59d5812 |
|[[http://mirror.clearos.com/clearos/7/iso/x86_64/ClearOS-netinst-x86_64-7.3.0.180516.iso   |64-bit Net Install]]|417 MB|  4e5e9ff1807eed54d4d03a9b4a9e9700b4743ad835df650f012a972efe9607d5 |


==== Virtual Machine and Cloud Images ====
Virtual Machine and Cloud Images are not yet available for this release.

^ Platform ^ Image ^ Proceedure ^
| Amazon EC2 | clearos-7.2.0 - ami-8fb03898 | Install and run updates to 7.3 |

==== Upgrade from Earlier ClearOS Community 6.x Releases ====
There is no supported method yet from upgrading from ClearOS 6 to ClearOS 7. 

Make a copy of your configuration data. Backup your user data as well or retire your ClearOS 6 disk. ClearOS 7 now supports the import of your configuration data but not your user data.

==== Upgrade from Earlier ClearOS Community 7.x Releases ====
Upgrades from 7.1.0 on properly running systems will happen automatically. To manually upgrade from ClearOS 7.1.0 or to validate that you are at the latest version run:

  yum update

{{keywords>clearos, clearos content, announcements, releases, clearos7.3, final, clearos7.3.0, maintainer_dloper}}

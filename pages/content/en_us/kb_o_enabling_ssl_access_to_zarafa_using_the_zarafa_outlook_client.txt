===== Enabling SSL Access to Zarafa Using the Zarafa Outlook Client =====
This guide is intended as a development guide and reference for enabling the SSL portions of the Zarafa Outlook Client on ClearOS.

===== Certificates =====
Most of the certificate material that currently exists under ClearOS 6.x is as it should be except that Zarafa server will want a 'pem' file for server certificate which is merely a combination of the key and the cert. Perform the following:

<code>
cat /etc/pki/tls/private/localhost.key /etc/pki/tls/certs/localhost.crt > /etc/pki/tls/certs/localhost.pem
chmod 600 /etc/pki/tls/certs/localhost.pem
</code>

Zarafa will also want a directory which contains client public keys if you are using an ultra-secure method for authentication. We won't address that here but the directory needs to exist nevertheless:

  mkdir /etc/zarafa/sslkeys

A known bug exists that crashes Zarafa is this directory has anything in it except certificates. It is untested whether that bug affects this version of Zarafa on ClearOS. Best to just leave it blank.

===== Configuration File =====
Backup and modify the /etc/zarafa/server.cfg file. The parameters in question are:

  server_ssl_enabled     = no
  server_ssl_key_file    = /etc/zarafa/ssl/server.pem
  server_ssl_key_pass    = replace-with-server-cert-password
  server_ssl_ca_file     = /etc/zarafa/ssl/cacert.pem
  server_ssl_ca_path     =
  sslkeys_path           = /etc/zarafa/sslkeys

We will change these to:

  server_ssl_enabled      = yes
  server_ssl_key_file     = /etc/pki/tls/certs/localhost.pem
  server_ssl_key_pass     =
  server_ssl_ca_file      = /etc/pki/tls/certs/ca-bundle.crt
  server_ssl_ca_path      =
  sslkeys_path            = /etc/zarafa/sslkeys

Effectively, once the certs exist and the path exists, the only parameters which need to change are:

  * server_ssl_enabled
  * server_ssl_key_file
  * server_ssl_key_pass
  * server_ssl_ca_file

===== Developer =====
The field in the Zarafa Server module which activates this change should be called something like a checkbox which may say "Enable Zarafa Client SSL access" 

When this is done, the server should:

  * check the path '/etc/zarafa/sslkeys' and create the directory if it does not exist.
  * check the existence of '/etc/pki/tls/certs/localhost.pem' and create the file by concatenating the existing key material if the '/etc/pki/tls/certs/localhost.pem' does not exist.
  * replace the value of 'server_ssl_key_pass' in the server.cfg file if the value equals 'replace-with-server-cert-password'.
  * replace the value of 'server_ssl_ca_file' in the server.cfg file if the value equals '/etc/zarafa/ssl/cacert.pem' to equal '/etc/pki/tls/certs/ca-bundle.crt'
  * Turn the value of 'server_ssl_enabled' from 'no' to 'yes' if it passes all other flight tests.

With the SSL enabled, the regular port 236 still works. Enabling SSL causes the Zarafa service to run on the default port of 237. This port should be opened on the incoming firewall rules.

Once this setting is set, a simple reload of the service will NOT work. In fact, changing this setting seem to cause chaos with a restart and will often crash. Stopping the zarafa-server service utterly and then starting again after a brief period seems to solve this issue.

{{keywords>clearos, clearos content, AppName, app_name, kb, howto, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

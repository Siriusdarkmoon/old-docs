{{ :userguides:beams.svg?80}}
<WRAP clear></WRAP>

===== Beam Manager =====
Manage various aspects of the modem/beams used to connect to the Internet.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/network/beams|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_beams|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_beams|here]].
==== Additional Notes ====

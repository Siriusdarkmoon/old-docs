===== Views =====
A ClearOS view is almost identical to a [[http://www.clearfoundation.com/docs/developer/apps/ci/current/general/views.html|CodeIgniter view]].  Some additional helpers have been added to simplify and standardize the view code.  This document highlights the differences as well as some best practices.

  * Coding styles
  * View standards
  * Field helper

===== 3 View Styles =====
There are three main styles of forms/controllers found in ClearOS.

  * Form View
  * CRUD View
  * Advanced View

==== Form View ====

  * [[:content:en_us:dev_framework_reference_guide_form_view|Form View]]


==== CRUD View ====

  * [[:content:en_us:dev_framework_reference_guide_crud_view|CRUD View]] 

==== Advanced View ====

  * [[:content:en_us:dev_framework_reference_guide_views|Advanced View]] 

{{keywords>clearos, clearos content, dev, framework, mvc, views, maintainer_dloper}}

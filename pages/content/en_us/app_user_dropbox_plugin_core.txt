{{ :userguides:user_dropbox_plugin_core.svg?80}}
<WRAP clear></WRAP>

===== User Dropbox Plugin =====
Dropbox is a cloud-based file storage and synchronization service. Use this app to synchronize files to a folder located in your home directory which can then be accessed by any device associated to the same Dropbox account (laptop, smartphone, tablet etc.).

This app is required for the master node only in a master/slave environment.  The app allows an administrator to define Dropbox access to a user's home directory.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/system/user_dropbox_plugin_core|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_user_dropbox_plugin_core|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_user_dropbox_plugin_core|here]].
==== Additional Notes ====

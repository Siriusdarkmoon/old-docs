===== Migrating Mail to Zarafa Community and Zarafa Professional =====
Whether you were using ClearOS 5.x with Cyrus or some other email client, you can import mail into Zarafa using several methods. These methods include IMAP (Zarafa Community and Zarafa Professional) and MIME (Zarafa Professional).

===== Client-side IMAP migration vs. Server-side migration =====
IMAP is a great tool for moving mail because it allows multiple connections to different servers. Your strategy for migration will enjoy its best success if your new Zarafa server can run parallel to your other server (ie. you are not upgrading the existing server hardware). With this method you can see both servers and in some situations, put the responsibility of moving the data in the hands of competent users or technicians. This is advantageous because the mailbox moves can be done in parallel and ad hoc.

Other tools exist to move the entire mailbox structure. These tools tend to do so serially (one at a time) and can cause a disruption if the mail size is big. Additionally, the system will effectively be 'down' during the migration, meaning that the users won't have access to their old mail while the process is running. On large environments, this can take days.

===== Client-side MIME migration =====
In many cases, migration of email from a MIME source (like MS Exchange) to another MIME destination (like Zarafa) is similar to the IMAP migration. The difference here is that you can wrap the entire mailbox into a PST on the local computer and then import into Zarafa. Effectively, you can use the entire computation power of all the workstations to aid the server in the export and the import.

There are tools that exist to import a batch of PSTs but they often do so one at a time. This may be acceptable in situations where the total mailbox store is not that large or if the migration window is particularly long (ie. several days)

==== A special consideration and note about MIME and MS Outlook =====
Two things to consider when migrating users of MIME and MS Outlook:

  * MIME return addresses won't work
  * Categories are a function of Outlook, not MIME

One thing that your users will notice after a migration is that they cannot email internal users using the reply address. This is because Outlook will use the MIME path (ie. /O=EXAMPLE/OU=EXCHANGE ADMINISTRATIVE GROUP (ABCDEFGH23JKLMNO)/CN=RECIPIENTS/CN=Joe) instead of the Internet email address (ie. joe@example.com). Consequently, the return path for the internal email address is likely to change out of your control in the new system and the email cannot find its way back to the user using the old MIME address. It is best to set the expectation with the client or user that they will get bounced messages when replying to **OLD** internal email if they are coming from MIME.

Second, categories will NOT migrate in a PST no matter how hard you try. To get these categories migrated, you will need to follow [[http://www.clearcenter.com/support/howtos/backing_up_and_restoring_outlook_categories|this guide.]] In your interviews with the client or user, you will need to ask if they use categories and export them before the migration. If they look at you funny when you say 'Categories' then they likely don't know and don't use them. However, if they use categories, they WILL miss them when they are gone.

===== Technical Execution =====
Once you have decided on the technical execution model you will use or if you are still unclear as to what needs to happen, then please visit [[http://www.clearcenter.com/support/howtos/migrating_to_zarafa_on_clearos|this document]].

{{keywords>clearos, clearos content, kb, howtos, maintainer_dloper, maintainerreview_x, keywordfix}}
===== Using the Untangle Directory Connector with ClearOS =====
This guide provides some tips on how to use the ClearOS RADIUS/LDAP system with Untangle.  

===== Configuration =====
==== ClearOS RADIUS Configuration ====
Before you can connect Untangle to [[Documentation:User Guide:RADIUS Server|ClearOS RADIUS/LDAP]], you need to create a client configuration entry.  In other words, you need to grant access to RADIUS for the Untangle system.  Please refer to the [[Documentation:User Guide:RADIUS Server|RADIUS User Guide]] about installation and more information.

To configure the RADIUS client configuration in ClearOS:

  * Go to <navigation>Network|Infrastructure|RADIUS Server</navigation> in the menu
  * Click on <button>Add</button> in the **Clients** summary
  * Specify:
    * The IP address of the Untangle system
    * A nickname of your choosing (e.g. Untangle)
    * A password for the Untangle access

That's all there is to it.

==== Untangle Directory Connector Configuration ====
To configure the Untangle Directory Connector:

  * Click on <button>Settings</button> on the **Directory Connector** rack
  * Click on the **RADIUS Connector** tab
  * Specify:
    * The IP address of the ClearOS system
    * The port number: 1812 (default)
    * The password specified above
    * The authentication method: PAP

Go ahead and click on <button>RADIUS Test</button> to make sure the connection succeeds.  If all goes well, specify a ClearOS username/password in the Untangle configuration and re-test.  If all goes well, save the settings to complete the configuration.

{{keywords>clearos, clearos content, KB Old ClearOS Guides Usinf the Untangle Directory Connector with ClearOS, app_name, xcategory, maintainer_x, maintainerreview_x, keywordfix, userguides}}

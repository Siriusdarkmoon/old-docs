===== Microsoft Active Directory Connector =====
With the **Active Directory((Marks belong to their respective owners)) Connector**, you can use your existing Windows users, groups and passwords on your ClearOS Business system.  This allows you to manage users and groups in one location, as well as apply policies in a consistent manner. 

If you are interested in implementing a single sign-on content filter solution using ClearOS, please take a look at the [[:content:en_us:kb_bestpractices_content_filtering_with_active_directory|Content Filtering with Active Directory Implementation Guide]].  The guide is also helpful for getting an understanding of how to implement the Active Directory Connector with ClearOS apps, so even if content filtering is not your cup of tea, the guide might still be worth a look.

With the Active Directory Connector, all user accounts and groups are subordinated to the AD system. Because ClearOS uses groups for access to services this means that there are special groups that you must make on your Active Directory Domain Controller. These special groups are called plugin groups.

You must make, on your AD server, the corresponding groups and assign the users you want access to ClearOS services to those groups. For example, create a group on AD called 'web_proxy_plugin' and then assign all the users on your domain that you'd like to have access to the proxy server.

Once these groups replicate to ClearOS, you will be able to use the domain username and passwords against the ClearOS server for services with plugin groups.

<note warning>The Active directory Connector does not work directly with some apps such as Kopano and Radius. Radius can be tweaked from the command line to work with the AD Connector. To use Kopano, you will require help directly from Kopano's support and is neither tested or supported by ClearCenter.</note>

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Directory|Active Directory Connector</navigation>

===== Initializing =====
Before configuring the Active Directory Connector, please assign your ClearOS' DNS settings to resolve DNS from the Active Directory Server(s) within your Active Directory domain. It is important that ClearOS use the DNS of the domain to properly resolve the hostname(s) of the domain for authentication purposes. Secondly, it is a good idea to have ClearOS use the clock of the server instead of the clocks on the internet. Even if you have the clocks of the AD server set to the same internet clocks, it is possible for it to mess up and it is better to have the clocks between the AD server and ClearOS to be the same and wrong together than for one to be wrong and the other to be right.

  * Clocks on the Active Directory server and ClearOS need to be synchronized. If the clock skew is too large, the connection will fail.

  * DNS needs to be properly configured or problems will likely arise. 

{{ :omedia:ad-sync.png|ClearOS Active Directory Connector}}When you visit the Active Directory Connector web-based configuration page for the first time, you will be shown a number of options needed to connect your ClearOS Professional system to your directory server.  There are a lot of little things that can trip up this process, so please read review this guide if you run into trouble.



==== Connector Settings ====
The following information is required to allow ClearOS to join the Active Directory domain.

=== Windows Domain ===
The **Windows Domain** is the one-word domain configured in Active Directory.

=== ADS Realm ===
The **ADS Realm** is the //full// DNS name for your domain.  You can find this parameter by reviewing the //My Computer / Properties// information on a system already joined to the Windows domain.  If you are a command prompt kind of person, you can also find this information on the Windows command line:

  echo %userdnsdomain%

If you are using an internal DNS name (e.g. myrealm.local), then please make sure your ClearOS system is configured to resolve this hostname.  You can either add this name to the [[:content:en_us:7_ug_dns|ClearOS DNS Server]] or make sure ClearOS is pointing to an internal DNS server on the [[:content:en_us:7_ug_network|IP Settings]] configuration.

Please see [[:content:en_us:kb_o_splitting_dns_cache_to_use_specific_dns_server_for_domains|Splitting DNS Caches to use Specific DNS Server for Domains]] for setting up the DNS server.

The DNS Server also needs an entry for "Server Name"."ADS REALM" and "Server Name" pointing to your ClearOS LAN IP.

=== Domain Controller ===
The **Domain Controller** is a DNS hostname for one of the domain controllers in your domain, preferably your Global Catalog server. You should use the hostname of the server and **NOT** the IP address. It is useful to ping the hostname from a command prompt to ensure that you have the proper DNS resolution before joining the domain.

==== Server Settings ====
The server settings are.  The **Server Name** is the one word name for the ClearOS system (e.g. gateway) while the **Server Comment** is a description of the system (e.g. ClearOS Internet Gateway).

==== Administrator Account ====
Any account that is permitted to join systems to the Windows Domain can be specified for the **Domain Administrator** and **Password** settings.

===== Reviewing User and Groups =====
Once you have connected ClearOS Professional to your Active Directory system, you will be able to review users and groups in the ClearOS web-based administration tool.  It can take up to a minute or two for the first directory synchronization to occur, but subsequent connections are much quicker. 

  * To review users go <navigation>System|Accounts|Users</navigation>
  * To review groups go <navigation>System|Accounts|Groups</navigation>

===== ClearOS App Policies =====
Any application in ClearOS that requires user authentication needs to have a corresponding group in Active Directory. Create those groups now on Active Directory and start assigning users in Active Directory to these groups.  This allows you to control which users have access to the apps on the ClearOS system.  For example, any user in the **web_proxy_plugin** group in Active Directory will be able to access the [[:content:en_us:7_ug_web_proxy|Web Proxy]] on ClearOS.  

When you visit an app page that requires user authentication, you will see an **App Policy** widget as shown in the screenshot below:

{{ :omedia:web_proxy_policy.png |ClearOS Web Proxy Policies}}

You can view members of this app policy by clicking on the <button>View Members</button>.  To change the group membership, please do so in your Active Directory system.  Here is a list of some of the apps that use user and and group information from Active Directory:

^App^Group Name^
|[[:content:en_us:7_ug_ftp|FTP Server]]|ftp_plugin|
|[[:content:en_us:7_ug_imap|IMAP and POP Server]]|imap_plugin|
| |openfire_plugin|
|[[:content:en_us:7_ug_openvpn|OpenVPN]]|openvpn_plugin|
|[[:content:en_us:7_ug_pptpd|PPTP Server]]|pptpd_plugin|
|[[:content:en_us:7_ug_print_server|Print Server Administrator]]|print_server_plugin|
|[[:content:en_us:7_ug_smtp|SMTP Server]]|smtp_plugin|
|[[:content:en_us:7_ug_user_certificates|User Certificates]]|user_certificates_plugin|
|[[:content:en_us:7_ug_web_proxy|Web Proxy]]|web_proxy_plugin|

===== Connecting to AD Off Network =====
Connecting to Active Directory when not on the same subnet or logical network can prove difficult. Some of what the process does includes broadcast type functions when certain services or ports are unavailable. 

It is important that the connection between your ClearOS server and your AD server is reliable and persistent. The Active Directory Connector has [[http://www.samba.org/samba/docs/man/Samba-HOWTO-Collection/winbind.html#id2654580|some technical]] requirements for your ClearOS to join the domain most of which are irrelevant if they exist on the same network but cause issues if it needs to traverse firewalls for certain ports.

Among the several requirements are:

"Before attempting to join a machine to the domain, verify that ... the target domain controller ... is capable of being reached via ports 137/udp, 135/tcp, 139/tcp, and 445/tcp."

Additional ports might include any AD services listed [[http://technet.microsoft.com/en-us/library/dd772723%28v=ws.10%29.aspx|here.]]

Additionally, ClearOS should ONLY use the Active Directory server as its DNS server. This means that when connectivity is down between ClearOS and its AD controller, it will not be able to function well as a DNS server itself.


===== Help =====
==== Links ====
  * [[:content:en_us:kb_troubleshooting_the_ad_connector|Active Directory Connector Troubleshooting]]
  * [[:content:en_us:kb_bestpractices_content_filtering_with_active_directory|Content Filtering with Active Directory Implementation Guide]]
  * [[http://technet.microsoft.com/en-us/library/dd772723%28v=ws.10%29.aspx|AD ports]]

==== Navigation ====

{{keywords>clearos, clearos content, Microsoft Active Directory Connector, app-active-directory, clearos7, userguide, categoryserver, subcategorydirectory, maintainer_bchambers}}

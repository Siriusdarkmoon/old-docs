===== Group Conventions in ClearOS =====
As a platform, ClearOS has to comply with certain best practices to increase the compatibility and success of interoperating with other platforms, applications, software, and cloud services. To accomplish this, ClearOS conforms with group-based administration practices, also known as [[https://en.wikipedia.org/wiki/Role-based_access_control|role-based access control]] or RBAC. Understanding this structure and how it applies to your environment will increase the security and compliance to best practices and even regulations that may be in effect for your industry or business uses.

Basically, in order to access a given resource, operation, or task, a user must be assigned to the group that has authorization to that resource, operation, or task. Users in this model are only assigned to groups and not assigned to resources through access controls.

This presents some challenges to the default POSIX infrastructure used by ClearOS but never fear, we designed it from the bottom up with these needs in mind!

===== Standards =====
[[https://en.wikipedia.org/wiki/POSIX|POSIX]] is a standard that comes into play here for ClearOS since ClearOS is built on Linux which comports to POSIX standards. Within POSIX, group membership is fully supported and a user can be assigned to any number of groups. This is accomplished through the Group IDentifier (or GID) for any particular object in the POSIX tree. Originally this functionality was limited to a single assignment per resource but extensions to the POSIX standards allow for additional membership GID object through POSIX Access Control Lists (ACL) support. You will need to ensure that your file system supports Filesystem ACL (FACL) support if you wish to use this extended standard in addition to the default single group assignment paradigm supported in ClearOS. While FACL support is fully compatible with ClearOS, it is not universally supported in the administration interface since the default POSIX standard is more compatible and generally sufficient, especially when using the RBAC model. In terms of ClearOS, POSIX plays heavily on the core of the system.

LANMAN or LAN Manager is a standard used in the Windows and Samba world to provide file system access, organizational, and security features and services. LANMAN conducts group membership as a set of keys that a user will retain during their session. Group changes, for example, are not felt by the user until the user conducts a login even in which their new key sets are distributed.

[[https://en.wikipedia.org/wiki/Kerberos_(protocol)|Kerberos]] is a standard used by Active Directory to ensure that tokenization of authentication occurs with additional controls based on tickets that must be re-applied for access to persist. Kerberos provides for a 'mutual authentication' model and is featured, for example, in the Samba Directory app and the Active Directory Connector.

===== Compliance =====
ClearOS strives to be universally compatible with best practices that support compliance needs. Since RBAC plays a central role in most compliance standards, it also plays a central role in ClearOS. By default, consider each user-facing system in ClearOS as a distinct role for which a group must be assigned. So for OpenVPN access, a group will exist separate from the PPTP group. For example, you should not have a situation where these two different, yet similar, role has the same access control. Nor should you ever have a system that has a user directly assigned to it. While these systems both support individual user-based access, ClearOS will not make use of those facilities since they don't comport themselves with best practices or compliance requirements. As such, ClearOS has built-in groups for all user-facing service in which anonymous access is not allowed. For example, some of these are the default groups for apps that are created for the use of specific systems:

^App^Group Name^
|[[:content:en_us:7_ug_ftp|FTP Server]]|ftp_plugin|
|[[:content:en_us:7_ug_imap_and_pop_server|IMAP and POP Server]]|imap_plugin|
|[[:content:en_us:7_ug_openvpn|OpenVPN]]|openvpn_plugin|
|[[:content:en_us:7_ug_pptp_server|PPTP Server]]|pptpd_plugin|
|[[:content:en_us:7_ug_advanced_print_server|Print Server Administrator]]|print_server_plugin|
|[[:content:en_us:7_ug_smtp_server|SMTP Server]]|smtp_plugin|
|[[:content:en_us:7_ug_user_certificates|User Certificates]]|user_certificates_plugin|
|[[:content:en_us:7_ug_web_proxy|Web Proxy]]|web_proxy_plugin|

These groups must exist and have users assigned to them for a user to access them. For built-in 'plugin' groups like this, the work has been done for you on the group creation and these are represented in the user interface as checkboxes instead of groups.

===== Interoperability =====
In the above compliance section a list of groups was listed. These, for example, are needed to be manually added to Active Directory when you use the Active Directory Connector app.

ClearOS furthers interoperability with systems by adopting their standards as well-formed structure withing ClearOS. For example, Windows default groups are included and special care has been taken to match these group with their windows conterparts. These include:

  * Account Operators
  * Administrators
  * Backup Operators
  * Guests
  * Power Users
  * Print Operators
  * Server Operators
  * Users

==== allusers ====
The 'allusers' group deserves special mention. This is the equivalent of the 'Domain Users' group in Active Directory and serves a similar purpose. In POSIX, the concept of a 'default' group exists but in Windows it does not other than the aforementioned 'Domain Users' group. When ClearOS is the directory it is important that we make a similar group to provide compatibility but we cannot use that same name. Since we can control the behaviors and effects of the open source side of things and because our original engineering tapped into the vast experience of some principle Samba Team members, we decided to take their counsel and design the default group to behave in this manner. 

Knowing that POSIX requires a default group and that such a construct can and will tag the group structure (GID) with this group permission upon creation of files in most POSIX compliant programs, the decision was made to make a single default group which in effect poisons permission in certain structures on the system. This would ensure some important things, share level access could be defined to the non-default groups available to both POSIX and Windows, and file level permissions would not be borked by an interoperation between users laying down the 'default group' as an access object. 

For example, let us say that you have a system which must comply with the compliance directives of HIPAA or Sarbanes-Oxley which both require group-based permission structures. These users are:

  * jdoe
  * tsmith

If we decide not to use the structure of ClearOS and instead we assign the user jdoe to have a default posix group of group1 and tsmith to have a default posix group of group2. We make both accounts also a member of the 'accounting' group. Now, you create a data folder structure called /var/flexshare/shares/accounts and you set the folder to permissions flexshare:accounting with group based access similar to -rwxrwx---. Now you create a Samba share and set the access permission also to 'accounting'. So far, so good. By default both of these users will be able to read and write to the directory for the fact that they both belong to the accounting group. But when jdoe creates a new file, its file-level permissions will be jdoe:group1. So even though tsmith has access to this folder, he will not have access to the new file created. 

Samba actually has a way around this issue by doing a force group on the share. There are other POSIX ways around this also such as setting GID bits. Well, that is all well and good for Samba but that doesn't help us if jdoe logs in with FTP to that same share. Or if jdoe logs in with SSH and navigates to that directory to create a file it will tag it with his default group, 'group1'. Moreover, the temptation to force group, force user, or force mask in Samba is an expensive (meaning hardware intensive) operation. We were specifically told by key Samba members to avoid using it at all because it does NOT scale well.

So, if both users are a member of 'allusers' then permissions set by POSIX programs will never lock someone who is authorized at a share level from accessing data that they should access. This is a compensation for that program or utility which may have access control but not file system control.  

So how does this help you? Well, assign the users to a group that they both share in common and never use 'allusers' for access control through the service. The extended group support is fully supported by Samba, LDAP, POSIX, and others and should be the mechanism that you use because it alone provide adherence to best practices and mutually agreed standards. The 'allusers' group should be used to deprovision access as it is done in the /home directories:

<code>
[root@home ~]# ls -la /home
total 4
drwxr-xr-x   1 root     root       16 May 15 19:43 .
dr-xr-xr-x. 19 root     root     4096 May 16 15:35 ..
drwx------   1 kimberly allusers   64 May 15 19:43 kimberly
</code>

As you can see, allusers is used here in home directories to prohibit access except at the user-level. Home directories are an example of user instead of role based access that is allowed under compliance measures. As you can see with this permission structure, the user has exclusive access to this folder in spite of any subsequent permission structures below it. 

Lastly, this issue of the default group has been known for some time in the POSIX world and a common way in which Redhat and others uses to combat this requirement of POSIX is to make the default group the same as the user. This way exclusive files look like this:

  -rwxrwx--- jdoe:jdoe filename

This works but is insufficient when it comes to interoperability with other systems and with some compliance systems because making a group and a user have the same name breaks certain things at both a technical and policy level. The better mechanism is as we have done it:

-rwx------ jdoe:allusers filename

You can use tools to change this behavior such as manually overriding LDAP with 'ldapvi' or other toolds but that breaks the support around the ClearOS model for several reasons;

  * it is known to break apps 
  * it is known to break interoperability
  * it is known to break compliance standards such as HIPAA and SOx
  * it is known to violate best practices of Samba and other open standards methods.


{{keywords>clearos, clearos content, app-groups, clearos6, clearos7, categorysettings, subcategoryaccounts, maintainer_dloper}}
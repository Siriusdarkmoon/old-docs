===== ClearOS Professional 6.6.0 Final Release Notes =====
**Released: February 19, 2015**

ClearOS Professional 6.6.0 Final has arrived!  Along with the usual round of bug fixes and enhancements, this release introduces WPAD, QoS, YouTube School ID support, an upgrade to the Intrusion Detection engine, and ISO-to-USB key support.  With the upcoming ClearOS 7 release, some of the features in the beta have been migrated to the ClearOS 7 roadmap, notably WordPress, Joomla!, Tiki Wiki CMS Groupware, Vtiger and SugarCRM.

===== What's New in ClearOS Professional 6.6.0 =====
The following is a list of new features in ClearOS Professional 6.6.0.

  * QoS
  * YouTube School ID support
  * WPAD (paid app)
  * Intrusion Detection engine upgrade
  * [[http://www.clearcenter.com/support/documentation/clearos_guides/clearos_usb_installer|ISO-to-USB key support]]

Since the last release, the following apps have been released:

  * ownCloud for Home
  * ownCloud for Business

In addition, Tiki Wiki CMS Groupware, WordPress, and Joomla! are available for experimentation.  These apps may be supported in a future release and are available as beta only.

The full changelog can be found here:

  * [[http://tracker.clearfoundation.com/changelog_page.php?version_id=49|Changelog - ClearOS 6.6.0 Beta 1]]
  * [[http://tracker.clearfoundation.com/changelog_page.php?version_id=53|Changelog - ClearOS 6.6.0 Beta 2]]
  * [[http://tracker.clearfoundation.com/changelog_page.php?version_id=52|Changelog - ClearOS 6.6.0 Final]]

===== Download =====
See the [[http://www.clearfoundation.com/Software/downloads.html|Downloads]] page.

==== Upgrade from Earlier ClearOS Professional 6.x Releases ====
To upgrade an existing system to ClearOS 6.6.0, you can run the following command:

  yum upgrade


{{keywords>clearos, clearos content, announcements, releases, clearos6.6, final, currentfinal, maintainer_dloper}}
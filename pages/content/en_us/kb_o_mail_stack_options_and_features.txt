===== Mail Stack Options and Features =====
E-mail is an undisputed essential service of the Internet.  If you or the organization you're working for is deploying ClearOS with the intention of providing e-mail services, this document is for you.

ClearOS is all about choice - choice around where you deploy services (in the cloud, private cloud or on-premise), and choice in terms of what tools you use to achieve a desired objective.  No where is that choice more pervasive than in selecting mail services under ClearOS.  The goal of this document is to:
  * list options available for running SMTP under ClearOS
  * what apps are free and what are paid apps that are optional
  * what are best practices for implementing each solution

===== On-Premise/Private Cloud vs. Cloud =====
At the highest elevation of decision making, deciding where to run your mail services trumps all.  While there are practically an unlimited number of vendors offering hosted mail solutions based on dozen's of platforms, if you're reading this document, we're assuming you're interested in taking advantage of apps and services available via the ClearOS Marketplace that can help you achieve your goals.

For purposes of clarity, an on-premise or private-cloud implementation of a mail stack is classified as apps and services running directly on ClearOS.  Linux, known for its security and stability, powers much of the Internet's back-end infrastructure, including mail services.  As such, ClearOS is an inherently 'good choice' as a platform for hosting your mail server and accounts.

//Where// you choose to physically deploy ClearOS is the origin for the differentiation "on-premise" or "private cloud".

On-premise represents those installations that are physically located within a company's point of presence - a store, office, school etc.  Many small businesses choose to run the mail stack on-premise, taking advantage of cost savings and accepting or mitigating services like redundent Internet connection, physical security measures, power backup etc. that a hosted solution provides.
 
Private cloud refers to ClearOS implementations that are physically located in the cloud - typically cloud-services such as Amazon's EC2 or Google's Cloud Compute or a datacenter dedicated to providing Internet connectivity and space (either virtual or physical) for running a server.  Larger companies may have co-located within at a datacenter.  Smaller companies can lease space or virtualized servers for running dedicated mail stack based on ClearOS in the cloud.  The term private-cloud is typically used in this case since ownership of the infrastructure is completely private - no advertising 'bots' or rogue employees have access to your mail.

==== On-Premise/Private Cloud ====
At the current time, there are four integrated options available for an on-premise or private cloud deployment of mail services:
*  OpenSource Mail Stack
*  Zarafa Community Edition for ClearOS
*  Zarafa Small Business Edition for ClearOS

=== OpenSource Mail Stack ===
For over 10 years, ClearOS has provided tremendous value by integrating the best-of-class software for providing a mail stack that is stable, secure and simple to configure.  This history lays the foundation for the open-source mail stack.  As its name implies, the solution is comprised of various software written by a diverse group of individuals and organizations under similar OSS licenses.

For those interested, the following table provides details on the software/project that provides each service or feature in the OpenSource mail stack.

|< 100% 20% 30% 50% >|
^Feature/Service ^Company/Project ^Home Page^
| SMTP                   | Postfix          | [[http://www.postfix.org|www.postfix.org]] | 
| IMAP(S)                | Cyrus            | [[http://cyrusimap.org|www.cyrusimap.org]] | 
| POP3(S)                | Cyrus            | [[http://cyrusimap.org|www.cyrusimap.org]] | 
| Antimalware/Antivirus                | ClamAV           | [[http://www.clamav.net|www.clamav.net]] | 
| Antispam               | SpamAssassin     | [[http://spamassassin.apache.org|spamassassin.apache.org]] | 
| Message Handler        | Amavis-New       | [[http://www.ijs.si/software/amavisd|www.ijs.si/software/amavisd]] | 
| Greylisting            | Postgrey         | [[http://postgrey.schweikert.ch|postgrey.schweikert.ch]] | 
| Mailbox Backup         | ClearCenter      | [[http://www.clearcenter.com/marketplace/system/ClearCenter_Remote_Server_Backup.html|www.clearcenter.com/backup]] | 
| Mail Archive           | ClearCenter      | [[http://www.clearcenter.com/marketplace/server/Mail_Archive.html|Available]] |
| Webmail                | Roundcube        | [[http://www.clearcenter.com/marketplace/server/Webmail.html|Available]] |
| Mobile Sync            | N/A              | N/A |
| MX Backup              | ClearCenter DNS  | [[http://www.clearcenter.com/Services/clearsdn-internet-domain-and-dns-services-6.html|www.clearcenter.com/dns]] |

<note tip>Just because a feature is listed as Not Applicable in the table above does not mean it cannot be done.  It simply means it has not been integrated into the ClearOS framework and Marketplace so it cannot be installed with a few clicks of a mouse.  However, if you have the knowledge, there is no shortage of software (both open-source and proprietary) that is compatible with ClearOS and can be installed manually.</note>

=== Zarafa Community and Small Business ===
In early 2012, ClearCenter partnered with Zarafa ([[http://www.zarafa.com|www.zarafa.com]]), the leading open-source Exchange server drop-in replacement.  Zarafa's ZCP (Zarafa Collaboration Platform) brings many great features to the mail stack, including a stellar webmail UI experience, calendaring, notes, groupware services (shared folders, calendars, notes etc.) and archiving. 

Zarafa actually uses many of the same open-source components that drives the Open-Source mail stack described in the previous section.  In fact, the only change is the Cyrus IMAP/POP3 service being removed for Zarafa which provides its own implementation of these protocols.

The table below provides a brief summary of the differences in Zarafa versions licensed under the ClearCenter/Zarafa partner agreement.

|< 100% 40% 20% 20% 20% >|
^Feature/Service ^ Community Edition ^ Small Business ^
|Intended Use | Private Use/Home | Business/EDU/GOV |
|ClearCenter Supported | No | Yes |
|Zarafa Supported | No | Yes |
|Outlook/Exchange Support | 3 Users | Yes |
|Advanced multi-user calendar | No | Yes |
|Pricing | Free | $14/user/yr |
|Mail Archive | No | [[http://www.clearcenter.com/marketplace/server/Mail_Archive.html|$95/yr]] |

<note tip>For a complete description and feature matrix breakdown, please visit the Zarafa product pages [[http://www.zarafa.com/content/editions|here]].</note>
==== Cloud ====
While options such as Office 365, hosted MS Exchange or IBM's SmartCloud are all valid options, for the purposes of this document, we focus only on GoogleApps.  ClearOS has a Google Apps syncronization app that makes an on-premise implementation of ClearOS (for non-email services like file sharing, user directory etc.) that allows an administrator to provision GoogleApps accounts to users when adding/modifying users on the local ClearOS installation.

===== Pricing, Setup & Configuration =====
{{ :documentation:user_guide:ss_mail_stack_tags.png?450|Marketplace}}

If you're using ClearOS Community or Professional Edition, we've made it easy to quickly select apps from the Marketplace that are required for each of the mail stack options discussed in the sections above.  Each option has been 'tagged' with a unique marker, as listed in the table below.  These same tags have been applied to each Marketplace app that, when combined, provide the mail stack as described.

To lookup an option in the ClearOS Marketplace, type in the tag in the search field.  Ensure no other filters are set.  Select all recommended apps and click on the "Install/Upgrade" button to proceed to the Marketplace confirmation.

|< 100% 40% 10% 20% 20% >|
^Mail Stack Description ^ Tag ^ ClearOS Community ^ ClearOS Professional[1] |
|Open-Source, Free Solution | 01_SMTP | Free | Free |
|Open-Source, with additional AV/AS Updates | 02_SMTP | $120/yr | $0 (Included) |
|Zarafa Community Edition | 01_ZARAFA | $10 | $10 [2] |
|Zarafa Community, with additional AV/AS Updates | 02_ZARAFA | $10 + $120/yr | $10 |
|Zarafa Small Business, with additional AV/AS Updates | 03_ZARAFA | Not available | $14/user/year |
|Google Apps | 01_GOOGLE | Not available | $100/yr [3] |

[1] Pricing based on having a ClearOS Professional Basic or above subscription (not valid for ClearOS Professional LITE).

[2] Zarafa Community Edition is **not** a supported app under the terms and conditions of the ClearOS Professional Edition.  If you are interested in a ClearCenter-supported mail collaboration suite, select Zarafa Small Business.

{{keywords>clearos, clearos content, kb, howtos, maintainer_dloper, maintainerreview_x, keywordfix}}
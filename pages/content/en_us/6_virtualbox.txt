===== ClearOS with Virtualbox =====
Now that you have [[:content:en_us:6_e_downloading|downloaded]] your ClearOS VirtualBox image, you can begin the install. 

===== VirtualBox =====
  * Make sure you have the correct [[http://www.clearcenter.com/Software/clearos-professional-downloads.html|ClearOS download]] for VirtualBox.
  * Unzip the downloaded zip file.
  * In VirtualBox, create a new Virtual Machine. 
  * Choose Linux as the Operating System and Linux 2.6 as the version - 64-bit if appropriate.
  * Continue with the VirtualBox wizard
  * Choose **Use existing hard disk** and click the browse folder icon to open the Virtual Media Manager.
  * Click the <button>Add</button> icon on the toolbar of the Virtual Media Manager.
  * Browse to the extracted .vmdk file and select it.
  * Click <button>Continue</button> on the Virtual Hard Disk screen.
  * Review your hardware choices.
  * Clock on <button>Finish</button>

Start the virtual machine and after booting, login as root with password **clearos**.

<note warning>If you are using NAT network interfaces in VirtualBox, you will need to configure port forwarding in your VirtualBox environment.  This will allow you to access the ClearOS web-based interface running on TCP port 81.  You can find details about port forwarding in the [[http://www.virtualbox.org/manual/ch06.html#natforward|VirtualBox User Guide]]</note>

===== Next Steps =====
Once you have booted the system, you can configure and verify your network with the [[:content:en_us:6_network_console|Network Console]].
{{keywords>clearos, clearos content, clearos6, userguide, categoryinstallation, subcategoryinstaller, subsubcategoryvirtualmachines, maintainer_dloper}}

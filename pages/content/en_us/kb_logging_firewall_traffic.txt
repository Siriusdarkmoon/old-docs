===== Firewall Logging =====
This document will allow you to accomplish one or more of the following objectives:

  * Log all firewall events on your gateway or server
  * Fill up your hard drive and potentially crash your server

So you don't miss it, here is a warning:

<note warning>The following method can lead to excessive logging and has the potential of filling up your hard drive and crashing your server or gateway.</note>


===== Logging firewall events =====
There are various points at which the firewall can log packets. For the purposes of logging traffic that is passed both in and attempted out over your firewall (ie. gateway traffic), you can look at the post-routing rule.

{{:content:en_us:netfilter-packet-flow.svg?650|Copyright Creative Commons by Jan Engelhardt}}

The general rule that enables logging of the postrouting traffic is:

  iptables -t nat -I POSTROUTING 1 -j LOG

There are several ways to input this rule in ClearOS. You can:

  * Install the [[:content:en_us:6_custom_firewall|Custom Firewall]] module and enter the rule.
  * Modify either the local or custom file in **/etc/clearos/firewall.d/** directory.
  * Execute the command from command line to run it temporarily only.

===== Deactivating Logging =====
Besides removing or disabling the rule, you may have to flush your firewall rules and start them again without the logging parameter. The removal in the custom Firewall module will often do the trick but it may not stop the rule and you may have to remove it by hand in addition to removing it from the file you placed it in.

  iptables -t nat -D POSTROUTING -j LOG

Performing a 'restart' of the firewall won't necessarily remove the logging.

Restarting the server with the rule gone will also work.

To validate that logging is turned off, tail your log file and see if there are additional entries:

  tail -f /var/log/messages

===== Reading the Logs =====
The output for the logs will render by default to the /var/log/messages log file. This will be very, very chatty but a typical line will look something like this:

  May 12 20:35:02 myhostname kernel: IN= OUT=eth1 SRC=172.31.255.220 DST=8.8.8.8 LEN=90 TOS=0x00 PREC=0x00 TTL=126 ID=27501 PROTO=UDP SPT=50246 DPT=53 LEN=70 

Some of the values that are important here are:
  * OUT: The interface the packet is destined to exit
  * SRC: The IP address recorded as the source
  * DST: The IP address recorded as the destination
  * PROTO: The network layer protocol used (eg. TCP, UDP, ICMP)
  * SPT: The source port of used
  * DTP: The destination port used

In our example we can see that this is a DNS request from our LAN to a Google DNS server.

===== Dealing with large log files =====
Because this method is chatty, it will produce very, very big log files. To find how much space you have available. To find out how much space is available for your log file in the /var/log directory, run the following:

  echo $(( `echo $(($(stat -f --format="%a*%S" /var/log/.))) `/1024/1024/1024 )) GB && echo $(( `echo $(($(stat -f --format="%a*%S" /var/log/.))) `/1024/1024 )) MB

By default, this log file is compressed weekly and log file rotation keeps logs for a month or more.

When you activate this logging, pay close attention to the current size of the /var/log/messages file:

  ls -lh /var/log/messages

Then watch it over the next hour and couple of days. The syslog system will suppress huge dumps of data into the log file but it only you can prevent server crashes here if the log file is to grow too big. 

If you want to reassign the log folder to a different mounted device for more space, you can use the [[:content:en_us:kb_o_storage_manipulation_using_bindmounts|bind mount howto]] to accomplish your dreams of keeping all this data around for analysis.


===== Links =====
  * [[http://websistent.com/linux-iptables-log-everything/|Log Everything Howto]]
  * [[https://commons.wikimedia.org/wiki/File:Netfilter-packet-flow.svg|IP Tables Flowchard]]

{{keywords>clearos, clearos content, kb, app-firewall, networkcategory, maintainer_dloper}}

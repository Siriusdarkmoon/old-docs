===== PAM =====
PAM (Pluggable Authentication Modules) is a system security tool that allows system administrators to set authentication policy without having to recompile programs that handle authentication.

===== Changes =====
The pam_listfile module is used to grant access to various services.  For example, the **Web Proxy** app uses pam_listfile to ensure that only users listed in the **web_proxy_plugin** group are granted access.  With Active Directory and Samba 4 allowing mixed case usernames, this limitation is a wee bit problematic.  Keep in mind, PAM itself is not case sensitive, just the pam_listfile module.

{{keywords>clearos, clearos content, AppName, app_name, version6, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

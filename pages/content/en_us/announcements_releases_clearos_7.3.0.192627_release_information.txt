===== ClearOS Business, Home, and Community 7 - 2017 Q3 =====
**Production Released: 7 July 2017**

ClearFoundation and ClearCenter are proud to announce the ISO release of ClearOS 7.3.0.192627, also known as ClearOS 7, 2017 Q3 ISO. This is a sub-minor release of ClearOS 7.3.0 which was released in updates earlier this year.

==== Significant Features Included on this ISO ====

  * Roll up of language packs
  * Kernel VCE patch for AMD APU processors
  * Certified Support for HPE Proliant Gen9 Models for 300 Series and below as well as Microserver Gen10

==== New Upstream Features and improvements Include ====

  * Kernel fix for 'Stack Clash'

==== ClearOS Marketplace Updates since last ISO ====

  * [[:content:en_us:7_ug_mssql|Microsoft SQL Server]]
  * [[:content:en_us:7_ug_gateway_management_community|Gateway.Management Community]]
  * [[:content:en_us:7_ug_gateway_management_home|Gateway.Management Home]]
  * [[:content:en_us:7_ug_gateway_management_business|Gateway.Management Business]]


===== Installation and Upgrade Information =====
For existing systems, if you desire to have the fixes included with this ISO you merely need to run updates to the current version from a registered system. These should be automatically applied but to ensure that they are applied, run the following from command line:

  yum update

Since this update includes kernel fixes which apply to the security of your system, please restart your system and ensure that the version booted is 514.21.2.v7 or later by running:

  uname -r
 
If you are installing ClearOS fresh on a system, this release is for you. ClearOS 7 features a Community, Home, and Business version. All versions of ClearOS 7 will install from the same install image. You will be required to select your version during the initial setup wizard on the first boot after ClearOS is installed. You can also use this image to install and update to future ClearOS 7 versions.

==== Upgrade from Earlier ClearOS Community 6.x Releases ====
There is no supported method yet from upgrading from ClearOS 6 to ClearOS 7. 

Make a copy of your configuration data. Backup your user data as well or retire your ClearOS 6 disk. ClearOS 7 now supports the import of your configuration data but not your user data.



==== Change Log ====
The full change logs can be found here:

  * [[https://tracker.clearos.com/changelog_page.php?version_id=171|Changelog - Changes since ClearOS 7.2.0 ]]
  * [[https://tracker.clearos.com/changelog_page.php?version_id=331|Changelog - ClearOS 7.3.0 ]]
  * [[https://tracker.clearos.com/roadmap_page.php?version_id=241|Changelog - ClearOS 7.3.0 - Updates]]


===== Roadmap =====

  * Samba 4 Directory (Still, very much in beta)
  * Usability and Ease of Use improvements
  * Video & Surveillance solutions
  * Configuration Orchestration

=== Roadmap Tracker and Open Bugs ===
  * [[https://tracker.clearos.com/roadmap_page.php|Known bugs]]

===== Feedback =====
Please post your feedback in the [[https://www.clearos.com/clearfoundation/social/community/clearos-2017-q3-iso|ClearOS 2017 Q3 ISO]] Discussion Forum. 


===== Download =====
==== ISO Images ====
^ISO^Size^SHA-256^
|[[http://mirror1-orem.clearos.com/clearos/7.3.0.192627/iso/x86_64/ClearOS-DVD-x86_64-7.3.0.192627.iso|64-bit Full]]|915 MB| b14201b3f62c875bef3e34062c6a3595dd9e8c57099902f9e92efbce728d609a |
|[[http://mirror1-orem.clearos.com/clearos/7.3.0.192627/iso/x86_64/ClearOS-netinst-x86_64-7.3.0.192627.iso|64-bit Net Install]]|417 MB| 89ebba21c8f66e8e0682b83b11c14b615362052c862ef25355c5167015a2db7d |


{{keywords>clearos, clearos content, announcements, releases, clearos7.3, final, clearos7.3.0, maintainer_dloper}}

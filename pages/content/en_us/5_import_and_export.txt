===== Import and Export =====
New users to the system can be imported from a CSV (Comma Separated Values) file. This can save time especially if you have a large user count and need an automated way to create users. You can also export users from the system to a spreadsheet.

===== Installation =====
If you [[:content:en_us:5_software_modules|install a module]] that depends on users/groups, this feature will automatically be installed as well.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Directory|Accounts|Import/Export</navigation>

===== Configuration =====
==== Importing Users ====
To import users you must have a well-formed CSV file of the users. If you need to get a template of how the users should look in the CSV file, click on the **Download template** link.

To start the upload:

  * Click on the <button>Choose File</button> button. This will open a window on your browser. 
  * Find and select the file and click the confirmation button.
  * Click the <button>Upload CSV</button> button and the system will indicate the number of users it will try to import.
  * Click the <button>Import Users</button> button and the users will begin importing into the system.

<note warning>
It is very important that the format or your CSV file is correct. Also some characters and symbols may not import properly can can cause some issues.

If your import reports errors, note the users that had problems and fix them. The system will not import users that have already been added.

ALSO: it is important that you be patient while the browser performs the import. A log will indicate which record is being imported and which ones succeed or fail.
</note>

{{  :omedia:ss-importexport-options.png?350  }}
==== Exporting users ====
To export the users, click the <button>Export as CSV File</button> button. This will let you download a current list of users on the system.

<note>
The system will not export passwords. It does not store them in plain text or reversible encryption and will leave the password field blank.
</note>

==== Import Log ====
To view the last import log file, click the <button>View Last Import Log</button> button.

===== Tricks and Tips =====
When creating users and group memberships for large organizations, it can be useful to create sample infrastructure on the server and export that list instead of using the template. This way you can see the well formed fields and replicate entries for multiple users using copy and paste methods.

===== Template Database Fields =====

=== username ===
This is the user's name on the network. It is recommended that usernames should be listed in lowercase. There should not be any spaces in this field. You should also avoid funny characters and symbols in usernames. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== firstName ===
This is the First Name of the user. This name should have proper capitalization and spelling. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== lastName ===
This is the First Name of the user. This name should have proper capitalization and spelling. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== password ===
This is the password you intend for the user. You should avoid certain characters like asterisks. On user exports, this field will ALWAYS be blank. On user imports, this field must ALWAYS be defined. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== street ===
This is the street address for the user. If you created the user in the individual user creation page, this field is auto-populated with the data from the system certificate. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== roomNumber ===
This is the room or floor number for the user. If you created the user in the individual user creation page, this field is blank. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== city ===
This is the city for the user. If you created the user in the individual user creation page, this field is auto-populated with the data from the system certificate. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== region ===
This is the state, region, or province for the user. If you created the user in the individual user creation page, this field is auto-populated with the data from the system certificate. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== country ===
This is the country for the user. If you created the user in the individual user creation page, this field is auto-populated with the data from the system certificate. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== postalCode ===
This is the postal or ZIP code for the user. If you created the user in the individual user creation page, this field is auto-populated with the data from the system certificate. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== organization ===
This is the organization name for the user. If you created the user in the individual user creation page, this field is auto-populated with the data from the system certificate. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== unit ===
This is the business unit or department for the user. If you created the user in the individual user creation page, this field is auto-populated with the data from the system certificate. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== telephone ===
This is the telephone number for the user. If you created the user in the individual user creation page, this field is blank. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== fax ===
This is the fax number for the user. If you created the user in the individual user creation page, this field is blank. To be well-formed, the username should exist in quotation marks in the raw CSV file.

=== mailFlag ===
Enables a personal mail account for the user in the mail system (POP3/IMAP/SMTP). This is the equivalent of checking the 'Mailbox' option in the user account creation window in the 'Services' section. Acceptable values are:

  * TRUE
  * FALSE

=== mailquota ===
A numeric value representing the mailbox quota for the user expressed in megabytes. Leave this field blank for 'unlimited'. Acceptable values are:

  * 50
  * 100
  * 200
  * 300
  * 400
  * 500
  * 600
  * 700
  * 800
  * 900
  * 1000
  * 2000
  * 3000
  * 4000
  * //(blank)//

=== proxyFlag ===
Enables user-based authentication for the proxy server for this user. This is the equivalent of checking the 'Proxy Server' option in the user account creation window in the 'Services' section. Acceptable values are:

  * TRUE
  * FALSE

=== openvpnFlag ===
Enables access to the network externally via the OpenVPN server for this user. This is the equivalent of checking the 'OpenVPN' option in the user account creation window in the 'Services' section. Acceptable values are:

  * TRUE
  * FALSE

=== pptpFlag ===
Enables access to the network externally via PPTP VPN server for this user. This is the equivalent of checking the 'PPTP VPN' option in the user account creation window in the 'Services' section. Acceptable values are:

  * TRUE
  * FALSE

=== sambaFlag ===
Enables access to the Samba server for this user. In particular, the user will be provisioned a user directory via Windows Networking. This is the equivalent of checking the 'Windows Networking' option in the user account creation window in the 'Services' section. Acceptable values are:

  * TRUE
  * FALSE

=== ftpFlag ===
Enables access to the FTP server for this user. In particular, the user will be provisioned access to their user directory via FTP. This is the equivalent of checking the 'FTP' option in the user account creation window in the 'Services' section. Acceptable values are:

  * TRUE
  * FALSE

=== webFlag ===
Enables access to the web server share for this user. This is the equivalent of checking the 'Web' option in the user account creation window in the 'Services' section. Acceptable values are:

  * TRUE
  * FALSE

=== pbxState ===
This field is for support for VoIP enabled systems only. Acceptable values are:

  * TRUE
  * FALSE

=== pbxPresenceState ===
This field is for support for VoIP enabled systems only. Acceptable values are:

  * TRUE
  * FALSE

=== pbxExtension ===
This field is for support for VoIP enabled systems only. This is the Extension of the user in the VoIP subsystem provided with the VoIP module. By default this field is blank.

=== groups ===
This is a list of groups to which the user is a member. Values should be restrained within quotation marks and be comma separated. For example:
  "administration,accounting,projects"

{{keywords>clearos, clearos content, Account Import, app-account_import, clearos5, userguide, categorydirectory, subcategorysetup, maintainer_dloper}}

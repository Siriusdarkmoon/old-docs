===== PHP Engines =====
The PHP Engines app allows an administrator to select the PHP version to run inside each web server virtual host.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/server/php_engines|here]]
===== Documentation =====
==== Version 6 ====
This app is not available for ClearOS 6.
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_php_engines|here]].
==== Additional Notes ====
===== SSL Version 2 v2 Protocol Detection =====
This entry from Security Metrics is followed up with the following CVE: [[:content:en_us:announcements_cve_cve-2005-2969|CVE-2005-2969]].


===== ClearCenter response =====
==== Short response ====
ClearOS contains backported fixes for this flaw prior to the general release.

==== Long response ====
The flaw reported in the CVE for this issue was fixed in a backported patch. That being said, negotiation of the SSLv2 is only a security issue if the traffic is intercepted. This issue, as such is not a vulnerability to the server but rather to the information transmitted via SSLv2 if the client selects that protocol.

This is not a vulnerability to the server but rather to data integrity.

==== Resolution ====
Update the server to remove the CVE vulnerability.

The following settings should be replaced in your /etc/httpd/conf.d/ssl.conf file:

  SSLProtocol -ALL +SSLv3 +TLSv1
  SSLCipherSuite ALL:!aNULL:!ADH:!eNULL:!LOW:!EXP:RC4+RSA:!MEDIUM:+HIGH

You may also need to specify these settings in any specific virtual host file configuration that you use. Be sure to reload your configuration.

  service httpd reload

To test the settings try the following:

  openssl s_client -connect localhost:443 -ssl2

You should get a minimal output and NOT get the certificate. To try SSLv3:

  openssl s_client -connect localhost:443 -ssl3

You should get an output that includes the certificate.

Test additional virtual hosts on the system:

  openssl s_client -connect virtualhost.example.com:443 -ssl2


===== Links =====

  * [[http://cve.mitre.org/cgi-bin/cvename.cgi?name=2005-2969|MITRE.org CVE database]]
{{keywords>clearos, clearos content, 3rd party, security metrics, non-cve, maintainer_dloper}}

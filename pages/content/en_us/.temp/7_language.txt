===== Language =====
The **Language** app is used to change the language used by the operating system and other ClearOS apps that support internationalization.

===== Installation =====
This feature is part of the core system and installed by default.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>System|Settings|Language</navigation>

===== Configuration =====
Keeping this page simple and easy to find is important since you may have to look for it in a foreign language!  Simply select your language and click on <button>Update</button>
{{keywords>clearos, clearos content, AppName, app_name, clearos7, userguide, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

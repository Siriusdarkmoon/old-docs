===== SSL Compression Methods Supported =====
This entry from Security Metrics suggests that there is some risk associated with negotiation of SSL Compression methods in HTTPS.

===== ClearCenter response =====
==== Short response ====
Negotiation of protocol compression is allowed under protocol. This is not a risk.

==== Long response ====
In order to use protocol compression in SSL, both parties must agree on the methods that they will use. It is common for the server to offer the list of methods that can be used. Such information is only usable to the endpoints can is not a risk. Even in instances where a man-in-the-middle attack is present, such information is largely useless in and of itself.

Compression is not security nor is it intended to be security. This should not even be a factor of security analysis.

==== Resolution ====
No action required.

===== Links =====

{{keywords>clearos, clearos content, 3rd party, security metrics, non-cve, maintainer_dloper}}

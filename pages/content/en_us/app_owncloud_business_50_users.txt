{{ :userguides:owncloud_business_50_users.svg?80}}
<WRAP clear></WRAP>

===== ownCloud for Business =====
Additional 50 users for ownCloud Business for ClearOS.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/cloud/owncloud_business_50_users|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_owncloud_business_50_users|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_owncloud_business_50_users|here]].
==== Additional Notes ====

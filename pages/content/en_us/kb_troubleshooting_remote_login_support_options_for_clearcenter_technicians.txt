===== Remote Login Support for ClearCenter Technicians =====
When you are having an issue with ClearOS and you have a support option that allows for remote support, ClearCenter technicians may require login to your ClearOS box in order to assist you in fixing your issue. As with everything IT, remote support is a balance between security and convenience. The more secure the method, the more steps we will need to go through in order to set up increasingly secure methods. The benefits include:

  * Limited duration access using temporary credentials
  * Limited access using admin specific credentials
  * Session supervision
  * Logging

=== Limited duration access using temporary credentials ===
ClearCenter recommends changing the credentials used by your administrators temporarily while active support is underway. At the conclusion of the support session, you can change the credentials back. This way, ongoing credential methods are not exposed while support is being carried out. 

To change the 'root' password temporarily in ClearOS, xxxxxxxxxxxxx. You can also change the 'root' password from command line by issuing the following command while logged in as root:

  passwd

You will be prompted to supply the new password twice and the characters will NOT echo as you type. It will look like this:

<code>[root@server ~]# passwd
Changing password for user root.
New password: 
Retype new password: 
passwd: all authentication tokens updated successfully.
[root@server ~]# </code>

Once you have change root's password, supply the credentials to [[https://secure.clearcenter.com/portal/system_password.jsp|this portal.]] We do NOT recommend that you send our technicians your credentials through email as email is trasmited over the Internet using non-encrypted protocols.

=== Limited access using admin specific credentials ===
Instead of giving passwords to ClearCenter support admins, you can consider having that technician give you their public SSH key instead. That way you don't have to change your password. Moreover, you can merely comment out their credential in the future to temporarily cut them off. This method allows the technician to log in to SSH but NOT into Webconfig. In situations where SSH access only is required this method allows you to let the technician in on the basis of THEIR credentials instead of yours.

To use this method, request the public key of the technician in your support ticket. [[https://secure.clearcenter.com/portal/system_password.jsp|In the password submission portal]], submit your address and the root accout but for the password, type 'SSH Public Key requested'. Once the technician gives you his public key, modify the /root/.ssh/authorized_keys file and enter the public key sent to you.

The contents of the file may look something like this (do NOT use the example, it is an invalid key).

  ssh-rsa AAAAB3N6YR7kAsJI1LkZCRqkWiGwQzwe0uuyRH6SyknbDgfcjV6TYKiC5Gr2XqAaDKueMG+fjHKQo16jOWBO3657EnbTsqrbegcCLRkukI582tdEbOautM38WHQyWqX8okbQoKF1BfnhnZiM58eabkzFU4gIvh6YR7kAsJI1LkZCRqkWiGwQzwe0uuyRH6SyknbDgfcjV6TYKiC5Gr2XqAaDKueMG+fjHKQo16jOWBO3657EnbTsqrbegcCLRkukI582tdEbOf/CIr6YR7kAsJI1LkZCRqkWiGwQzwe0uuyRH6SyknbDgfcjV6TYKiC5Gr2XqAaDKueMG+fjHKQo16jOWBO3657EnbTsqrbegcCLRkukI582tdEbON7KbPJw== dloper@my-computer-name

=== Session supervision ===
Session supervision is where you allow ClearCenter support admins to share the screen which you are using. There are a number of 3rd party methods for providing you the ability to monitor what is going on in the support session. There is also a method available on ClearOS called 'screen'. If you use a 3rd party method, you will likely have the ClearOS technician remote control a Windows or Mac workstation and then you will access the ClearOS server on behalf of the technician. The advantage here is that you can observe the technician and this provides you with the ability to limit the technician to direct information about the server. This method is often required for high security environments.

Third party utilities include:

  * Teamviewer
  * Crossloop
  * VNC
  * GoToMeeting
  * LogMeIn
  * many, many others

Screen, the linux console sharing method:

To install screen, run the following from command line:

  yum install screen

Once screen is installed, you can have the technician log into a non-privileged SSH session and then join his screen and elevate privileges. You can also submit the root credentials and just ask to monitor all activity (when trust is high).

=== Logging ===
One of the advantages to using screen (even when using a 3rd party sharing method while ALSO using screen) is that you can log all entries during the login session. Logging is useful for all high security remote control sessions because it allows for logging of all entries by the technician. This is useful if an audit of what the ClearCenter technician did on your system is required or mandated by your company's security policy.

===== Methods =====
The methods for remote support are:

  * Direct access
  * Direct access with shared screen
  * Tertiary access with screen with guided login

==== Direct access ====
With direct access, security confidence is high. Meaning, your trust relationship with ClearCenter is high.

ClearCenter technicians consider customer data to be private and exclusive to the customer. We have a long tradition of keeping customer data secure. As such, many clients trust our technicians to maintain the highest levels of propriety with their site data. ClearCenter does NOT share customer data with 3rd parties and technicians to not seek out data while on customer systems. While some data may be viewed as a consequence of the particular support required (for example, a technician may see an email headers which includes a list of message recipients while troubleshooting a backed up SMTP server queue), all customer data is NOT subject to scrutiny by the technicians unless is required to the solution of the issue at hand.

Direct access is the most convenient method. Simply submit your credentials to the portal and reference that in the ticket.

[[https://secure.clearcenter.com/portal/system_password.jsp|Password Submission Portal]]

==== Direct access with shared screen ====
With direct access to command line provided to a ClearOS technician, you and the technician can join together in a command line sharing screen session. This is typically used for the following:

  * Training or observation capabilities
  * Moderate security concerns

==== Tertiary access with screen with guided login ====
Tertiary access is where you grant ClearOS engineers access to a completely separate box in your control as a non-root user. You then join a screen session with that user and then use the credentials for which only you know to gain access to the box needing support. This is required when the optimum security of the box is required and ONLY supervised access to the server is allowed. Per usual, logging options under 'screen' are still available.

===== Help =====
==== Links ====
  * [[http://www.tecmint.com/screen-command-examples-to-manage-linux-terminals/|Command line screen sharing examples]]


==== Navigation ====

[[:|ClearOS Documentation]] ... [[..:|Knowledgebase]] ... [[:Knowledgebase:Troubleshooting:|Troubleshooting]]
{{keywords>clearos, clearos content, AppName, app_name, versionx, xcategory, maintainer_x, maintainerreview_x, titlefix, keywordfix}}

{{ :userguides:kaspersky_gateway.svg?80}}
<WRAP clear></WRAP>

===== Premium Gateway Antimalware by Kaspersky =====
Gateway Antimalware Premium is powered by Kaspersky Lab to provide secure web browsing for your organization.  The app transparently scans and automatically removes malicious and potentially hostile programs from incoming HTTP/FTP traffic using technology that is reliable and scalable.  Kaspersky Lab is firmly positioned as one of the world's leading IT security software vendors for endpoint users.

This app includes a license for up to 50 users.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/gateway/kaspersky_gateway|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_kaspersky_gateway|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_kaspersky_gateway|here]].
==== Additional Notes ====

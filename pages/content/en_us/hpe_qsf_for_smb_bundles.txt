====== Quick Select Files for HPE SMB Bundles ======

===== Introduction =====

ClearOS Quick Select Files are designed to efficiently select and install apps and packages for ClearOS. Quick Select Files (QSF) contain metadata used by the ClearOS Marketplace engine. Instead of installing apps one-at-a-time from the ClearOS Marketplace, QSF files provide a streamlined method for installing a list of known applications as a batch during the ClearOS install process. QSF files are particularly helpful when provisioning multiple ClearOS systems for similar deployments or configuring ClearOS for predefined uses (as is the case with [[https://www.hpe.com/us/en/servers/entry-level.html#ourSolutions|HPE’s Small Business Solution Bundles]]).

===== Before You Begin =====

Download the appropriate Quick Select File for your HPE Small Business Solution use case from one of the 3 links below. Place the file on a USB drive or locally on the PC/Mac you will use to log into the ClearOS browser-based Webconfig console during the Getting Started phase of the ClearOS install. Make sure the file is saved as a text file using a '.txt' file extension.

===== Quick Select Files =====

==== Unified Threat Management ====

Version 1.0 - May 31, 2018

[[https://ftp1.clearos.com/qsf/HPE_UTM_SMB_Solution_QSF.txt|UTM QSF]]

==== Storage & Backup ====

Version 1.0 - May 31, 2018

[[https://ftp1.clearos.com/qsf/HPE_Storage_Backup_SMB_Solution_QSF.txt|S&B QSF]]

==== Multi-Function ====

Version 1.0 - May 31, 2018

[[https://ftp1.clearos.com/qsf/HPE_Multifunction_SMB_Solution_QSF.txt
|MF QSF]]

===== How to Configure ClearOS with a Quick Select File =====

One of the last steps in the Getting Started phase of a ClearOS installation is the Marketplace step in which you are presented with four options to proceed (see screenshot below). One of these options is to use QSF to select what apps and packages should be installed. Select this option by highlighting it, and click on “Next” to upload your QSF file.

{{content:en_us:7_qsf_01.png}}

On the following step in the Wizard, you will be asked to upload your QSF file. Click “Choose File” to select the .txt file you have previously saved to a USB drive or your local device then click “Upload Quick Select File”.

{{content:en_us:7_qsf_02.png}}

Once you have uploaded your QSF file, if successful, the form will display how many apps and packages will be installed. It will also display, in tile format, all apps that are available. Apps that were included in the QSF file will already be selected for installation as shown in the screenshot below. Click “Next” to continue.

{{content:en_us:7_qsf_03.png}}

ClearOS may take up to a few minutes to process and verify the list of applications for compatibility. When complete, you will see a screen similar to the screenshot below displaying an App Review page with a final list of the applications to be installed. Click “Next” to continue then click the Confirm on the pop up to complete the setup process.

{{content:en_us:7_qsf_04.png}}
{{ :userguides:kaspersky_gateway_50_users.svg?80}}
<WRAP clear></WRAP>

===== Premium Gateway Antimalware by Kaspersky =====
Additional 50 users for Premium Gateway Antimalware - Powered by Kaspersky Labs.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/gateway/kaspersky_gateway_50_users|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_kaspersky_gateway_50_users|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_kaspersky_gateway_50_users|here]].
==== Additional Notes ====

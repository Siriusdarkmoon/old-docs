===== PHP Engines =====
This app allows you to run multiple versions of PHP in ClearOS
===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:7_ug_Marketplace|install the module]].
===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Web|PHP Engines</navigation>

{{7_ug_php_engines_1.png}}

{{keywords>clearos, clearos content, app_php_engines, php_engines, php, web server, flexshare, clearos7, userguide, categoryserver, subcategoryweb, maintainer_nhowitt}}


From here you can selectively enable and disable the required versions of PHP by clicking on the <button>View</button> button

{{7_ug_php_engines_2.png}}

<note>You cannot disable PHP 5.4 as this is the ClearOS default version</note>

===== Using the different versions of PHP =====

Using the different versions of PHP on your websites is controlled through the [[7_ug_web_server|Web Server app]] or the [[7_ug_flexshare|Flexshare app]].
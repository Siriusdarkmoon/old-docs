===== Fusion PBX =====
"FusionPBX is an open source FreeSWITCH GUI (Graphical User Interface). FusionPBX can be used as a high available single or domain based multi-tenant PBX, carrier grade switch, call center server, fax server, voip server, voicemail server, conference server, voice application server, appliance framework and more. FreeSWITCH(tm) is a highly scalable, multi-threaded, multi-platform communication platform. FusionPBX license is the MPL 1.1 license.

It can be run on most operating systems on the hardware of your choice. It provides unlimited extensions, voicemail-to-email, music on hold, call parking, call center, call queues, analog lines or high density T1/E1 circuits and many other features. It provides the functionality your business needs and brings corporate level phone system features to small, medium and large businesses."  Source: http://www.fusionpbx.com/

====== How to install FusionPBX on ClearOS ======
  * Testing: https://suite.tiki.org/FusionPBX
  * Reference implementation is with Jitsi

====== Todo ======
   * Configure with LDAP: https://code.google.com/p/fusionpbx/issues/detail?id=89

====== Related links ======
   * https://okay.com.mx/en/linux/rpm-repositories-for-centos-6-and-7.html
   * https://suite.tiki.org/FusionPBX

{{keywords>clearos, kb, howtos, tiki, skunkworks, maintainer_dloper, maintainerreview_dloper}}

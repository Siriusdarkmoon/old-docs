===== Types of Network Interfaces in ClearOS =====
ClearOS supports a variety of physical, virtual, and logical interfaces which you can use to connect your ClearOS server or gateway to the internet, LAN, and other networks.

This document does not cover security roles for interfaces which is covered [[:content:en_us:kb_o_network_types_-_external_lan_hotlan_dmz|here]].

===== Supported interfaces types =====
While the base OS supports many interface types, you will only be able to get support for the most common types listed here. If you need support for a type not listed here, please experiment with those using howtos related to CentOS or RHEL.

  * Standard Ethernet
  * Loopback
  * VLAN
  * Virtual
  * Bridge
  * Bond

==== Standard Ethernet ====
By and large, standard Ethernet ports are what is commonly used. In most situations, ClearOS is used with regular, standard Ethernet interfaces configured in one of the 4 common security modes; External (meaning faces internet), LAN (faces away from internet), DMZ (faces away from internet), and HotLAN (faces away from internet). Under ClearOS 6, your interfaces will show up at 'ethN' where 'N' is a number. Under ClearOS 7, your interfaces will typically show up as 'ensNnN' where 'NnN' can be a series of numbers and letters or just a single number.


==== Loopback ====
The loopback device is a special network interface. The standard IP address for the loopback is 172.0.0.1 for each and every box that has TCP/IP running. This interface is not useful for conducting communication to any outside machine but rather is used for internal communication. Because this interface is only useful to testing, development and other system level activities, it is disregarded in a layout of interfaces even though it is present. When you encounter this interface it will have the name of 'lo'.


==== VLAN ====
[[https://en.wikipedia.org/wiki/Virtual_LAN|VLANs]] are special interfaces under ClearOS that allow you to set up additional, separate logical interfaces under a single physical interface. In order to use VLANs, your interface must be connected to another device on this physical interface that understands the VLAN configuration on the other end. This is typically a switch that has the port that ClearOS is plugged into configured in 'trunking' mode with a specified VLAN ID. The VLAN ID can be a number between 1 and 4,094. This will end up with an interface that is constructed that will contain the first part of the interface name, a dot, and then the VLAN ID. For example, if your standard interface name is 'ens1p0' and your VLAN ID was 804 then your interface would be named 'ens1p0.804'.

For VLANs, you should NOT use a configuration on the standard interface but only under VLANs. Meaning, if you need to set up a VLAN on an interface, all network configurations should be also VLAN-based.

==== Virtual ====
Virtual addresses are simply addresses that can be made to ALSO answer on an existing standard Ethernet interface. Such virtual addresses should use the same subnet as the existing IP on the standard Ethernet interface. In the system, a series of numbers will be added to the name of the interface after a colon. For example, if this will end up with an interface that when constructed will contain the first part of the interface name, a colon, and then the Virtual Interface designation. For example, if your standard interface name is 'ens1p0' and your Virtual IP address designation was 200 then your interface would be named 'ens1p0:200'.

For purposes of organization, ClearOS will use virtual interface designations for things defined in the UI between 0 and 199. Interfaces created by users will start at 200 and go from there.

=== Virtual interfaces on VLANs ===
It is possible to ALSO set up virtual interfaces on top of VLAN interfaces. You cannot, however, set up a VLAN on a virtual interface. The resulting interface will use periods and colons for their designations. For example, ens1p0.804:200 could be a valid interface name.

==== Bridge ====
Bridges are interfaces that virtually exist and then have physical or logical interfaces bound onto them. Bridge devices are essential to virtualization and can be used in ClearOS to turn extra network interfaces into a switch. Meaning if you have 8 NICs on your server that you can create a bridge and give it and IP address. Then you will take unconfigured network interfaces and add them to the bridge. Doing this will allow your interfaces connected to different NICs to behave as though they are connected to a switch. In addition to this coolness, there is the potential of performing certain types of filtration, scanning, and other network management now that your ClearOS server is a switch. Some of these methods are contained in howtos on this site.

==== Bond ====
Bonding allows to interfaces to work together to send traffic to the same logical connection on the other end. If you configure ClearOS in this manner then you can put two network cards together to gain the benefits of redundancy and additional performance. Of course, the switch on the other end needs to both support this and be configured, but it is possible. This methods is loosely supported in the backend by the ClearCenter team but is not available in the UI.



{{keywords>clearos, clearos content, kb, version6, version7, categoryinstallation, categorynetwork, maintainer_dloper}}

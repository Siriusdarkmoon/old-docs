===== Windows Networking Login Process =====
This Howto covers the conditions for client login process for Windows Networking under ClearOS.

===== Authentication =====
Windows workstations can authenticate with ClearOS using the Windows Networking protocol (CIFS) in two different ways.

=== Domain login ===
Using the domain login, the account which the user authenticates does NOT exist as a local workstation. Instead, the workstation is joined to the domain and therefore 'trusts' the domain controller's accounts.

=== Peer login ===
Peer logins work by correlating the local login with the domain login. When a client tries to access a server the client will automatically volunteer the credentials to the server which the user logged in. In the case where a user logged into a server with a username and password, this means that it will, by default, offer that username and password. Imagine then that you have a username on the local machine that is the same as exists on the Primary Domain Controller. If the password is different, it will deny the user even though the user can access all local items. If the password happens to be the same, you are in; no domain joining required.

===== Login events =====
==== GINA ====
On windows workstations, the login prompt is actually a self contained program called the GINA (Graphical Identification 'nd Authentication). It is provided by a dll and can actually be replaced with other funky login processes. We won't go into detail here because the Microsoft GINA is sufficient for ClearOS logins.

==== LSA ====
The GINA communicates the username and password to the Local Security Authenticator (LSA). If the workstation is joined to the domain and the domain was specified in the GINA, then the workstation will contact the domain controller using at least NTLM and at most NTLM and Kerberos. ClearOS does not require Kerberos when using the Windows Networking module. Future versions which use Samba 4 will use the Kerberos as a requirement. In the case where a domain controller is specified and the LSA cannot reach the domain controller, the LSA will access the cached credentials file to see if the authentication should complete anyways.

==== WinLogon ====
The winlogon process handles the post authentication and pre-authorization tasks. This includes some of the following:

  - Desktop lockdown
  - Standard SAS recognition
  - SAS routine dispatching
  - User profile loading
  - Screen saver control
  - Multiple network provider support
  - Logon scripts and Group Policy Objects

=== Profiles ===
Profiles are a blessing and a curse. They allow your users to maintain certain aspects of their desktop environment between workstations. This is useful if you have multiple users that user different computers each day. However, roaming profiles copy themselves by default on the workstations which means that you can consume a lot of time and bandwidth for logon and logoff events if you use this feature.

Roaming profiles ALWAYS take time to load when you log on and log off. The problem gets even worse as the profile gets bigger. This is because when a log on event occurs the client workstation will sync the data from the server to the workstation. When a log off event occurs, the workstation will sync all the profile from the workstation back to the server.

As you add additional items into the profile such as items to the desktop or other resources, these items get sync'd. A training policy that teaches users to store large items on shared or personal storage like their home directories on the server can mitigate long logon and log off events.

To test the baseline for a roaming profile, discover if a new profile with not significant data takes a long time to load. If it take a long time to load with virtually no data, then you should look at things like performance of the server and other potential samba tunables to speed of the authentication process. If a blank profile is quick and a regular user profile is long, chances are that it is long because the users is consuming too many resources within their profile structure.

=== Logon scripts ===
Since the Windows Networking module is not an Active Directory module it doesn't support some things like Group Policy Objects. It does, however, support logon scripts. This can be used to distribute GPO files and run time commands. The login script is located typically at 

  \\servername\netlogon\logon.cmd

This is a hidden share and you should access the server logged in as winadmin to modify it or use a tool like WinSCP to copy up changes. This script can reference other scripts located in the Netlogon directory or its subdirectories, other directories or UNCs, local files, and can push registry changes, execute programs and even register .POL file to modify certain group policy object.

===== Links and References =====
{{keywords>clearos, clearos content, KB Old ClearOS Guides Windows Networking Login Process, app_name, xcategory, maintainer_dloper, maintainerreview_x, keywordfix, userguide}}

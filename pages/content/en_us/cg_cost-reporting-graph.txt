===== Dashboard - Cost Reporting Graph =====
ClearGLASS will dynamically list the costs and inventory count of all your machines across all your clouds to make it easy to see your current spend. This graph is located on the main page of the console.

The Total Cost is a snapshot of your infrastructure spend across your clouds. The cost projection does not include stopped instances.

[image]
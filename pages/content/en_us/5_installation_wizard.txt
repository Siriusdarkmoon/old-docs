===== Installation Wizard =====
After [[:content:en_us:5_starting_the_install|starting the install]], you will be presented with a simple and short installation wizard.  The wizard takes you through the basic configuration options that are required to  install ClearOS.  Once completed, your installation will finish in only a few minutes.

To navigate the wizard, you can use the following keys:

  * <button>Tab</button> - navigates between fields
  * <button>Arrow keys</button> - navigates within elements of a field
  * <button>Enter</button> - selects the current highlighted item

The details of each wizard screen is detailed below.

===== Video Tutorial =====
{{youtube>large:M5G1IaDOCCo|2-minute ClearOS install}}

===== Language =====
At the language screen, use the arrows to navigate to the language you want to configure your system for.  Press tab to highlight the <button>OK</button> button and then press <button>Enter</button> to confirm.

===== Keyboard =====
At the keyboard type screen, select the keyboard layout that most resembles your system's keyboard (for example, 'us' for United States and Canada). Navigate (tab) to select <button>OK</button>, and then hit <button>Enter</button>.

===== Install Media ===== 
For Installation method, if you are using a CDROM or DVD install disk, select Local CDROM.  If you are using other methods including PXE or USB to install, select //Network// (HTTP).

===== Install / Upgrade =====
If you are installing ClearOS Enterprise for the first time, leave //Install// selected and press <button>OK</button>. If you are upgrading your system, select //Upgrade// and press <button>OK</button>.

If you are upgrading, this will be the final step in the installation wizard.

===== Warning Screen =====
At the warning screen, read the warning and follow the directions on the screen.  Capitalization is important!

<note warning>The installation process will remove all data attached to the server (including USB and firewire drives).</note>

===== Server Type =====
ClearOS supports both gateway and standalone server modes.

  * Standalone mode is used to create a server on a local area network (behind an existing firewall).  Only one network card is required. 

  * Gateway mode allows your system to act as a firewall, gateway and server on your local network.  At least two network cards are required.  If you have two or more network cards installed in the server and want to protect your local network against threats originating from the Internet, then select gateway mode.

===== Network Connection Type =====
If you are installing with a CD-ROM, you will need to select the type of Internet connection you have (DSL, DSL/PPPoE, Cable).

===== Network Card Drivers =====
You will need to manually configure your network card settings if the installer does not automatically detect the driver.

===== Network Configuration =====
Unless your Internet Service Provider (ISP) provides a static IP address, it is recommended that you use Dynamic IP Configuration.  If your ISP assigns a static IP you will need to enter the individual TCP/IP settings as provided by your ISP.  Make sure you have these settings available during the installation process.

==== PPPoE ====
ClearOS supports PPPoE DSL connections. Add the username and password provided by your ISP on this screen. For brain dead ISPs, you may also need to specify DNS servers.

==== LAN IP Address ====
If you are installing ClearOS as a gateway, you must specify the network settings for your local area network. The **LAN hostname** can be used instead of the IP address for many network tools.  For instance, you will be able to access the web-based administration tool at https://LAN-hostname:81 in your web browser.

===== Hostname - Password - Time Zone =====
The next few screens will ask for your system name, system password and time zone.

<note warning>
Do not forget your system password!
</note>

===== Hard Disk Partitioning =====
If you would like to specify your own partition scheme, then you should select **yes** on the **Select Partition Type** screen.  The //Advanced Partitioning// screen will appear in the second stage of the installation process... don't panic!

If you selected custom/advanced partitioning, please refer to the [[:content:en_us:5_configuring_partitions_and_raid|Advanced Partitioning Guide]] for help once you complete the software selection process.

===== Software Modules =====
Select the software components to install on your system. Not all the modules are shown here - don't panic. With the ClearOS web-based configuration, you can add other modules at any time.

You will receive a final confirmation page and then the install will proceed format your drive(s) and to copy the files to your server and the conclusion of the install you should get a prompt to reboot the server.  Do not forget to remove your installation media.
{{keywords>clearos, clearos content, Installation Wizard, clearos5, userguide, categoryinstallation, subcategoryinstaller, maintainer_dloper}}


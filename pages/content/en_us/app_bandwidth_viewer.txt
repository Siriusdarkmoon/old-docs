{{ :userguides:bandwidth_viewer.svg?80}}
<WRAP clear></WRAP>

===== Bandwidth Viewer =====
The Bandwidth Viewer draws a live graph of your interfaces bandwidth. This can help to determine the maximum speed of your interfaces and identify bandwidth problems across your network.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/reports/bandwidth_viewer|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_bandwidth_viewer|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_bandwidth_viewer|here]].
==== Additional Notes ====

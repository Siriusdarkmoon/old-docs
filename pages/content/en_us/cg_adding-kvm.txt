===== KVM =====
ClearGLASS supports adding KVM hypervisors via  libvirt library. This will return a list of all virtual machines (running and terminated), along with the hypervisor itself. You can execute actions such as reboot, shutdown, undefine, suspend, resume and start, provided that they are supported by the version of libvirt. You can also create virtual machines throught iso images or through disk images and even deploy new ones through cloudinit based images. . Ability to VNC on virtual machines is going to be supported soon. A more detailed article about KVM management can be found on http://docs.ClearGLASS/article/99-managing-kvm-with-mist-io

==== Connecting through ssh ====
In order to add your KVM hypervisor, you will need to provide the hostname, a username and ssh key, that will be used by libvirt to communicate with the server. The username has to be root, or a user that has access to libvirt (eg a sudo user). ClearGLASS does not support encrypted ssh keys, so make sure you provide an ssh key without a passphrase.

ClearGLASS via libvirt will try to communicate with the KVM hypervisor using the user/port/key combination and if it succeeds, the hypervisor will be added and you will be able to see your VMs in the Machines section.

[image]

You can enable monitoring for a machine, by pressing Enable monitoring. If you have an ssh key associated, ClearGLASS will deploy collectd open source monitoring agent automatically. If you haven't associated a key, a bash command will appear in a popup for you to run on the machine manually.

[image]


=== Common issues ===
== 1) User permission errors ==
If you provide a user which has rights to connect to libvirt (root or sudoer), and a valid ssh key, it should be enough to connect to the remote kvm hypervisor. To make sure run the command below from a remote server:

<code>
virsh -c "qemu+ssh://root@remote_kvm_server:22/system?keyfile=/home/user/.ssh/ssh_key&no_tty=1&no_verify=1" list<br>
</code>
		
where you replace root@remote_kvm_server with the username (if different than root) and remote kvm host, and  /home/user/.ssh/ssh_key with the path of your ssh private key file. If this succeeds and you are able to see the listing of your VMs, then ClearGLASS can connect as well (provided that you specify the same user/port/hostname/ssh key combination)

If the command fails to run, but you are able to ssh to the server with this user/key, checkout how libvirt is locally setup and if the user has the permission to access it.

== 2) Invalid private key ==
Make sure you are adding a passphrase-less private ssh key. ClearGLASS currently does not support encrypted private keys and it won't accept them. 

=== Connecting through tcp ===
If you don't specify an ssh key on the add KVM wizard, ClearGLASS will consider that you are connecting via tcp (qemu+tcp), in this case the KVM hostname should have tcp port 5000 open and allowing connections from ClearGLASS. If you are running the open source standalone version of ClearGLASS and the KVM hypervisor can be connected via unix socket, you could specify localhost as the hostname, and ClearGLASS will try to connect via unix socket (qemu:///system)

==== Related Articles ====
  * [[content:en_us:cg_setting-up-clearglass|Quick Start Guide]]
  * [[content:en_us:cg_monitoring-on-linux|Setting up monitoring on Linux]]
  * [[content:en_us:cg_manage-infrastructure|Managing Infrastructure]]
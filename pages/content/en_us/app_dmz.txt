{{ :userguides:dmz.svg?80}}
<WRAP clear></WRAP>

===== DMZ =====
The DMZ app allows an administrator to create a physical subnetwork, providing accessibility from untrusted networks (eg. the Internet) while maintaining isolation to the Local Area Network (LAN).  Typically, a DMZ is used to host an organization's external services like website, web portal, SMTP/mail etc.  In order to configure this app, the server must have a minimum of 3 network interfaces.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/network/dmz|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_dmz|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_dmz|here]].
==== Additional Notes ====

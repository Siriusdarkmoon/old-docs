===== Creating a Local Mirror =====
Keeping a local mirror of ClearOS can be useful in some circumstances.  This document provides a quick guide on how to implement one.

===== Local Mirror ClearOS 6.x and 7.x =====

==== Find and Allocate Disk Space ====
You can expect the ClearOS 6.x and 7.x releases to take up about 40-60 GB before the version reaches its end of life. Local mirrors are 'current' mirrors so you won't need to store the whole tree for the entire life of the project.  So the first step in creating a local mirror is to find and allocate the necessary disk space on your system.  If you are using ClearOS, you can use a Flexshare for this task.  In our example, we have created a Flexshare called **mirrors** and made sure **Web** access was enabled.  On the file system, the mirror directory defaults to /var/flexshares/shares/mirrors.

==== Download / Synchronize ====
Now that you have a mirror directory all set up, it's time to start the first synchronize. We will use the ftpsync program to sync our mirrors. You can download the ftpsync program here [[http://repo.clearfoundation.com/ftpsync.tgz|here]]:

  [[http://repo.clearfoundation.com/ftpsync.tgz]]

From an ssh command prompt as root you can perform the following from the directory that you want to install it from (e.g /root/support/extrapackages):

  yum -y install wget
  wget http://repo.clearfoundation.com/ftpsync.tgz

Extract the file:

  tar xvzof ftpsync.tgz

  
For ClearOS 6.x, there are two distinct mirrors that need to be kept synchronized:

^Mirror^Description^
|clearos|The ClearOS RPMs|
|clearcentos|The slimmed down CentOS mirror|

To synchronize the ClearOS download servers with the local Flexshare mirror, run:

  rsync -rltv --delete --progress sync.clearfoundation.com::clearos /var/flexshare/shares/mirrors/clearos/ 
  rsync -rltv --delete --progress sync.clearfoundation.com::clearcentos /var/flexshare/shares/mirrors/clearcentos/
 
===== Local Updates =====
If you want your internal ClearOS systems to use your new local mirror, change the yum repository entries in /etc/yum.repos.d/base.repo.  The excerpt below shows how one of the repository entries (base-os) was changed.

<code>
[clearos]
name=ClearOS $releasever - $basearch - OS
#baseurl=http://mirror.clearos.com/clearos/$releasever/os/$basearch/
mirrorlist=http://mirrorlist.clearos.com/clearos/mirrorlist/clearos-current-os-$releasever
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ClearOS-$releasever
</code>

Changed to:

<code>
[clearos]
name=ClearOS 7.2.0 - 7 - OS
baseurl=http://192.168.1.1/clearos/7.2.0/os/7/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ClearOS-$releasever
</code>

When testing your new repository, you may need to run **yum clean all** command to make sure you are not using cached data.

{{keywords>clearos, clearos content, kb, clearos5, categorysdn, howtos, maintainer_dloper, maintainerreview_dloper}}

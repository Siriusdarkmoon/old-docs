===== Outgoing Firewall =====
The **Outgoing Firewall** feature lets you block or allow certain kinds of  traffic from leaving your network.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Network|Firewall|Outgoing</navigation>

===== Configuration =====
The **Firewall Outgoing** feature is useful for blocking/allowing instant messaging, chat, certain type of downloads, and more.  You may also want to block traffic using the [[:content:en_us:5_protocol_filter|Protocol Filter]], so make sure you review that feature as well.

You have two ways to block/allow traffic:
  * by destination port/service
  * by destination IP address/domain

==== Choose an Outgoing Mode ====
There are two modes to the **Firewall Outgoing** system:

  * Allow all outbound traffic and block specific types of traffic (default)
  * Block all outbound traffic and allow specific types of traffic

==== Outgoing Traffic - By Port/Service ====
**Destination Ports** prevents/allows a connection on a particular port/service.  For instance, adding port 80 (web) disables/enables web-surfing for your entire local network.

==== Outgoing Traffic - By Host/Destination ====
**Destination Domains** allows you to block/allow certain networks and sites. For instance, if your outgoing mode is set to allow all outgoing traffic, you could block the entire island nation of [[http://en.wikipedia.org/wiki/Tuvalu|Tuvalu]] (why would you do that?) by adding the 202.2.96.0/19 network to the block list.  

<note warning>If you block destinations with the firewall bear in mind that users of the proxy may not be blocked.  If you require proxy users to be blocked, your best option is to block the destinations using the [[:content:en_us:5_content_filter|Content Filter]].</note>
{{keywords>clearos, clearos content, Outgoing Firewall, app-egress_firewall, clearos5, userguide, categorynetwork, subcategoryfirewall, maintainer_dloper}}

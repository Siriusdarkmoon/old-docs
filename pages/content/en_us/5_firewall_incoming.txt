===== Incoming Firewall =====
The **Firewall Incoming** feature is used for two primary purposes:

  * To allow external (Internet) connections to your ClearOS system
  * To permanently block a particular IP address or entire networks from accessing ClearOS

===== Installation =====
This feature is part of the core system and installed by default.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Network|Firewall|Incoming</navigation>

===== Configuration =====
==== Incoming Connections ====
When the firewall is enabled on your ClearOS system, the default behavior is to block all external (Internet) traffic.  If you plan on running services **on** your ClearOS system that can be accessible from the Internet, then you will need to add the firewall policy to do so.  For example, the [[:content:en_us:5_openvpn|OpenVPN server]] requires UDP port 1194 to be open on the firewall.

You can also open up ports to allow for remote management of your ClearOS system.  For example, you can open up TCP port 81 to give access to [[:content:en_us:5_webconfig|Webconfig]].

There are three ways to add an incoming firewall rule: 

  * select a standard service in the **Standard Services** drop down
  * input a protocol and single port number in the **Port Number** box.
  * input a protocol and multiple consecutive ports in a port range in the **Port Range** box.

<note info>Unlike some other firewall systems you do **not** need to open a port on the incoming page if you are [[:content:en_us:5_firewall_port_forwarding|forwarding the the port]] to an internal server on your network.</note>

==== Block External Hosts ====
{{keywords>clearos, clearos content, Incoming Firewall, app-incoming_firewall, clearos5, userguide, categorynetwork, subcategoryfirewall, maintainer_dloper}}

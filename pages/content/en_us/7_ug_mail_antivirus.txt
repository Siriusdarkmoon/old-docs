===== Mail Antivirus =====
The **Mail Antivirus** app scans mail messages as they pass through your ClearOS mail system.  It is the first line of defense to prevent malicious e-mail messages from reaching your end users.

Viruses can be costly in terms of downtime and cleanup.  All it takes is one!
===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Messaging|Mail Antivirus</navigation>

===== ClearCenter Antimalware Updates =====
The open source [[http://www.clamav.net|ClamAV]] solution is the antimalware engine used in ClearOS.  This software automatically checks for updates several times a day for new antivirus signatures.  This is already included in ClearOS for free!

{{:omedia:clearsdn-icon-xxs.png }} In addition, the ClearCenter [[http://www.clearcenter.com/Services/clearsdn-antimalware-updates-7.html|Antimalware Updates]] service provides //additional// daily signature updates to improve the effectiveness of the antimalware system.  These signatures are compiled from third party organizations as well as internal engineering resources from ClearCenter.  We keep tabs on the latest available updates and fine tune the system so you can focus on more important things.

===== Configuration =====
==== Mail Policies ====
When configuring the antimalware system, you must make some mail policy decisions.  There are three types of policies available:

  * **Bounce** - bounce the e-mail
  * **Discard** - silently discard the e-mail
  * **Pass Through** - allow e-mail, but with warning (original sent as an attachment)

=== Detected Virus Policy ===
When a virus is detected, you can choose to either discard the message, or pass the message through.  We recommend discard mode for most installations.

=== Bad Header Policy ===
When a bad e-mail header is detected, you can choose to either discard the message, or pass the message through.  We recommend pass through mode for most installations.

=== Banned File Extension Policy ===
The antimalware software not only performs virus scanning, but also manages file attachment policies.  Certain types of file attachments are prone to viruses.  The ability to block attachments by file extension is another layer of security for your mail system.

==== Banned File Extensions ====
Select the file extensions that you wish to ban from going through your mail system.  Both internal and external mail are checked.

=== Microsoft Office Documents ===
<note warning>If you have problems receiving some Microsoft Office documents, try allowing ".bin".
Macro-enabled documents (docm, xlsm etc.) have their own filters which can be controlled separately.</note>

===== Related Links =====

  * [[:content:en_us:7_ug_antimalware_updates|Antimalware Updates]]
  * [[:content:en_us:7_ug_antispam_updates|Antispam Updates]]
  * [[:content:en_us:7_ug_antivirus|Gateway Antivirus]]
  * [[:content:en_us:7_ug_greylisting|Greylisting]]
  * [[:content:en_us:7_ug_mail_antispam|Mail Antispam]]
  * [[:content:en_us:7_ug_smtp|SMTP Server]]
{{keywords>clearos, clearos content, Mail Antivirus, app-mail-antivirus, clearos7, userguide, categoryserver, subcategorymessaging, maintainer_bchambers, maintainerreview_x}}

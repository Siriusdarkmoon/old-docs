===== Content Filter =====
The content filtering software blocks inappropriate websites from the end user. The software can also be used to enforce company policies; for instance, blocking personal webmail sites like Hotmail can decrease lost productivity at the office.

The filter engine uses a variety of methods including phrase matching, URL filtering and black/white lists.  Although the filter works effectively 'out-of-the-box', for best results, we recommend subscribing to a service level the includes the 'Content Filter Update' service (see Services link below).  By keeping your blacklist up-to-date, you will be providing your LAN with the most effective blocking solution against the 'churn' of sites that change daily.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Gateway|Proxy and Filtering|Content Filter</navigation>

===== Video Tutorial =====
{{youtube>large:NzMwskLB_1Y|2-minute ClearOS Enterprise Content Filter Basics}}

===== Content Filter Updates and ClearSDN =====
{{ :omedia:clearsdn-icon-xxs.png}} The ClearSDN [[http://www.clearcenter.com/Services/clearsdn-content-filter-updates-7.html|Content Filter Updates]] service provides weekly blacklist updates to improve the effectiveness of the content filter system.  These blacklists are compiled from third party organizations as well as internal engineering resources from ClearCenter.  We keep tabs on the latest available updates and fine tune the system so you can focus on more important things.

The **Content Filter** also hooks into the [[:content:en_us:5_antivirus|Antivirus]] and [[:content:en_us:5_antiphishing|Antiphishing]] engines in ClearOS.  You may also want to subscribe to the [[http://www.clearcenter.com/Services/clearsdn-antimalware-updates-7.html|Antimalware Updates]] service to keep your content filter running at its optimum.

===== Configuration =====
The web-based administration tool gives you access to a number of configuration settings.  The filter **must** be run in parallel with the [[:content:en_us:5_web_proxy|Web Proxy]] server.  

<note info>//**It is important to understand**// the implications of running the content filter with a web proxy server configured to run in **standard** mode.</note>

=== Standard Mode ===
In standard mode, the web proxy operates on port 3128 and the content filter operates on port 8080.  You must change the settings of all the web-browsers located on the local network to point to one of the above ports in order to take advantage of proxy or filtering services.   If users have the technical knowledge and have access to the browser settings on their local machine, they could potentially by-pass the proxy server and have full access to content on the Internet.

=== Transparent Mode ===
In transparent mode, all requests from the local network automatically pass through the web proxy cache.  The settings for the local machines do not need to be changed.  By-passing the proxy is not possible by changing browser settings on the local machine.  Obviously, this is the preferred configuration.

==== Configure Advanced Filtering ====
=== Banned File Extensions / Banned MIME Types ===
**Banned File Extensions**
Banning specific file extensions is a useful tool for limiting content available to users on the LAN.  It can also greatly decrease the chances of users unwittingly downloading and running 'arbitrary' code downloaded from the Internet which could potentially contain viruses, spyware of other malicious code.

By checking a box next to an extension, you are **disallowing** filtered users from accessing this file type.  If you wish an extension to be blocked and it is not listed in the available list, add it to the list using the "Add a new extension type" form.

**Banned MIME Types**
Similarly, MIME types instruct a browser to utilize certain applications in order to display content encoding.  Security exploits in the applications themselves can be used to infiltrate a computer.  MIME types checked in the "Banned MIME Types" form will not be allowed to pass through the firewall and to the computer making the request on the LAN, providing a more secure environment.

=== Banned Site List / Exempt Site List ===
**Banned Site List**
Sites entered in the "Banned Site List" will be banned, regardless of the site's content, or whether the site is on one of the blacklists.

**Exempt Site List**
Sites entered in the "Exempt Site List" will be allowed, regardless of the site's content.  Use this form if content on a site triggers a 'false positive' that you wish to override.

**Grey Site List**
Sites entered in the "Grey Site List" will not be blocked by the blacklists but will still be checked for content.  For example, you may have the **news** blacklist enabled to prevent people from wasting time during the business day.  However, you may have also decided to allow just BBC news. If you add bbc.co.uk to the exception list, all web pages will be allowed.  If you add bbc.co.uk to the greylist, then most pages will pass through just fine, but [[http://news.bbc.co.uk/2/hi/health/1346542.stm|this mildly racy page]] and other might get blocked by the phrase list system.

=== Banned User IP List / Exempt User IP List ===
If you have some or all of your workstations configured to use static IP addresses, you can configure individual workstations' access to the web.

**Banned User IP List**

Here you can configure LAN IP addresses that will be completely blocked from accessing the web. You can either add IP addresses individually or add groups as defined below.

**Exempt User IP List**

Here you can configure LAN IP addresses that will be granted free access to the web.  You can either add IP addresses individually or add groups as defined below.  When you configure an exempt IP, the web request still goes through the filter and proxy, but nothing is ever blocked.  If you need to completely bypass the content filter / web proxy system, you can use the [[:content:en_us:5_web_proxy|web site bypass in the proxy]] feature.

**Groups**

You can configure groups of IP addresses to simplify and organize workstation access to the web.  For example in an educational environment you can add all administrator/staff IP addresses to a //Staff// group and add them to the Exempt User IP List.

=== Weighted Phrasing ===
The content filter system uses phrase lists to calculate a score for every web page. You can fine tune your content filter scoring by specifying which phrase lists to use.  

In general you will want the phrase lists you select here to correspond with the blacklists you are using.  At a minimum you will want to include the //proxies// phraselist to prevent your users from bypassing the filter.

<note info>Note that more weighted phrases activated for the content filter mean that the filter will take more time to look at each page.  It is recommended that if you are using a low powered server, you limit the number of weighted phrase lists you use and instead use more blacklists. 
</note>

=== Blacklists ===
The content filter system uses black lists to block specific web sites. You can fine tune your content filter black lists by specifying which lists to use.  Note that these lists are updated weekly by the //**Content Filter Update Service** // if you have subscribed to that service.

==== Configure Filter ====
=== Sensitivity Level ===
The sensitivity level is an arbitrary scale that allows 'coarse' adjustment of the phrase filter sensitivity.  Increasing the sensitivity level means that fewer bad phrases/words will cause the filter to block the page.

=== PICS Level ===
An Internet standard for rating web content.  This setting will prove to be of minor significance as sites self-administrate this parameter.  As a general rule, the recommendation is to disable this setting.

=== Reporting Level ===
Several options are available to customize what a user sees when the filter blocks a page:

  * Stealth Mode - Site is not blocked...User's IP and site is logged 

  * Access Denied - User's browser will receive an 'Access Denied' in place of the web page.

  * Short Report - A short error message 'bubble' will be displayed like the one below

  * Full Report - Same as above, but the weighted limit and actual value will be displayed (useful for fine-tuning the system).

  * Custom Report - Uses the customizable HTML template

=== Block IP Domains ===
Used to prevent users from circumnavigating the URL-based portion of the filter by using IP addresses instead of URL's.  Pages will still be filtered based on the other filtering mechanisms: weightedphrases, mime types, file extensions etc.

=== Blanket Block ===
Most restrictive setting.  All sites will be blocked with the exception of those listed in the exempt list.  Useful for kiosks/public terminals where a browser is used to access a company site etc.
{{keywords>clearos, clearos content, Content Filter, app-content-filter, clearos5, userguide, categorygateway, subcategoryproxyandcontentfilter, maintainer_dloper}}

===== Apache Banner Linux Distribution Disclosure =====
This entry from Security Metrics indicates that the Apache service shows what operating system you are running.

===== ClearCenter response =====
==== Short response ====
Knowing the version of the operating system running is not a vulnerability.

==== Long response ====
While knowing the version of a flawed and non-up-to-date operating system is an advantage to a hacker, knowing that an operating system is an automatically updated OS which is known for timely fixes and updates is NOT an advantage but rather a deterrent to further investigation.

==== Resolution ====
No action required.

If you want to obfuscate your OS in Apache, you can perform the following:

First, establish a baseline by looking at your own headers:

  curl --head localhost

Next, modify the /etc/httpd/conf/httpd.conf file and change the following two lines:

  ServerSignature On
  Server Tokens OS

to:

  ServerSignature Off
  Server Tokens Prod

(optional)
... and while you are at it, close down php from revealing its version as well by modifying /etc/php.ini and changing:

  expose_php = On

to this:

  expose_php = Off

Restart the web service:

  service httpd restart

Lastly, re-examine the reporting service:

  curl --head localhost


===== Links =====
{{keywords>clearos, clearos content, 3rd party, security metrics, non-cve, maintainer_dloper}}

===== Squid =====
Squid is a high-performance proxy caching server for Web clients, supporting FTP, gopher, and HTTP data objects. Unlike traditional caching software, Squid handles all requests in a single, non-blocking, I/O-driven process. Squid keeps meta data and especially hot objects cached in RAM, caches DNS lookups, supports non-blocking DNS lookups, and implements negative caching of failed requests.

===== Changes =====
{{keywords>clearos, clearos content, AppName, app_name, version6, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

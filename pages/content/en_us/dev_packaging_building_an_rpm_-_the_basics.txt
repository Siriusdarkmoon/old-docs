===== Building An RPM - The Basics =====
If you have never built an RPM or need to know about the ClearOS package build environment, then this document is for you.  The information provided here gives you an overview of what is needed to manually build your own RPMs.   Though ClearOS uses a [[:content:en_us:dev_packaging_building_an_rpm_-_build_system|build system]] for creating RPMs, it is still worthwhile learning the manual procedure described here.

===== Prepare Your Development Environment =====
Before getting underway, the first thing you need to do create a development environment.  Follow [[:content:en_us:dev_development_environment|these instructions]] and come back to this document when you're done.

===== Prepare the Source Code =====
With your [[:content:en_us:dev_development_environment|development environment]] ready to go, it is now time to prepare the source code.  To get the ball rolling, we are going to start with a source RPM.  In our example, we are using the dnsmasq software since it is small and straightforward.  Don't worry that the source code is from version 5.x, it does not matter for this quick tutorial.

  rpm -Uvh http://download.clearfoundation.com/clearos/enterprise/sources/5.x/dnsmasq-2.49-1.1.v5.src.rpm

You can ignore those warnings about user/groups not existing - you will see that most of the time when install source RPMs.  Installing the source RPM added three files to the RPM build directory:

  * rpmbuild/SPECS/dnsmasq.spec
  * rpmbuild/SOURCES/dnsmasq.init
  * rpmbuild/SOURCES/dnsmasq-2.49.tar.gz

In more complex packages, you will see patches and other helper files in the SOURCES directory.  In our example, we only have the single dnsmasq tar-zip along with a ClearOS compatible start/stop script (/etc/rc.d/init.d/dnsmasq).  You don't need a source RPM to put these files in the SPECS and SOURCES directories, you can place those files manually.  We simply used a source RPM for convenience. 

===== Build the RPM =====
Now it is time to actually build the rpm!

   rpmbuild -ba rpmbuild/SPECS/dnsmasq.spec

Uh-oh.  Something went wrong:

  error: Failed build dependencies:
    dbus-devel is needed by dnsmasq-2.49-1.1.mydist.i386
 
The dnsmasq build requires the dbus-devel package.  You will find that you may need to install other *-devel packages to build an RPM, so get used to that process when building RPMs the old fashioned way.  As root, go ahead and install dbus-devel:

  su -c 'yum install dbus-devel' -

Then re-rerun the rpmbuild command:

  rpmbuild -ba rpmbuild/SPECS/dnsmasq.spec

If all goes well, you should get both your RPM and source RPM files built:

  * rpmbuild/SRPMS/dnsmasq-2.49-1.1.mydist.src.rpm
  * rpmbuild/RPMS/x86_64/dnsmasq-2.49-1.1.mydist.x86_64.rpm
  * rpmbuild/RPMS/x86_64/dnsmasq-debuginfo-2.49-1.1.mydist.x86_64.rpm

===== Next Steps =====
If this is your first time building an RPM, then you have just started a journey of learning and discovery!  The [[http://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/index.html|Fedora RPM Guide]] is a good place to go for a lot more detail.  Also take a look at the links listed at the end of this document.  From spec files, to good 64-bit habits, to cross-distribution support -- there is quite a bit of ground to cover.

Once you have the hang of it, you can start to delve into the [[:content:en_us:dev_packaging_building_an_rpm_-_build_system|ClearFoundation build system]].

===== Related Links =====
  * [[http://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/index.html|Fedora RPM Guide]]
  * [[http://www.gurulabs.com/GURULABS-RPM-LAB/GURULABS-RPM-GUIDE-v1.0.PDF|Guru Labs Creating RPMS]] - PDF 
  * [[http://www.rpm.org/|rpm.org]]
{{keywords>clearos, clearos content, AppName, app_name, dev, packing, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

===== ClearGLASS Community =====

ClearGLASS is a single dashboard to manage multi-cloud infrastructure.

===== Installation =====
If this app is not already installed, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this app in the menu system at the following location:
 
<navigation>Server|Management|ClearGLASS Community</navigation>

===== User Guides =====
==== Getting Started ====
  * [[content:en_us:cg_overview|ClearGLASS Overview]]
  * [[content:en_us:cg_setting-up-clearglass|Quick Start Guide]]
  * [[content:en_us:cg_manage-infrastructure|Managing Infrastructure]]
  * [[content:en_us:cg_launching-vms|Provisioning VMs and Containers]]

==== Adding Clouds, Bare-metal, and Containers ====
  * [[content:en_us:cg_adding-amazon-ec2|AWS EC2]]
  * [[content:en_us:cg_adding-microsoft-azure|Azure Classic]]
  * [[content:en_us:cg_adding-azure-arm|Azure Resource Manager (ARM)]]
  * [[content:en_us:cg_adding-other-servers|Bare-metal servers]]
  * [[content:en_us:cg_adding-clearcenter|ClearCenter]]
  * [[content:en_us:cg_adding-digital-ocean|Digital Ocean]]
  * [[content:en_us:cg_adding-docker|Docker containers]]
  * [[content:en_us:cg_adding-google-compute-engine|Google Compute Engine]]
  * [[content:en_us:cg_adding-hostvirtual|HostVirtual]]
  * [[content:en_us:cg_adding-kvm|KVM]]
  * [[content:en_us:cg_adding-linode|Linode]]
  * [[content:en_us:cg_adding-nephoscale|NephoScale]]
  * [[content:en_us:cg_onapp|OnApp]]
  * [[content:en_us:cg_adding-openstack|OpenStack]]
  * [[content:en_us:cg_adding-packet|Packet.net]]
  * [[content:en_us:cg_adding-rackspace|RackSpace]]
  * [[content:en_us:cg_adding-softlayer|SoftLayer]]
  * [[content:en_us:cg_adding-vmware-vcloud|VMware vCloud]]
  * [[content:en_us:cg_adding-vsphere|VMware vSphere]]
  * [[content:en_us:cg_adding-vultr|Vultr]]
  * [[content:en_us:cg_delete-clouds|Delete clouds]]
==== Cost Management and Reduction ====
  * [[content:en_us:cg_cost-reporting-graph|Dashboard - Cost Reporting Graph]]
  * [[content:en_us:cg_grouping-and-calculating-cost-using-tags|Grouping and calculating cost using tags]]
  * [[content:en_us:cg_machine-costs|Machine Costs]]
  * [[content:en_us:cg_custom-pricing|Custom Pricing]]
  * [[content:en_us:cg_reducing-cloud-costs|Reducing Cloud Costs]]
  * [[content:en_us:cg_insights-overview|Insights - Overview]]

==== Kubernetes ====
  * [[content:en_us:cg_kubernetes-getting-started-guide|Kubernetes Getting Started Guide]]
  * [[content:en_us:cg_scaling-a-kubernetes-cluster|Scaling a Kubernetes Cluster]]
  * [[content:en_us:cg_migrating-to-microservices-package|Migrating to Microservices Package]]
==== Docker Containers ====
  * [[content:en_us:cg_monitoring-docker-containers|Monitoring Docker Containers]]
==== Tagging ====
  * [[content:en_us:cg_tagging-overview|Tagging Overview]]
  * [[content:en_us:cg_using-tags-to-manually-set-pricing|Using Tags to Set Custom Pricing]]
  * [[content:en_us:cg_using-tags-to-group-machines-and-calculate-costs|Using Tags to Group Machines and Calculate Costs]]
  * [[content:en_us:cg_dynamically-adding-tags-to-machines|Dynamically Add Tags to Machines]]

==== Monitoring and Alerting ====
  * [[content:en_us:cg_setting-up-monitoring-on-openshift|Setting up monitoring on OpenShift]]
  * [[content:en_us:cg_monitoring-docker-containers|Monitoring Docker Containers]]
  * [[content:en_us:cg_monitor-disk-space-and-get-alerts|Monitor disk space and get alerts]]
  * [[content:en_us:cg_monitoring-on-linux|Setting up monitoring on Linux]]
  * [[content:en_us:cg_monitoring-on-windows|Setting up monitoring on Windows]]
  * [[content:en_us:cg_add-metrics|Adding more monitoring metrics]]
  * [[content:en_us:cg_setting-monitoring-through-a-proxy-server|Using a proxy to gather monitoring data in restrictive environments]]
  * [[content:en_us:cg_monitor-ping-connectivity|Monitor ping connectivity]]
  * [[content:en_us:cg_disable-monitoring|Disable monitoring]]
  * [[content:en_us:cg_adding-a-custom-metric|Adding a custom metric]]
  * [[content:en_us:cg_how-to-monitor-a-process-over-time-eg-apache-postgresql|How to monitor a process over time (eg apache, postgresql)]]
==== Self-Service Provisioning ====
  * [[content:en_us:cg_self-service-service-provisioning-for-devs|Self-Service Provisioning]]
==== SSH Keys ====
  * [[content:en_us:cg_adding-and-using-signed-ssh-keys|Adding and using signed ssh keys]]
  * [[content:en_us:cg_generating-keys|Generating new keys and installing them to your servers]]
  * [[content:en_us:cg_importing-your-ssh-keys-in-clearglass|Importing your SSH keys in ClearGLASS]]
==== Bare-metal Servers ====
  * [[content:en_us:cg_bare-metal-servers-overview|Bare-metal servers - Overview]]
==== Executable Scripts ====
  * [[content:en_us:cg_create-and-run-executable-scripts|Create and run executable scripts]]
==== Cloud Scheduler ====
  * [[content:en_us:cg_running-scheduled-tasks-cronjobs-with-clearglass-api|Automated Scheduling Feature (API)]]
  * [[content:en_us:cg_scheduler|Task Scheduling Example]]
==== Workflow Automation ====
  * [[content:en_us:cg_workflow-automation|Workflow Automation]]
==== Ansible Playbooks ====
  * [[content:en_us:cg_ansible-playbooks|Ansible Playbooks]]

==== FAQs ====
  * [[content:en_us:cg_cost-reporting-with-clearglass|Cost reporting with ClearGLASS]]
  * [[content:en_us:cg_what-ports-do-i-need-to-open-for-clearglass|What ports do I need to open for ClearGLASS]]
  * [[content:en_us:cg_how-do-i-change-my-email|How do I change my email]]
  * [[content:en_us:cg_how-do-i-reset-my-password|How do I reset my password]]
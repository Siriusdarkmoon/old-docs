===== Unknown Service Detection Banner Retrieval =====
This entry from Security Metrics indicates that the scan can see a TCP SYN banner for an unknown protocol.

===== ClearCenter response =====
==== Short response ====
The specified service is not known to have a vulnerability.

==== Long response ====
By simply telnetting to a port, header information may be offered by the specific protocol to which you connect. This is not indicative of a particular risk or vulnerability.

==== Resolution ====
No action is specifically required.

In this case, you may know more about the protocol running on that port than Security Metrics does. If this is not a public service, consider removing the general incoming firewall rule and replacing it with a targeted custom rule which allows only those hosts which require the server.

This may not be tenable so a refutation of the risk may be all that is required.

===== Links =====
{{keywords>clearos, clearos content, 3rd party, security metrics, non-cve, maintainer_dloper}}

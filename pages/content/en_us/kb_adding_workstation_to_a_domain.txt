===== Add a Windows Workstation to a Samba Domain =====
/**
 * t 538189
 */

/* ... */

This howto covers adding Window workstations to an NT4 style domain and adding workstations to AD capable domains. This is typically a straight-forward process but there can be some issues if you don't have things configured properly on your server and network or if your workstation OS version doesn't play nice for various reasons (which is more and more common).

===== Preparation =====
It is recommended that you have the following actions completed on your network before adding workstations to your domain:

  * Server Name
  * Certificates
  * Time and Date
  * DNS
  * WINS

==== Server Name ====
You will need to decide on the convention of the server name before setting things up. Most of this is done already in the wizard and so this consideration should happen even before you do the initial wizard. Here is a list of names that you will be asked for for your server:

  * Domain Name
  * Hostname
  * Internet Hostname
  * Domain
  * Server Name
 
== Domain Name ==
During the Wizard, in the section entitled 'Internet Domain', you will be asked for your Internet Domain. While you can use a domain name that employs a bogus top level domain like '.local' or '.lan', you can just as easily use your valid domain that you purchased from ClearCenter or some other registrar. The advantage of using a valid domain that is purchased from ClearCenter is that your boxes can tie in nicely to Dynamic DNS for ease of management and resiliency during network changes.

There is some debate about whether it is best practice or not to have a bogus domain on your LAN while employing valid domains on the internet or whether using a valid domain across the board is best.

The typical arguments for the bogus domains on LAN is that if you have a bogus domain names then you can address internal resources without revealing the IP addresses used internally to the outside. This is requires that you use a DNS server on the inside of your network. ClearOS can perform the function of this DNS server if needed.

The typical arguments for using a valid domain name on the LAN is that since you are using DNS on the inside of your LAN anyways, you can just use split-horizon DNS type topologies so that DNS inquiries on the LAN reveal the LAN servers in addition to external servers while external DNS queries only reveal externally configured server. ClearOS can perform the function of a caching split-horizon DNS server if needed. We recommend this method because it simplifies the environment.

== Hostname ==
This is the fully qualified domain name that your server will be known. Primarily this is a LAN perspective. If you are using a bogus domain name for the LAN it should be based on that bogus domain name. For example, 'server1.system.lan'. If you are using split horizon DNS then it would follow your valid fully qualified doamin name. For example, 'server1.example.com'.

== Internet Hostname ==
This is the hostname that would work for a valid DNS name from the outside world even if you are using bogus domain names on the inside. If you do not have a valid external name (because you having registered a domain), you can use the 'poweredbyclear.com' name which is designated to this box in your portal. The Internet Hostname parameter is used by programs like apache to designate your name space for your box as a distinct object from virtual names or other items. If you are using a valid domain name for your hostname (like 'server1.example.com' for example under split-horizon DNS), then you can simply set the value to be the exact same as the hostname.

== Domain ==
Not to be confused with 'Domain Name', this is the windows NT4 style short domain name that complies with NetBIOS. You will input this information in the Windows Networking module or the Active Directory Connector module. It follows the specification for LANManager domain names instead of Internet domain names. The domain is restricted to a 15 character, caseless name without special characters. The most common practice is to use the primary element of the Domain Name as the Domain if it comports with the requirements. For example, if the Hostname is 'server1.example.com' then you would set the Domain to be 'EXAMPLE'. The character [[https://technet.microsoft.com/en-us/library/cc959336.aspx|limit for this value is 15 characters]].

== Server Name ==
This is the short servername used in Windows environments and complies with [[https://technet.microsoft.com/en-us/library/cc959336.aspx|NetBIOS standards]]. It is not the full DNS name but in best practices would equal the name of the first part of the fully qualified domain name. For example, if your Hostname was 'server1.example.com', this name would be 'SERVER1'.

==== Certificates ====
While this isn't a strict requirement it is best practice to have the certificate authority of the servers set up and configured before proceeding. This should use 'Hostname' or 'Internet Hostname' configured previously for your certificate so that the names match. This will remove the certificate error if you happen to [[:content:en_us:kb_o_trusting_ca_of_clearos_in_workstations|trust this CA with your workstation]] (which is also a best practice.)

==== Time and Date ====
It is essential that the time and date on your network is consistent throughout. Part of the security model for CIFS (Common Internet File System, the protocol used by Windows Networking) relies on the time being accurate between the workstation and the server (and also the server and other servers which is especially important when using the Active Directory Connector).

One approach is to point all the workstations and servers to an external time server. This is by far the easiest configuration.

Another approach is to set up ClearOS or an AD server as a network time (NTP) server and point all of the time sync for workstations and servers to this central server. Then you can point just this central server to an external time sync device. The reason why this approach is more scalable is that it works even when your external connectivity to the Internet is down. Even if the central server is not able to get an accurate time from the world-view of things, it is able to keep all the nodes close by accurate. It is ok for it to even be wrong so long as everything else is wrong together! Another advantage to this approach is that you don't have increased usage of your internet pipe for updates to network time.

==== DNS ====
It is important to have a solid DNS strategy for your LAN infrastructure. ClearOS is capable of acting as a Caching DNS server. DNS queries to ClearOS are handled in the following order:

  * Split referrals to other DNS providers
  * DNS includes
  * Hosts file
  * DNS lookup external

ClearOS can reference other servers on your network and be used only for its own purposes, or ClearOS can be used as a primary location for DNS queries. Generally speaking, ClearOS is very fast at providing DNS resolution. In situations where there is an AD server present, it is recommended that the workstations on the network use AD for their DNS (This include deployments with Microsoft running as an AD domain controller and also situations where ClearOS is the AD domain controller, ie. Samba Directory.) 

== Split referrals to other DNS providers ==
In some cases it may be useful to refer ClearOS to different DNS provider for the implicit purpose of resolving a specific or several specific domains. This is recommended for Active Directory Connector and you can [[:content:en_us:kb_o_splitting_dns_cache_to_use_specific_dns_server_for_domains|use this guide to assist you]].

== DNS includes ==
While this is not required for anything related to this specific topic, it is useful to mention. You can include DNS statements within the DNS server to override other lookups. This can be useful in poisoning a specific DNS host or redirecting it for other purposes. For example, if on the local domain is web server that has a public IP address that is forwarded into your domain, you will not be able to use the external address to get to it. But by using a DNS include you can override the external lookup and provide the internal hostname to IP address designation.

This process works for DNS names that you want to work (split horizon DNS) and for DNS names that you don't want to work ([[http://tracker.clearfoundation.com/view.php?id=1956|poison]] [[|DNS]].)

== Hosts file ==
== DNS lookup external ==

==== Windows 7 and later Registry Changes to join a domain ====
For Windows 7 and later you will need to make changes to the workstation. You can manually make the changes using **'regedit'** or create this as a file in notepad and save it with the '.reg' extension. Then, double-click to add it to the registry. Here is the code:

<code>
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\LanManWorkstation\Parameters]

"DomainCompatibilityMode"=dword:00000001
"DNSNameResolutionRequired"=dword:00000000
</code>

**"DomainCompatibilityMode"=dword:00000001"**

By setting this parameter, you tell your workstation that you want it to use NT4 style domains. There is no security risk here other than the kerberos-layer differences intrinsic between NT4 and 2000 and it does not prohibit the workstation from joining an Windows 2000 domain or later domain.

**"DNSNameResolutionRequired"=dword:00000000**

As part of the domain join process, Windows 2000 and later domains not only make a computer account for the domain but also create a DNS entry. The workstation then validates that the DNS entry resolves before allowing the workstation to join the domain. There is no integration point in NT4 domains to DNS so this doesn't occur and as a result, the workstation will not join the domain. In our opinion, this was added to Windows 2000 domains and later in order to make management of DNS for workstations smoother. It doesn't enhance security but since it is required by default and doesn't work this way under NT4-style domains, we must disable it for the time being.

==== Windows 10 Registry Changes to run logon scripts ====
Microsoft have decided for security reasons to disable logon scripts from running in Windows 10, now preferring to use Group Policies. You cannot apply Group Policies automatically to the NT4 style of domains which ClearOS is using. To re-enable logon scripts in Windows 10 you need to make some registry changes before you join the domain. The easiest way is to create a file, say logon.reg with the following contents:
<code>Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System]
"EnableLinkedConnections"=dword:00000001

[HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\NetworkProvider\HardenedPaths]
"\\\\*\\NETLOGON"="RequireMutualAuthentication=0, RequireIntegrity=0,RequirePrivacy=0"
"\\\\*\\SYSVOL"="RequireMutualAuthentication=0, RequireIntegrity=0,RequirePrivacy=0"
"\\\\{MyWindowsDomainName}\\netlogon"="RequireMutualAuthentication=0, RequireIntegrity=0,RequirePrivacy=0"
</code>Change {MyWindowsDomainName} to your domain name (default is CLEARSYSTEM) then import the file by double-clicking on it **before you join the domain**.
<note>If you do not enable "Windows 10 Domain Logons" in Windows Networking, you can set "RequirePrivacy=1" and the more secure SMB3 protocol will be used.</note>
<note tip>You can create a single file with both sets of registry changes. The logon script registry changes have no effect on versions of Windows prior to Windows 10.</note>
==== Enabling the SMB1.0 Protocol in Windows 10 ====
Windows 10, since the Fall Creators Update (1709), is no longer shipping with SMB1.0 support enabled. This means that if you enable "Windows 10 Domain Logons", Windows 10 machines can no longer access Windows Networking (Samba) Domains. If you try to join a ClearOS Domain you may get the following popup:

{{7_ug_samba_no-smb1.png}}

The cause is that SMB1.0 support is now disabled by default in Windows 10. The link takes you to [[https://support.microsoft.com/en-gb/help/4034314/smbv1-is-not-installed-by-default-in-windows|this Microsoft document]]. To enable SMB1.0 support see [[https://support.microsoft.com/en-us/help/2696547/how-to-detect-enable-and-disable-smbv1-smbv2-and-smbv3-in-windows-and|this Microsoft document]] or just go Control Panel > Programs and Features > "Turn Windows Features on and off" then scroll down to SMB 1.0/CIFS File Sharing Support and enable it. You will need to reboot afterwards. There is also a PowerShell method in the document.

<note>With ClearOS7 up to date, including samba-4.7.1, there is no need to enable what was formerly named "Windows 10 Domain Logons" and is now named "Force SMB1 Protocol" if enabled. If disabled, the parameter won't show. Then you won't need to enable SMB1.0 in your Windows 10 clients.</note>

<note tip>If you feel you really need to force the SMB1 protocol to be used, you can edit /etc/samba/smb.conf and add the line:<code>server max protocol = NT1</code>It is worth pointing out that it was a vulnerability in the SMB1 protocol which enabled the WannaCry virus to spread.</note>

{{keywords>clearos, clearos content, kb, bestpractices, Windows Networking, app-windows-networking, clearos6, clearos7, categoryserver, subcategoryfile, maintainer_dloper}}

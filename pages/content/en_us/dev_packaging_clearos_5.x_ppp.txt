===== PPP =====
The ppp package contains the PPP (Point-to-Point Protocol) daemon and documentation for PPP support. The PPP protocol provides a method for transmitting datagrams over serial point-to-point links. PPP is usually used to dial in to an ISP (Internet Service Provider) or other organization over a modem and phone line.

===== Patches =====
  * LDAP patch

{{keywords>clearos, clearos content, AppName, app_name, version5, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

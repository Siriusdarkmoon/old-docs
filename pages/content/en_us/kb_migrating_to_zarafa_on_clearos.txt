=====  Migrating To Zarafa On ClearOS =====
This document provides some tips for migrating mail, contacts, calendars, etc. from Exchange and other mail servers.

===== Migrating from Outlook/Exchange =====
Migrating from Exchange is a common task. There differences between Zarafa and Exchange are subtle but with proper training and effective communication you can minimize your stress and the stress of your users (often the same thing).

==== Considerations ====
There are several consideration that need to be made when migrating from Exchange. First, Outlook contains data that does not fit into a PST file. Here are some of the following item you can expect not to migrate using PST only migration:

  * Categories 
  * Reply email paths (explained below)
  * Auto-complete (also explained below)

=== Reply email path ===
Emails set between users of the same email server (Exchange and other MIME servers) transact their sends through MIME maps which exist on the server. The return path for these addresses are intrinsic to the server. This means that emails sent before your migration will not be able to be replied to with the new server. Simply put, you are replying to the user via an address on the server that is not there anymore. This is true even if the name in the field shows the legitimate and well formed email address (ie. bob@example.com). If a user needs to reply to a message, use forwarding instead and select the recipient from the new Global Address List.

=== Auto-complete ===
Auto-complete will not work as expected because with the migration, you are changing the path back to users that are internal. Auto-completion, if working at all on your migrated profile for external users should be fine but for internal users, you will need to remove the bad auto-completes and repopulate them with entries from the Global Address List.

==== Individual PST migration using Outlook ====
There are two methods for migrating email to Outlook. You can import the PST into the new Zarafa profile as long as you are aware of key technical concerns. Additionally, the Zarafa Migration tool in the next section can be a big help to your success (especially if you have a lot of users).

Additionally, Zarafa is best configured while Outlook is CLOSED!!! Outlook can behave erratically if you change the account information while within Outlook itself. ClearCenter recommends using the Mail (or Mail 32-bit) control panel to make modifications to profiles and data structures while working with Zarafa.

The Outlook Zarafa Client has two main modes; online and cached. You cannot import a PST effectively when using cached mode. What was just said is important so it will be repeated ... you cannot import a PST effectively when using cached mode. The reason for this is that Outlook will import a PST to the cached copy and Zarafa does NOT sync items both ways on imports. As long as you are aware of this fact, you will have success. When you import a PST into Outlook that is connected to Zarafa, it will import properly if you are in Online mode. You can ALWAYS switch from Online mode to Cached mode so in a migration scenario, simply configure the workstation for online mode, complete the import then change it to cached mode after it is completed. 

If you have a client workstation that is already in cached mode, never fear. Simply make an additional profile on that same workstation. Launch Outlook under that profile, perform the import. Close Outlook. Launch Outlook in the cached mode. Start Synchronization.

==== Zarafa Migration Tool ====
If you are migrating from Outlook or Exchange, you can use Zarafa's migration tools to import a **.pst** to the Zarafa Server.  Please see [[http://doc.zarafa.com/7.0/Migration_Manual/en-US/html-single/index.html|Zarafa's Migration Manual]] for all the details.

===== Migrating from IMAP =====
If you are migrating from a standard IMAP server, you can use the **imapsync** tool installed on your ClearOS system to import mailboxes from a running mail server.  A basic command for doing such a migration is shown below.  The example shows the migration of **david's** mail from **oldmail.example.com** to the local Zarafa server:

  imapsync --subscribe --syncinternaldates \
  --host1 oldmail.example.com --port1 143 --user1 david --password1 mypassword --authmech1 LOGIN \
  --host2 127.0.0.1 --user2 david --password2 mypassword --authmech2 LOGIN \
  --dry

The **--subscribe** and **--syncinternaldates** flags should be used in the vast majority of cases.  Also take note of the **--dry** flag at the end.  This flag will run imapsync in test mode -- nothing will be synchronized, but a summary of what would happen is shown.  Once you are happy with the results of the dry run, you can run the synchronization without **--dry**.

==== Excluding Folders ====
You may want to exclude certain folders (e.g. a large Spam folder) from the synchronization.  Use the **--exclude** flag in this case:

  ... --exclude Spam --exclude Some_other_folder --exclude Another_folder

==== Mapping Folder Names ====
You may also want to change standard folder names from and old convention to what is used in Zarafa.  For example, your old mail server may use **Sent** for all sent mail, but Zarafa uses **Sent Items**.  Use the **--regextrans2** flag in this case:

   ... --regextrans2 "s,Sent,Sent\ Items,"


===== Migrating from ClearOS 5.x =====
Migrating from ClearOS 5.x (Horde/Kolab) is similar to the above procedure for plain old IMAP (please read that first).  However, there are a few flags that can be used to keep things nice an tidy:

  * Ignore the groupware folders: Calendar, Contacts, Notes and Tasks
  * Change the "Sent" folder to Zarafa's "Sent Items"

The flags for these tweaks are shown in the example below. 

  imapsync --subscribe --syncinternaldates \
  --host1 oldmail.example.com --port1 143 --user1 david --password1 mypassword --authmech1 LOGIN \
  --host2 127.0.0.1 --user2 david --password2 mypassword --authmech2 LOGIN \
  --exclude ^user --exclude Contacts --exclude Calendar --exclude Notes --exclude Tasks \
  --regextrans2 "s,Sent,Sent\ Items,"

{{keywords>clearos, clearos content, kb, howtos, maintainer_dloper, maintainerreview_x, keywordfix}}
